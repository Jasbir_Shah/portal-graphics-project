# Relativity

## Summary

Our rendering of M. C. Escher's lithograph 'Relativity'. This was a project developed for the COMP3320 Computer Graphics class of 2016
at The University of Newcastle, Australia. 

The goal of the assignment was to design anything related to computer graphics and illusions. Our project combined the painting 'Relativity'
by M. C. Escher and portals. We also avoided using libraries of a higher level than OpenGL and GLFW.

## Prerequisites

*   Visual Studio 2014+

## Contacts

*   Michael Banks    
*   Brendan Baxter
*   Jayden Matas
*   Adrian Mereles
*   Jasbir Shah