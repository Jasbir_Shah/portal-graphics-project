#define GLM_FORCE_RADIANS

#include <spdlog/spdlog.h>

#include "relativity/core/engine.hxx"
#include "relativityUtils/createPortalsFromFile.hxx"

static const auto FRAMEBUFFER_LOGGER = spdlog::stderr_logger_mt("framebuffer", true);
static const auto GL_LOGGER = spdlog::stderr_logger_mt("gl", true);
static const auto MESH_LOGGER = spdlog::stderr_logger_mt("mesh", true);
static const auto SHADER_LOGGER = spdlog::stderr_logger_mt("shader", true);
static const auto TEXTURE_LOGGER = spdlog::stderr_logger_mt("texture", true);

/**
 * Initialises an Engine instance.
 */
int main(int* argc, int* argv[]) {
    // Don't use cstdio past here
	std::ios_base::sync_with_stdio(false);
	//createPortalsFromFile::populatePortals();
	auto engine = std::make_unique<relativity::core::Engine>("resources/");
	engine->run();
	return 0;
}