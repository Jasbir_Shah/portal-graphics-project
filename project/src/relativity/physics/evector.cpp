#include "relativity/physics/evector.h"

using namespace relativity::physics;
using namespace std;

EVector::EVector()
{
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

EVector::EVector(double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

EVector::~EVector()
{
	
}

void EVector::setX(double _x)
{
	x = _x;
}

void EVector::setY(double _y)
{
	y = _y;
}

void EVector::setZ(double _z)
{
	z = _z;
}

double EVector::getX()
{
	return x;
}

double EVector::getY()
{
	return y;
}

double EVector::getZ()
{
	return z;
}
