#include "relativity/physics/physics_engine.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/mesh.hxx"

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <memory>
#include <string>

/*
*	physics_engine.cxx
*	is used to calculated physical interactions within the environment
*/
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::physics;
using namespace std;

PhysicsEngine::PhysicsEngine() : logger(spdlog::stdout_logger_mt("physics-engine", true))
{
}

PhysicsEngine::~PhysicsEngine()
{
	// nothing required
}

/*
*	update()
*	is used to update all values at the same time systematically
*/
void PhysicsEngine::update(
	const double time_delta,
	const EntityList& entities, const PortalMap& portals,
	const shared_ptr<Entity>& player,
	const shared_ptr<Entity>& stairs
) {
	bool processing = true;
	while (processing)
	{
		processing = false;
		const auto event = poll();

		switch (event.get_type())
		{
		case event::EventType::NO_EVENT: break;
		default:
			player->notify(event);
			processing = true;
			break;
		}
	}

	movePlayer(time_delta, player);
	checkPortals(portals, player);
	checkCollisions(time_delta, player, stairs);

	auto mesh = player->get_graphics_component()->get_meshes().at(0);
	const auto pos = glm::vec3(glm::column(mesh->get_model(), 3));


}

/*
*	checkPortals()
*	is used to check wheteher a player has collided with a portal
*/
void PhysicsEngine::checkPortals(
	const PortalMap& portals,
	const shared_ptr<Entity>& player
) {
	for (const auto& portal : portals) {
		const auto player_pos = player->get_world_position();
		const auto portal_pos = portal.second->get_position();
		const auto threshold = 10.f;

		if (glm::distance(player_pos, portal_pos) < threshold) {
			auto mesh = player->get_graphics_component()->get_meshes().at(0);
			const auto destination = portals.at(portal.second->get_exit());
			const auto relative_position = player_pos - portal_pos;

			mesh->set_model(glm::mat4());

			player->set_position(destination->get_position());
			player->set_world_position(destination->get_position());
			player->set_yaw(0);

			mesh->set_model(glm::translate(mesh->get_model(), destination->get_position()));

			player->set_old_position(player->get_position());
			player->set_old_yaw(player->get_yaw());
			player->set_old_pitch(player->get_pitch());
			player->set_old_roll(player->get_roll());
			player->set_old_model(mesh->get_model());

			player->set_yaw(glm::radians(destination->get_orientation().z));
			player->set_pitch(glm::radians(destination->get_orientation().y));
			player->set_roll(glm::radians(destination->get_orientation().x));

			mesh->set_model(glm::rotate(mesh->get_model(), player->get_yaw(), glm::vec3(0, 1, 0)));
			mesh->set_model(glm::rotate(mesh->get_model(), player->get_pitch(), glm::vec3(1, 0, 0)));
			mesh->set_model(glm::rotate(mesh->get_model(), player->get_roll(), glm::vec3(0, 0, 1)));

			player->set_position(player->get_position() + (player->get_movement() * 20.0f));

			player->set_gravity(destination->get_gravity());
			player->set_up(glm::vec3(0, 0, 0) - destination->get_gravity());

			mesh->set_model(glm::translate(mesh->get_model(), -player->get_old_position()));
			mesh->set_model(glm::translate(mesh->get_model(), player->get_position()));

			glm::vec3 orientation;

			if (player->get_up().y == 1)
			{
				glm::vec3 orientation = glm::normalize(glm::vec3(
					-cos(player->get_yaw()),
					0,
					sin(player->get_yaw())
				));
			}

			else if (player->get_up().z == 1)
			{
				glm::vec3 orientation = glm::normalize(glm::vec3(
					-cos(player->get_yaw()),
					-sin(player->get_yaw()),
					0
				));
			}

			else if (player->get_up().x == 1 || player->get_up().x == -1.f)
			{
				glm::vec3 orientation = glm::normalize(glm::vec3(
					0,
					-sin(player->get_yaw()),
					-cos(player->get_yaw())
				));
			}

			player->set_orientation(orientation);
			const auto pos = glm::vec3(glm::column(mesh->get_model(), 3));

			player->set_world_position(pos);
			player_position = player->get_world_position();

			mesh->set_model(glm::rotate(mesh->get_model(), player->get_roll(), glm::vec3(0, 0, -1)));
			mesh->set_model(glm::rotate(mesh->get_model(), player->get_roll(), glm::vec3(0, 0, -1)));
		}
	}
}

/*
*	movePlayer()
*	calls the players update method that will check whether or not the user is requesting the player to move
*/
void PhysicsEngine::movePlayer(
	const double time_delta,
	const shared_ptr<Entity>& player
) {
	player->update(time_delta);
}

/*
*	applyGravity()
*	if a player's feet are on the surface of a object, return true
*/
bool PhysicsEngine::applyGravity(const vector<glm::vec3>& bounding_box, const std::shared_ptr<Entity>& player)
{
	 auto max = bounding_box[MAX];
	 auto min = bounding_box[MIN];

	if (player->get_up().y == 1)
	{
		return
			(player_position.x > min.x && player_position.x < max.x) &&
			(player_position.y - FEET > min.y - 1 && player_position.y - FEET < max.y + 1) &&
			(player_position.z > min.z && player_position.z < max.z);
	}

	else if (player->get_up().z == 1)
	{
		return
			(player_position.x > min.x && player_position.x < max.x) &&
			(player_position.z - FEET > min.z - 1 && player_position.z - FEET < max.z + 1) &&
			(player_position.y > min.y && player_position.y < max.y);
	}

	else if (player->get_up().x == 1 || player->get_up().x == -1.f)
	{
		return
			(player_position.y > min.y && player_position.y < max.y) &&
			(player_position.x + FEET > min.x - 2 && player_position.x + FEET < max.x + 2) &&
			(player_position.z > min.z && player_position.z < max.z);
	}
}


/*
*	checkCollisions()
*	checks a player's position against every object to see if it is colliding with an object. If so, prevent player from moving
*/
void PhysicsEngine::checkCollisions(
	const double time_delta,
	const shared_ptr<Entity>& player,
	const shared_ptr<Entity>& stairs
) {
	bool grav;
	grav = true;

	player_position = player->get_world_position();

	for (const auto& mesh : stairs->get_graphics_component()->get_meshes())
	{
		for (const auto& subMesh : mesh->get_submeshes())
		{
			std::vector<glm::vec3> bounding_box = subMesh->get_bounding_box();

			if (check(player, bounding_box, time_delta))
			{
				player->stop();
			}

			if (applyGravity(bounding_box, player))
			{
				grav = false;
			}
		}
	}

	if (grav)
	{
		player->set_old_position(player->get_position());
		player->set_position(player->get_position() + (
			glm::vec3(0.f, -1.f, 0.f) * GRAVITY * (float)time_delta));
		player->set_world_position(player->get_world_position() + (player->get_gravity() 
			* GRAVITY * (float)time_delta));

		player_position = player->get_world_position();

		auto mesh = player->get_graphics_component()->get_meshes().at(0);

		mesh->set_model(glm::translate(mesh->get_model(), -player->get_old_position()));
		mesh->set_model(glm::translate(mesh->get_model(), player->get_position()));
	}
}

/*
*	check()
*	checks a player's feet position against a selected object to see whether the player is within the bounding box
*/
bool PhysicsEngine::check(
	const shared_ptr<Entity>& player,
	vector<glm::vec3>& bounding_box,
	const double time_delta
) {

	 auto max = bounding_box[MAX];
	 auto min = bounding_box[MIN];

	if (player->get_up().y == 1)
	{
		if ((player_position.x + 2 > min.x && player_position.x + 2 < max.x) &&
			(player_position.y - FEET + 1 > min.y && player_position.y - FEET + 1 < max.y) &&
			(player_position.z + 2 > min.z && player_position.z + 2 < max.z))
		{
			if (player_position.y - FEET > max.y - 15)
			{
				player->set_old_position(player->get_position());
				glm::vec3 position = player_position;

				position.y += (max.y - player->get_world_position().y + FEET);
				player->set_world_position(position);

				position = player->get_position();

				position.y += (max.y - player->get_position().y + FEET);
				player->set_position(position);

				player_position = player->get_world_position();

				auto mesh = player->get_graphics_component()->get_meshes().at(0);

				mesh->set_model(glm::translate(mesh->get_model(), -player->get_old_position()));
				mesh->set_model(glm::translate(mesh->get_model(), player->get_position()));

				return false;
			}

			return true;
		}

		return false;
	}

	if (player->get_up().x == 1 || player->get_up().x == -1.f)
	{
		if ((player_position.y + 2 > min.y && player_position.y + 2 < max.y) &&
			(player_position.x + FEET - 1 > min.x && player_position.x + FEET - 1 < max.x) &&
			(player_position.z + 2 > min.z && player_position.z + 2 < max.z))
		{
			if (player_position.x + FEET > max.x - 15)
			{
				player->set_old_position(player->get_position());
				glm::vec3 position = player->get_position();;

				get_logger()->info("Player pos before <{}, {}, {}>", position.x, position.y, position.z);

				position.y -= (min.x - player->get_world_position().x - FEET);
				player->set_position(position);

				get_logger()->info("Player pos after <{}, {}, {}>", position.x, position.y, position.z);

				position = player_position;

				position.x += (min.x - player->get_world_position().x - FEET);
				player->set_world_position(position);

				player_position = player->get_world_position();

				auto mesh = player->get_graphics_component()->get_meshes().at(0);

				mesh->set_model(glm::translate(mesh->get_model(), -player->get_old_position()));
				mesh->set_model(glm::translate(mesh->get_model(), player->get_position()));

				return false;
			}

			return true;
		}

		return false;
	}

	if (player->get_up().z == 1)
	{
		if ((player_position.x + 2 > min.x && player_position.x + 2 < max.x) &&
			(player_position.z - FEET + 1 > min.z && player_position.z - FEET + 1 < max.z) &&
			(player_position.y + 2 > min.y && player_position.y + 2 < max.y))
		{
			if (player_position.z - FEET > max.z - 15)
			{
				player->set_old_position(player->get_position());
				glm::vec3 position = player_position;

				position.z += (max.z - player->get_world_position().z + FEET);
				player->set_world_position(position);

				position = player->get_position();

				position.z += (max.z - player->get_position().z + FEET);
				player->set_position(position);

				player_position = player->get_world_position();

				auto mesh = player->get_graphics_component()->get_meshes().at(0);

				mesh->set_model(glm::translate(mesh->get_model(), -player->get_old_position()));
				mesh->set_model(glm::translate(mesh->get_model(), player->get_position()));

				return false;
			}

			return true;
		}

		return false;
	}

	return false;
}

bool PhysicsEngine::intersects(const shared_ptr<Entity>& player, const shared_ptr<Mesh>& mesh) {
	if (!mesh->has_bounding_box()) return false;

	const auto max = mesh->get_bounding_box()[MAX];
	const auto min = mesh->get_bounding_box()[MIN];

	return
		(player_position.x > min.x && player_position.x < max.x) &&
		(player_position.y < min.y && player_position.y > max.y) &&
		(player_position.z > min.z && player_position.z < max.z);
}
