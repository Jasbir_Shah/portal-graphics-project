#include "relativity/entity/light.hxx"
#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"
#include <iostream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace relativity::entity;
using namespace relativity::event;
using namespace std;

const glm::vec3 Light::roll(1.0f, 0.0f, 0.0f);
const glm::vec3 Light::pitch(0.0f, 1.0f, 0.0f);
const glm::vec3 Light::yaw(0.0f, 0.0f, 1.0f);

void Light::update(const float time_delta) {
	// It's important that this comes first
	process_events();
	const float rotation = get_rotation_speed() * time_delta;
	bool changed = false;

	if (is_rotating) {
		set_yaw(get_yaw() + rotation);
		changed = true;
	}

	if (changed) {
		// Convert our degrees into radians, the <cmath> API takes radians only
		auto yaw_radians = glm::radians(get_yaw());
		auto pitch_radians = glm::radians(get_pitch());

		// We find the light's front vector by performing some trigonometry
		// with the light's yaw and pitch orientation.
		// It's important that we normalize it to get a unit vector 
		const auto new_target = glm::normalize(glm::vec3(
			cos(yaw_radians) * cos(pitch_radians),
			sin(pitch_radians),
			sin(yaw_radians) * cos(pitch_radians)
		));

		set_target(new_target);
		set_view(glm::lookAt(get_position(), get_position() + get_target(), pitch));
		light_space = get_projection() * get_view();
	}
}

void Light::process_events() {
	bool processing = true;

	// Keep processing events until we either run out or find an event we care about
	while (processing) {
		processing = false;
		const auto event = poll();

		switch (event.get_type()) {
		case EventType::NO_EVENT: break;
		// The following events are just flag toggles for the shaders
		case EventType::TOGGLE_FOG:
			using_fog = !using_fog;
			if (using_fog) get_logger()->info("Enabling fog");
			else get_logger()->info("Disabling fog");
			break;
		case EventType::TOGGLE_GAMMA:
			using_gamma = !using_gamma;
			if (using_gamma) get_logger()->info("Enabling gamma correction");
			else get_logger()->info("Disabling gamma correction");
			break;
		case EventType::TOGGLE_PHONG:
			using_blinn_phong = !using_blinn_phong;
			if (using_blinn_phong) get_logger()->info("Enabling Blinn-Phong specular shading");
			else get_logger()->info("Disabling Blinn-Phong specular shading");
			break;
		case EventType::TOGGLE_BRDF:
			using_brdf = !using_brdf;
			if (using_brdf) get_logger()->info("Enabling BRDF");
			else get_logger()->info("Disabling BRDF");
			break;
		case EventType::TOGGLE_LIGHT:
			using_perspective = !using_perspective;
			if (using_perspective) get_logger()->info("Enabling Perspective Lights");
			else get_logger()->info("Disabling Perspective Lights");
			break;
		case EventType::TOGGLE_SHADOW:
			using_shadow = !using_shadow;
			if (using_shadow) get_logger()->info("Enabling Shadows");
			else get_logger()->info("Disabling Shadows");
			break;
		case EventType::TOGGLE_AMBIENT:
			using_ambient = !using_ambient;
			if (using_ambient) get_logger()->info("Enabling Ambient");
			else get_logger()->info("Disabling Ambient");
			break;
		case EventType::TOGGLE_DIFFUSE:
			using_diffuse = !using_diffuse;
			if (using_diffuse) get_logger()->info("Enabling Diffuse");
			else get_logger()->info("Disabling Diffuse");
			break;
		case EventType::TOGGLE_SPECULAR:
			using_specular = !using_specular;
			if (using_specular) get_logger()->info("Enabling Specular");
			else get_logger()->info("Disabling Specular");
			break;
		case EventType::TOGGLE_FALLOFF:
			using_falloff = !using_falloff;
			if (using_falloff) get_logger()->info("Enabling Falloff");
			else get_logger()->info("Disabling Falloff");
			break;
		case EventType::TOGGLE_MAT_DIFFUSE:
			using_material_diffuse = !using_material_diffuse;
			if (using_material_diffuse) get_logger()->info("Enabling Material Diffuse");
			else get_logger()->info("Disabling Material Diffuse");
			break;
		case EventType::TOGGLE_MAT_NORMAL:
			using_material_normal = !using_material_normal;
			if (using_material_normal) get_logger()->info("Enabling Material Normal");
			else get_logger()->info("Disabling Material Normal");
			break;
		case EventType::TOGGLE_MAT_SPECULAR:
			using_material_specular = !using_material_specular;
			if (using_material_specular) get_logger()->info("Enabling Material Specular");
			else get_logger()->info("Disabling Material Specular");
			break;
		case EventType::TOGGLE_LIGHT_ROTATE:
			is_rotating = !is_rotating;
			if (is_rotating) get_logger()->info("Enabling Light Rotation");
			else get_logger()->info("Disabling Light Rotation");
			break;
			// We don't care about this event, get another
		default: processing = true; break;
		}
	}
}