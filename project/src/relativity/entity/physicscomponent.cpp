#include "relativity/entity/physicscomponent.h"

using namespace relativity::entity;
using namespace std;

PhysicsComponent::PhysicsComponent()
{
	location.reserve(SIZEOFVECTOR);
	velocity.reserve(SIZEOFVECTOR);
	acceleration.reserve(SIZEOFVECTOR);

	for (int i = 0; i < location.size(); i++)
	{
		location[i] = 0;
		velocity[i] = 0;
		acceleration[i] = 0;
	}
}

PhysicsComponent::~PhysicsComponent()
{

}

void PhysicsComponent::setLocation(std::vector<double> _location)
{
	location = _location;
}

void PhysicsComponent::setLocation(double _x, double _y, double _z)
{
	location[X] = _x;
	location[Y] = _y;
	location[Z] = _z;
}

void PhysicsComponent::setVelocity(std::vector<double> _velocity)
{
	velocity = _velocity;
}

void PhysicsComponent::setVelocity(double _x, double _y, double _z)
{
	velocity[X] = _x;
	velocity[Y] = _y;
	velocity[Z] = _z;
}

void PhysicsComponent::setAcceleration(std::vector<double> _acceleration)
{
	acceleration = _acceleration;
}

void PhysicsComponent::setAcceleration(double _x, double _y, double _z)
{
	acceleration[X] = _x;
	acceleration[Y] = _y;
	acceleration[Z] = _z;
}

void relativity::entity::PhysicsComponent::move()
{

}
