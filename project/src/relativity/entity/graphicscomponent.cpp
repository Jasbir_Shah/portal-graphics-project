#include "relativity/entity/graphicscomponent.h"
#include "relativity/graphics/mesh.hxx"

#include <initializer_list>
#include <memory>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace std;

GraphicsComponent::GraphicsComponent() : meshes(vector<shared_ptr<Mesh>>()) {
	// Do nothing
}

GraphicsComponent::GraphicsComponent(initializer_list<shared_ptr<Mesh>> meshes) : meshes(vector<shared_ptr<Mesh>>(meshes)) {
	// Do nothing
}

GraphicsComponent::~GraphicsComponent() {
	// Do nothing
}