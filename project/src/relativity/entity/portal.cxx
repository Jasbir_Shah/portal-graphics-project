#include "relativity/entity/portal.hxx"
#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace relativity::entity;
using namespace relativity::event;
using namespace std;

Portal::Portal(
	const glm::vec3 position, const glm::vec3 orientation, const glm::vec3 gravity,
	const string exit, const shared_ptr<GraphicsComponent> mesh
): 	exit(exit), mesh(mesh), position(position), orientation(orientation), gravity(gravity) {

	// Convert the Euler angle orientation to a quaternion
	const auto rotation = glm::toMat4(
		glm::toQuat(
			glm::orientate3(glm::vec3(
				glm::radians(get_orientation().x),
				glm::radians(get_orientation().y),
				glm::radians(get_orientation().z)
			))
		)
	);

	// Translate the model matrix by the position, then rotate it by the orientation
	model = glm::translate(glm::mat4(), position) *	rotation;
}

void Portal::update(const float time_delta) {
	// It's important that event processing is done first, this 
	// function is time-variant
	process_events();
}

void Portal::process_events() {
	bool processing = true;

	// Keep processing events until we find something or hit the end of the queue
	while (processing) {
		processing = false;
		const auto event = this->poll();

		switch (event.get_type()) {
		case EventType::NO_EVENT: break;
		// We don't care about this event, get another
		default: processing = true; break;
		}
	}
}

void Portal::draw() {
	for (const auto submesh : mesh->get_meshes()) {
		for (const auto subsubmesh : submesh->get_submeshes()) {
			subsubmesh->bind();
			subsubmesh->draw();
		}
	}
}

void Portal::draw() const {
	for (const auto submesh : mesh->get_meshes()) {
		for (const auto subsubmesh : submesh->get_submeshes()) {
			subsubmesh->bind();
			subsubmesh->draw();
		}
	}
}

glm::mat4 Portal::project(const glm::mat4& view, const glm::mat4& proj) {
	const auto distance = glm::length(get_position());

	glm::vec4 plane(
		get_orientation() * glm::vec3(0.f, 0.f, -1.f),
		distance
	);

	plane = glm::inverse(glm::transpose(view)) * plane;

	if (plane.w > 0.0f) {
		return proj;
	}

	const auto q = glm::inverse(proj)
		* glm::vec4(
			glm::sign(plane.x),
			glm::sign(plane.y),
			1.0f, 1.0f
		);

	const auto c = plane * (2.0f / glm::dot(plane, q));

	const auto clipped_proj = glm::row(
		proj,
		2,
		c - glm::row(proj, 3)
	);

	return clipped_proj;
}