#include "..\..\..\include\relativity\entity\entity.h"
#include "relativity/entity/entity.h"
#include "relativity/entity/physicscomponent.h"
#include "relativity/entity/graphicscomponent.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::event;
using namespace std;

relativity::entity::Entity::Entity(const std::shared_ptr<GraphicsComponent> graphics_component, const std::shared_ptr<PhysicsComponent> physics_component)
	: graphics_component(graphics_component), physics_component(physics_component)
{
	gravity = glm::vec3(0.0f, -1.0f, 0.0f);
	yaw = 0;
	pitch = 0;
	roll = 0;
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	orientation = glm::normalize(glm::vec3(0.0f, 0.0f, 1.0f));
	target = glm::vec3(0.0f, 0.0f, 0.0f);
	old_position = glm::vec3(0.0f, 0.0f, 0.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);

	moving_forward = false;
	moving_backward = false;
	rotate_left = false;
	rotate_right = false;
	changed = false;
}

relativity::entity::Entity::~Entity()
{

}

void relativity::entity::Entity::set_gravity(glm::vec3 _gravity)
{
	gravity = _gravity;
}

void relativity::entity::Entity::set_yaw(float _yaw)
{
	yaw = _yaw;
}

void relativity::entity::Entity::set_pitch(float _pitch)
{
	pitch = _pitch;
}

void relativity::entity::Entity::set_roll(float _roll)
{
	roll = _roll;
}

void relativity::entity::Entity::set_old_yaw(float _old_yaw)
{
	old_yaw = _old_yaw;
}

void relativity::entity::Entity::set_old_pitch(float _old_pitch)
{
	old_pitch = _old_pitch;
}

void relativity::entity::Entity::set_old_roll(float _old_roll)
{
	old_roll = _old_roll;
}

void relativity::entity::Entity::set_position(glm::vec3 _position)
{
	position = _position;
}

void relativity::entity::Entity::set_world_position(glm::vec3 _world_position)
{
	world_position = _world_position;
}

void relativity::entity::Entity::set_orientation(glm::vec3 _orientation)
{
	orientation = glm::normalize(_orientation);
}

void relativity::entity::Entity::set_target(glm::vec3 _target)
{
	target = _target;
}

void relativity::entity::Entity::set_old_position(glm::vec3 _old_position)
{
	old_position = _old_position;
}

void relativity::entity::Entity::set_old_model(glm::mat4 _old_model)
{
	old_model = _old_model;
}

void relativity::entity::Entity::set_up(glm::vec3 _up)
{
	up = _up;
}

void relativity::entity::Entity::set_movement(glm::vec3 _movement)
{
	movement = _movement;
}

std::shared_ptr<PhysicsComponent> relativity::entity::Entity::get_physics_component()
{
	return physics_component;
}

std::shared_ptr<GraphicsComponent> relativity::entity::Entity::get_graphics_component()
{
	return graphics_component;
}

glm::vec3 relativity::entity::Entity::get_gravity()
{
	return gravity;
}

float relativity::entity::Entity::get_yaw()
{
	return yaw;
}

float relativity::entity::Entity::get_pitch()
{
	return pitch;
}

float relativity::entity::Entity::get_roll()
{
	return roll;
}

float relativity::entity::Entity::get_old_yaw()
{
	return old_yaw;
}

float relativity::entity::Entity::get_old_pitch()
{
	return old_pitch;
}

float relativity::entity::Entity::get_old_roll()
{
	return old_roll;
}

glm::vec3 relativity::entity::Entity::get_position()
{
	return position;
}

glm::vec3 relativity::entity::Entity::get_world_position()
{
	return world_position;
}

glm::vec3 relativity::entity::Entity::get_orientation()
{
	return orientation;
}

glm::vec3 relativity::entity::Entity::get_target()
{
	return target;
}

glm::vec3 relativity::entity::Entity::get_old_position()
{
	return old_position;
}

glm::mat4 relativity::entity::Entity::get_old_model()
{
	return old_model;
}

glm::vec3 relativity::entity::Entity::get_up()
{
	return up;
}

glm::vec3 relativity::entity::Entity::get_movement()
{
	return movement;
}

void Entity::processEvents()
{
	bool processing = true;

	while (processing)
	{
		processing = false;
		const auto event = this->poll();

		auto mesh = this->get_graphics_component()->get_meshes().at(0);

		switch (event.get_type())
		{
		case EventType::NO_EVENT: break;
		case EventType::PLAYER_JUMP:
			set_position(get_position() + 30.f * glm::vec3(0, 1, 0));
			set_world_position(get_world_position() + 30.f * get_up());

			mesh->set_model(glm::translate(mesh->get_model(), -get_old_position()));
			mesh->set_model(glm::translate(mesh->get_model(), get_position()));

			old_world_position = world_position;
			set_old_position(get_position());
			set_old_yaw(get_yaw());
			set_old_model(mesh->get_model());

			break;

		case EventType::PLAYER_MOVE_FORWARD: moving_forward = true; break;
		case EventType::PLAYER_MOVE_BACKWARD: moving_backward = true; break;
		case EventType::PLAYER_ROTATE_LEFT: rotate_left = true; break;
		case EventType::PLAYER_ROTATE_RIGHT: rotate_right = true; break;

		case EventType::STOP_PLAYER_MOVE_FORWARD: moving_forward = false; break;
		case EventType::STOP_PLAYER_MOVE_BACKWARD: moving_backward = false; break;
		case EventType::STOP_PLAYER_ROTATE_LEFT: rotate_left = false; break;
		case EventType::STOP_PLAYER_ROTATE_RIGHT: rotate_right = false; break;
		default: processing = true; break;
		}
	}
}

void relativity::entity::Entity::name()
{

}

void Entity::stop()
{
	auto mesh = this->get_graphics_component()->get_meshes().at(0);
	mesh->set_model(old_model);
	position = old_position;
	set_yaw(old_yaw);
	world_position = old_world_position;
}

void relativity::entity::Entity::update(const double time_delta)
{
	changed = false;
	const float distance = MOVEMENT_SPEED * time_delta;
	const float rotation = ROTATION_SPEED * time_delta;

	auto mesh = this->get_graphics_component()->get_meshes().at(0);

	old_world_position = world_position;
	set_old_position(get_position());
	set_old_yaw(get_yaw());
	set_old_model(mesh->get_model());

	if (get_up().y == 1)
	{
		orientation = glm::normalize(glm::vec3(
			-cos(get_yaw()),
			0,
			sin(get_yaw())
		));
	}

	else if (get_up().z == 1)
	{
		orientation = glm::normalize(glm::vec3(
			-cos(get_yaw()),
			-sin(get_yaw()),
			0
		));
	}

	else if (get_up().x == 1 || get_up().x == -1)
	{
		orientation = glm::normalize(glm::vec3(
			0,
			-sin(get_yaw()),
			-cos(get_yaw())
		));
	}

	//orientation = glm::normalize(glm::vec3(
	//	-cos(get_yaw()),
	//	0,
	//	sin(get_yaw())
	//));

	processEvents();

	if (moving_forward)
	{
		set_position(get_position() + (movement * distance));
		changed = true;
		set_world_position(get_world_position() += orientation*distance);
	}

	if (moving_backward)
	{
		set_position(get_position() - (movement * distance));
		changed = true;
		set_world_position(get_world_position() -= orientation*distance);
	}

	if (rotate_left)
	{
		set_yaw(get_yaw() + rotation);
		changed = true;
	}

	if (rotate_right)
	{
		set_yaw(get_yaw() - rotation);
		changed = true;
	}

	if (changed)
	{
		mesh->set_model(glm::translate(mesh->get_model(), -get_old_position()));
		mesh->set_model(glm::translate(mesh->get_model(), get_position()));
		mesh->set_model(glm::rotate(mesh->get_model(), get_yaw() - get_old_yaw(), glm::vec3(0, 1, 0)));
	}
}
