#include "relativity/entity/camera.hxx"
#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace relativity::entity;
using namespace relativity::event;
using namespace std;

// Orthonormal vectors for convenience

void Camera::update(const float time_delta) {
	// It's important that event processing is done first, this 
	// function is time-variant
	
	process_events();

	/*if (rotating_up)
	{
		pitch_rotation += ROTATION_CHANGE;
	}

	if (rotating_down)
	{
		pitch_rotation -= ROTATION_CHANGE;
	}

	if (rotating_left)
	{
		yaw_rotation += ROTATION_CHANGE;
	}

	if (rotating_right)
	{
		yaw_rotation -= ROTATION_CHANGE;
	}

	if (moving_forward_once)
	{
		set_distance -= ZOOM_CHANGE;
	}

	if (moving_backward_once)
	{
		set_distance += ZOOM_CHANGE;
	}*/

	calculate_position();
	view = glm::lookAt(position, player->get_world_position(), up);
}

void relativity::entity::Camera::calculate_position()
{
	auto yaw = player->get_yaw();
	auto old_yaw = player->get_old_yaw();

	if (player->get_up().y == 1.f) {
		distance.x = set_distance * cos(-yaw + yaw_rotation);
		distance.y += pitch_rotation;
		distance.z = set_distance * sin(-yaw + yaw_rotation);
	}
	else if (player->get_up().x == 1.f || player->get_up().x == -1.f) {
		if (player->get_up().x == -1.f) {
			if (distance.x > 0.f) {
				distance.x = -distance.x;
			}
		}

		distance.x += pitch_rotation;
		distance.y = set_distance * -sin(-yaw + yaw_rotation); 
		distance.z = set_distance * cos(-yaw + yaw_rotation);
	}
	else if (player->get_up().z == 1.f) {
		distance.x = set_distance * -cos(-yaw + yaw_rotation); 
		distance.y = set_distance * sin(-yaw + yaw_rotation);
		distance.z += pitch_rotation; 
	}

	position = player->get_world_position() + distance;
	up = player->get_up();
}

void Camera::process_events() {
	bool processing = true;

	// Keep processing events until we find something or hit the end of the queue
	while (processing) {
		processing = false;
		const auto event = this->poll();

		switch (event.get_type()) {
		case EventType::NO_EVENT: break;
		// Spatial movement
		case EventType::CAMERA_MOVE_UP:	moving_up = true; break;
		case EventType::CAMERA_MOVE_DOWN: moving_down = true; break;
		case EventType::CAMERA_MOVE_FORWARD_ONCE: moving_forward_once = true; break;
		case EventType::CAMERA_MOVE_BACKWARD_ONCE: moving_backward_once = true; break;
		// Rotational movement
		case EventType::CAMERA_ROTATE_UP: rotating_up = true; break;
		case EventType::CAMERA_ROTATE_DOWN: rotating_down = true; break;
		case EventType::CAMERA_ROTATE_LEFT: rotating_left = true; break;
		case EventType::CAMERA_ROTATE_RIGHT: rotating_right = true; break;
		// Spatial movement
		case EventType::STOP_CAMERA_MOVE_UP: moving_up = false; break;
		case EventType::STOP_CAMERA_MOVE_DOWN: moving_down = false; break;
		case EventType::STOP_CAMERA_MOVE_FORWARD: moving_forward = false; break;
		case EventType::STOP_CAMERA_MOVE_BACKWARD: moving_backward = false; break;

		// Rotational movement
		case EventType::STOP_CAMERA_ROTATE_UP: rotating_up = false; break;
		case EventType::STOP_CAMERA_ROTATE_DOWN: rotating_down = false; break;
		case EventType::STOP_CAMERA_ROTATE_LEFT: rotating_left = false; break;
		case EventType::STOP_CAMERA_ROTATE_RIGHT: rotating_right = false; break;
		// We don't care about this event, get another
		default: processing = true; break;
		}
	}
}