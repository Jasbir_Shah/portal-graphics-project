#include "relativity/core/window.hxx"

#include <glfw\glfw3.h>
#include <iostream>
#include <memory>

#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"

#ifndef _CALLBACK_SWITCHES_
#define KEY_SWITCH(key, action, button, state, caller, event_type) (key == button && action == state) { \
	const auto event = Event(caller, event_type); \
	get_logger()->info("Event registered: {} from {}", #event_type, caller); \
	notify(event); \
}

#define SCROLL_SWITCH(x, y, caller, event_type) { \
	const auto event = Event(caller, event_type); \
	get_logger()->info("Event registered: {} from {}", #event_type, caller); \
	notify(event); \
}
#endif

using namespace relativity::core;
using namespace relativity::event;
using namespace std;

Window::Window(const int width, const int height, const string& title): WindowWrapper(width, height, title), logger(spdlog::get("window")) {
	// Do nothing
}

Window::~Window() {
	// Do nothing
}

void Window::key_callback(const int key, const int scancode, const int action, const int mods) {

	//camera move
	if KEY_SWITCH(key, action, GLFW_KEY_ESCAPE,		GLFW_PRESS,	  0, EventType::CLOSE_WINDOW)
	else if KEY_SWITCH(key, action, GLFW_KEY_Q, GLFW_PRESS, 0, EventType::CAMERA_MOVE_UP)
	else if KEY_SWITCH(key, action, GLFW_KEY_E, GLFW_PRESS, 0, EventType::CAMERA_MOVE_DOWN)
	else if KEY_SWITCH(key, action, GLFW_KEY_Q, GLFW_RELEASE, 0, EventType::STOP_CAMERA_MOVE_UP)
	else if KEY_SWITCH(key, action, GLFW_KEY_E, GLFW_RELEASE, 0, EventType::STOP_CAMERA_MOVE_DOWN)
	
	//camera rotate
	else if KEY_SWITCH(key, action, GLFW_KEY_LEFT, GLFW_PRESS, 0, EventType::CAMERA_ROTATE_LEFT)
	else if KEY_SWITCH(key, action, GLFW_KEY_RIGHT, GLFW_PRESS, 0, EventType::CAMERA_ROTATE_RIGHT)
	else if KEY_SWITCH(key, action, GLFW_KEY_LEFT, GLFW_RELEASE, 0, EventType::STOP_CAMERA_ROTATE_LEFT)
	else if KEY_SWITCH(key, action, GLFW_KEY_RIGHT, GLFW_RELEASE, 0, EventType::STOP_CAMERA_ROTATE_RIGHT)

	//player move
	else if KEY_SWITCH(key, action, GLFW_KEY_SPACE, GLFW_PRESS, 0, EventType::PLAYER_JUMP)
	else if KEY_SWITCH(key, action, GLFW_KEY_W, GLFW_PRESS, 0, EventType::PLAYER_MOVE_FORWARD)
	else if KEY_SWITCH(key, action, GLFW_KEY_A, GLFW_PRESS, 0, EventType::PLAYER_ROTATE_LEFT)
	else if KEY_SWITCH(key, action, GLFW_KEY_S, GLFW_PRESS, 0, EventType::PLAYER_MOVE_BACKWARD)
	else if KEY_SWITCH(key, action, GLFW_KEY_D, GLFW_PRESS, 0, EventType::PLAYER_ROTATE_RIGHT)
	else if KEY_SWITCH(key, action, GLFW_KEY_W, GLFW_RELEASE, 0, EventType::STOP_PLAYER_MOVE_FORWARD)
	else if KEY_SWITCH(key, action, GLFW_KEY_A, GLFW_RELEASE, 0, EventType::STOP_PLAYER_ROTATE_LEFT)
	else if KEY_SWITCH(key, action, GLFW_KEY_S, GLFW_RELEASE, 0, EventType::STOP_PLAYER_MOVE_BACKWARD)
	else if KEY_SWITCH(key, action, GLFW_KEY_D, GLFW_RELEASE, 0, EventType::STOP_PLAYER_ROTATE_RIGHT)

	//other stuff
	else if KEY_SWITCH(key, action, GLFW_KEY_UP, GLFW_PRESS, 0, EventType::INCREASE_PORTAL_DEPTH)
	else if KEY_SWITCH(key, action, GLFW_KEY_DOWN, GLFW_PRESS, 0, EventType::DECREASE_PORTAL_DEPTH)
	else if KEY_SWITCH(key, action, GLFW_KEY_1,		GLFW_PRESS,	  0, EventType::TOGGLE_AMBIENT)
	else if KEY_SWITCH(key, action, GLFW_KEY_2,		GLFW_PRESS,	  0, EventType::TOGGLE_DIFFUSE)
	else if KEY_SWITCH(key, action, GLFW_KEY_3,		GLFW_PRESS,	  0, EventType::TOGGLE_SPECULAR)
	else if KEY_SWITCH(key, action, GLFW_KEY_4,		GLFW_PRESS,	  0, EventType::TOGGLE_FALLOFF)
	else if KEY_SWITCH(key, action, GLFW_KEY_5,		GLFW_PRESS,	  0, EventType::TOGGLE_SHADOW)
	else if KEY_SWITCH(key, action, GLFW_KEY_6,		GLFW_PRESS,	  0, EventType::TOGGLE_MAT_DIFFUSE)
	else if KEY_SWITCH(key, action, GLFW_KEY_7,		GLFW_PRESS,	  0, EventType::TOGGLE_MAT_NORMAL)
	else if KEY_SWITCH(key, action, GLFW_KEY_8,		GLFW_PRESS,	  0, EventType::TOGGLE_MAT_SPECULAR)
	else if KEY_SWITCH(key, action, GLFW_KEY_F,		GLFW_PRESS,	  0, EventType::TOGGLE_FOG)
	else if KEY_SWITCH(key, action, GLFW_KEY_P,		GLFW_PRESS,	  0, EventType::TOGGLE_PHONG)
	else if KEY_SWITCH(key, action, GLFW_KEY_G,		GLFW_PRESS,	  0, EventType::TOGGLE_GAMMA)
	else if KEY_SWITCH(key, action, GLFW_KEY_R,		GLFW_PRESS,	  0, EventType::RELOAD_PORTALS)
	else if KEY_SWITCH(key, action, GLFW_KEY_Z,		GLFW_PRESS,	  0, EventType::SWITCH_COLOUR)
	else if KEY_SWITCH(key, action, GLFW_KEY_X,		GLFW_PRESS,	  0, EventType::SWITCH_NORMAL)
	else if KEY_SWITCH(key, action, GLFW_KEY_C,		GLFW_PRESS,	  0, EventType::SWITCH_POSITION)
	else if KEY_SWITCH(key, action, GLFW_KEY_V,		GLFW_PRESS,	  0, EventType::SWITCH_COLLISION)
	else if KEY_SWITCH(key, action, GLFW_KEY_B,		GLFW_PRESS,	  0, EventType::SWITCH_LIGHT)
}

void Window::scroll_callback(const double x, const double y) {
	if (y >= 0.9f) SCROLL_SWITCH(x, y, 0, EventType::CAMERA_MOVE_FORWARD_ONCE)
	else if (y <= -0.9f) SCROLL_SWITCH(x, y, 0, EventType::CAMERA_MOVE_BACKWARD_ONCE)
}