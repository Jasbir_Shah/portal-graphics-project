#define GLM_FORCE_RADIANS

#include "relativity/core/engine.hxx"
#include "relativity/entity/entity.h"
#include "relativityUtils/createPortalsFromFile.hxx"

#include <glm/glm.hpp>

using namespace relativity::core;
using namespace relativity::config;
using namespace relativity::entity;
using namespace relativity::event;
using namespace relativity::graphics;
using namespace relativity::physics;
using namespace std;

Engine::Engine(const std::string& source_dir, const shared_ptr<Settings> settings):
		settings(settings), 
		start_time(chrono::high_resolution_clock::now()),
		logger(spdlog::stderr_logger_mt("engine", true)),
		physics_engine(make_shared<PhysicsEngine>()),
		event_queue(make_shared<EventQueue>()) {	
	
	get_logger()->info("Creating engine");
	event_queue->subscribe(graphics_engine);
	event_queue->subscribe(physics_engine);
	started = true;

	// Try to initialise GLFW
	if (initialise_glfw()) {
		event_queue->subscribe(window);
		glfwMakeContextCurrent(window->get());
	}
	else {
		glfwTerminate();
		started = false;
	}

	// Try to initialise OpenGL
	if (initialise_open_gl()) {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_MULTISAMPLE);
		graphics_pool = make_shared<GraphicsComponentPool>(source_dir);
			
		stairs = make_shared<Entity>(graphics_pool->construct(ComponentType::STAIRS));
		player = make_shared<Entity>(graphics_pool->construct(ComponentType::PLAYER));;
		const auto exit = make_shared<Entity>(graphics_pool->construct(ComponentType::EXIT));;

		auto stairs_mesh = stairs->get_graphics_component()->get_meshes().at(0);
		stairs_mesh->set_model(
			glm::translate(
				stairs_mesh->get_model(),
				glm::vec3(0.0f, 0.0f, 0.0f)
			)
		);

		auto player_mesh = player->get_graphics_component()->get_meshes().at(0);

		player->set_position(glm::vec3(0.f, 200.f, -100.f));
		player->set_world_position(player->get_position());

		player_mesh->set_model(
			glm::translate(player_mesh->get_model(), player->get_position())
		);
		
		auto exit_mesh = exit->get_graphics_component()->get_meshes().at(0);

		exit_mesh->set_model(
			glm::translate(exit_mesh->get_model(), glm::vec3(230.8, 70, -401))
		);

		entities.push_back(exit);
		entities.push_back(stairs);
		entities.push_back(player);

		portals = utils::load_portals_from_file(settings, source_dir, graphics_pool);
		get_logger()->info("Portals loaded: {}", portals.size());

		graphics_engine = make_shared<GraphicsEngine>(
			make_shared<Entity>(graphics_pool->construct(ComponentType::QUAD)),
			event_queue,
			settings->get_window_width(), settings->get_window_height(),
			1024, 1024, player
		);
	}
	else {
		started = false;
	}
}

Engine::~Engine() {
	// Finalize and clean up GLFW  
	glfwTerminate();
}

void Engine::update(const double time_delta) {
	get_physics()->update(time_delta, entities, portals, player, stairs);
	get_graphics()->update(
		time_delta, 
		entities, portals, player,
		settings->get_window_width(),
		settings->get_window_height(),
		window
	);
}

void Engine::process_events() {
	glfwPollEvents();

	bool processing = true;
	while (processing) {
		processing = false;
		const auto event = get_window()->poll();

		switch (event.get_type()) {
		case EventType::NO_EVENT:
			break;
		// Close the window
		case EventType::CLOSE_WINDOW: 
			glfwSetWindowShouldClose(get_window()->get(), GL_TRUE);
			break;
		// We don't care about this event, get another
		case EventType::RELOAD_PORTALS:
			portals = utils::load_portals_from_file(settings, "resources/", graphics_pool);
		default:
			graphics_engine->notify(event);
			physics_engine->notify(event);
			processing = true; break;
		}
	}
}

void Engine::run() {
	using namespace chrono;
	auto prev_frame = high_resolution_clock::now();

	while (!glfwWindowShouldClose(get_window()->get())) {
		const auto curr_frame = high_resolution_clock::now();
		const auto time_delta = duration_cast<duration<double>>(curr_frame - prev_frame).count();
		process_events();
		update(time_delta);
		prev_frame = curr_frame;
	}
}

bool Engine::initialise_glfw() {
	bool initialised = glfwInit();
	get_logger()->info("Initialising GLFW");
	
	// Create the main window
	window = make_shared<Window>(
		settings->get_window_width(),
		settings->get_window_height(),
		"Relativity Engine"
	);

	initialised = initialised && window->get();

	if (!initialised) {
		get_logger()->critical("Failed to initialise GLFW.");
	}

	return initialised;
}

bool Engine::initialise_open_gl() {
	get_logger()->info("Initialising GLEW");

	// Use experimental GL features
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	// If GLEW hasn't initialized 
	if (err != GLEW_OK) {
		get_logger()->critical("Error: {}", glewGetErrorString(err));
	}

	return (err == GLEW_OK); 
}
