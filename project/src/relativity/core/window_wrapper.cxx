#include "relativity/core/window_wrapper.hxx"

#include <glfw/glfw3.h>
#include <iostream>

using namespace relativity::core;
using namespace std;

shared_ptr<spdlog::logger> WindowWrapper::LOGGER = spdlog::daily_logger_mt("window", "logs/window");

WindowWrapper::WindowWrapper(const int width, const int height, const string& title) {
	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_RESIZABLE, 0);

	window = glfwCreateWindow(
		width,
		height,
		title.c_str(),
		nullptr, nullptr
	);

	glfwSetWindowUserPointer(window, this);
	glfwSetErrorCallback(WindowWrapper::s_error_callback);
	glfwSetKeyCallback(window, WindowWrapper::s_key_callback);
	glfwSetScrollCallback(window, WindowWrapper::s_scroll_callback);
}

WindowWrapper::~WindowWrapper() {
	glfwDestroyWindow(get());
}

void WindowWrapper::s_error_callback(int error, const char * description) {
	LOGGER->error(description);
}

void WindowWrapper::s_key_callback(GLFWwindow * window, int key, int scancode, int action, int mods) {
	auto handle = glfwGetWindowUserPointer(window);
	auto caller = static_cast<WindowWrapper*>(handle);

	caller->key_callback(key, scancode, action, mods);
}


void WindowWrapper::s_scroll_callback(GLFWwindow * window, double x, double y) {
	auto handle = glfwGetWindowUserPointer(window);
	auto caller = static_cast<WindowWrapper*>(handle);

	caller->scroll_callback(x, y);
}