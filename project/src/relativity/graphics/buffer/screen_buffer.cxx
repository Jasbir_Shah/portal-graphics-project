#include "relativity/core/gl_error.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/graphics/buffer/screen_buffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <string>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

ScreenBuffer::ScreenBuffer(const string& vertex_shader_source, const string& fragment_shader_source, 
	const GLuint width, const GLuint height, const GLuint source_texture): 
		Framebuffer(false), source_texture(source_texture) {
	// Create a texture to render the framebuffer's contents to
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create a render buffer for storing the depth and stencil values
	glGenRenderbuffers(1, &render_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, render_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	// Attach the render buffer to the framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, render_buffer);

	// Attach the texture to the framebuffer
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	
	GL_CHECK;
	check();

	// Configure shaders
	shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	shader.add_attribute(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
	shader.add_attribute(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
	shader.link();

	// Reactivate the default render buffer and framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

ScreenBuffer::~ScreenBuffer() {
	glDeleteRenderbuffers(1, &render_buffer);
	glDeleteTextures(1, &texture);
}

void ScreenBuffer::draw(const GLuint width, const GLuint height, 
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer,
	const shared_ptr<Light>& light
) {
	glViewport(0, 0, width, height);
	GL_CHECK;

	shader.bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, source_texture);
	shader.bind_uniform<GLint>("view_map", 0);
	GL_CHECK;

	for (const auto& entity : entities) {
		for (const auto& mesh : entity->get_graphics_component()->get_meshes()) {
			shader.draw(mesh);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_CHECK;
}