#include "relativity/core/gl_error.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"

#include <gl/glew.h> 
#include <spdlog/spdlog.h>

#include <memory>
#include <functional>
#include <string>

using namespace relativity::core;
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

int Framebuffer::MAX_DEPTH = 0;

Framebuffer::Framebuffer(const bool is_multisampled): multisampled(is_multisampled), 
		logger(spdlog::get("framebuffer")) {
	// Generate a framebuffer and make it the active buffer
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	GL_CHECK;
}

Framebuffer::~Framebuffer() {
	glDeleteFramebuffers(1, &framebuffer);
}

void Framebuffer::bind() {
	glBindFramebuffer(GL_READ_FRAMEBUFFER, get());
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, get());
	GL_CHECK;
}

void Framebuffer::bind(const shared_ptr<Framebuffer>& target_buffer) {
	glBindFramebuffer(GL_READ_FRAMEBUFFER, get());

	(target_buffer)
		? glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target_buffer->get())
		: glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	GL_CHECK;
}

void Framebuffer::bind(
	const shared_ptr<Framebuffer>& source_buffer,
	const shared_ptr<Framebuffer>& target_buffer
) {
	(source_buffer)
		? glBindFramebuffer(GL_READ_FRAMEBUFFER, source_buffer->get())
		: glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	(target_buffer)
		? glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target_buffer->get())
		: glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	GL_CHECK;
}

/**
 * There's a lot of things that can go wrong, so we check *most* of the possibilities
 */
bool Framebuffer::check() {
	bool is_complete = false;
	GLuint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	switch (status) {
	case GL_FRAMEBUFFER_COMPLETE:
		get_logger()->info("Framebuffer bound");
		is_complete = true;
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		get_logger()->error("Framebuffer has an incomplete attachment");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		get_logger()->error("Framebuffer has missing attachments");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		get_logger()->error("Framebuffer has mismatched dimensions with its attached buffers");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
		get_logger()->error("Framebuffer is using an unsupported format");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		get_logger()->error("Framebuffer has an incomplete draw buffer");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		get_logger()->error("Framebuffer has an incomplete read buffer");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
		get_logger()->error("Framebuffer textures have mismatched multisample rates");
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
		get_logger()->error("Framebuffer is using at least one layered texture, therefore all textures must be layered, this is not the case");
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		get_logger()->error("Framebuffer format is unsupported");
		break;

	default:
		get_logger()->error("Framebuffer could not be bound because of an unknown error");
		break;
	}

	GL_CHECK;
	return is_complete;
}