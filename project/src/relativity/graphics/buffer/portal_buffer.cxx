#include "relativity/core/gl_error.hxx"
#include "relativity/core/gl_types.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/graphics/buffer/portal_buffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp>
#include <glm/glm.hpp>

#include <memory>
#include <string>

using namespace relativity::core;
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

PortalBuffer::PortalBuffer(const string& vertex_shader_source, const string& fragment_shader_source,
	const string& simple_vertex_shader_source, const string& simple_fragment_shader_source,
	const GLuint width, const GLuint height, const shared_ptr<Framebuffer> geometry_buffer,
	const shared_ptr<Framebuffer> light_buffer,	const shared_ptr<Entity> quad,
	const bool is_multisampled, const GLsizei max_samples):
	Framebuffer(is_multisampled), geometry_buffer(geometry_buffer), light_buffer(light_buffer), 
		quad(quad) {
	const GLenum texture_type = (is_multisampled) ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
	const GLsizei samples = (max_samples < GL_MAX_COLOR_TEXTURE_SAMPLES) ? max_samples : GL_MAX_COLOR_TEXTURE_SAMPLES;

	// Create a texture to render the framebuffer's contents to
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	(is_multisampled)
		? glTexImage2DMultisample(texture_type, samples, GL_RGB, width, height, GL_TRUE)
		: glTexImage2D(texture_type, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create a render buffer for storing the depth and stencil values
	glGenRenderbuffers(1, &render_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, render_buffer);
	
	(is_multisampled)
		? glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH_STENCIL, width, height)
		: glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL, width, height);

	// Attach the render buffer to the framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, render_buffer);

	// Attach the texture to the framebuffer
	glBindTexture(texture_type, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture_type, texture, 0);
	
	GL_CHECK;
	check();

	// Configure texture shader
	texture_shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	texture_shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	texture_shader.add_attribute(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
	texture_shader.add_attribute(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
	texture_shader.link();

	// Configure simple shader
	simple_shader.add_shader("vertex", simple_vertex_shader_source, GL_VERTEX_SHADER);
	simple_shader.add_shader("fragment", simple_fragment_shader_source, GL_FRAGMENT_SHADER);
	simple_shader.add_attribute(0, 3, GL_FLOAT, GL_FALSE, 17, 0);
	simple_shader.link();

	// Reactivate the default render buffer and framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

PortalBuffer::~PortalBuffer() {
	glDeleteRenderbuffers(1, &render_buffer);
	glDeleteTextures(1, &texture);
}

void PortalBuffer::draw(const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer,
	const shared_ptr<Light>& light
) {
	// We need to blit the buffer output to the target buffer when multisampling
	if (is_multisampled()) {
		glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}

	glViewport(0, 0, width, height);
	draw_portal(0, width, height, entities, portals, target_buffer, light, view, proj);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_CHECK;
}

void PortalBuffer::draw_portal(const int depth, const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const shared_ptr<Framebuffer>& target_buffer,
	const shared_ptr<Light>& light, 
	const glm::mat4& view, const glm::mat4& proj
) {
	for (const auto portal : portals) {
		// 1. Draw the portal into the stencil buffer
		// Disable writing to the colour and depth buffers
		bind(geometry_buffer, geometry_buffer);
		enable_mask<GL::Stencil, GL::Depth>(true);
		enable_test<GL::Stencil>(true);
		enable_test<GL::Depth>(false);
		enable_mask<GL::Depth, GL::Color>(false);
		// Enable stencil testing and fail the test when we're inside the portal region.
		// At depth 0 this increments stencil value where the portal is normally. 
		// Past depth 0 it increments the stencil value only when it's inside the outer portal 
		// and inner portal.
		// If the fragment is inside our portal's region but the stencil value is equal to the current
		// depth then that fragment is inside the outer portal (and hence makes the inner portal).
		glStencilFunc(GL_NOTEQUAL, depth, 0xFF);
		glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

		// Draw the portal to the stencil buffer from the outer camera. 
		// This is just what we've said above.
		draw_meshes(simple_shader, { portal }, view, proj);

		bind(light_buffer, light_buffer);
		enable_mask<GL::Stencil, GL::Depth>(true);
		enable_test<GL::Stencil>(true);
		enable_test<GL::Depth>(false);
		enable_mask<GL::Depth, GL::Color>(false);
		glStencilFunc(GL_NOTEQUAL, depth, 0xFF);
		glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);
		draw_meshes(simple_shader, { portal }, view, proj);

		// We now need to draw what's inside of the inner portal, a.k.a. it will either become the new
		// outer portal, or we've reached the maximum depth and will draw the scene from the portal's
		// viewpoint.
		// To calculate this we just multiply our current view matrix by the portal's modelview matrix.
		const auto portal_view = view
			* portal.second->get_model()
			* glm::rotate(
				glm::mat4(),
				glm::radians(180.f),
				glm::vec3(0.f, 1.f, 0.f) * portal.second->get_quaternion()
			)
			* glm::inverse(portals.at(portal.second->get_exit())->get_model());

		const auto portal_proj = portal.second->project(portal_view, proj);

		// If we've reached the maximum depth, draw the innermost portal from the inner portal's viewpoint
		if (depth == get_max_depth()) {
			// Draw the scene from the portal's view
			// Enable drawing to the colour and depth buffers.
			// We're now going to draw the scene from the portal's viewpoint within the stencil area we 
			// just incremented, the inner portal as clipped by the outer portal.
			// Disable writing to the stencil buffer, but enable stencil testing as we now
			// only want to draw the scene as viewed by the inner portal as clipped by the outer portal.
			geometry_buffer->bind();
			geometry_buffer->enable_mask<GL::Color>(true);
			geometry_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
			geometry_buffer->enable_test<GL::Depth, GL::Stencil>(true);
			geometry_buffer->enable_mask<GL::Stencil>(false);

			// Clear the depth buffer. We'll need to perform another depth test in the outer portal
			// to make sure the inner portal is occluded properly by external objects, but for now 
			// none of them should have any effect on what's seen by the inner portal itself.
			geometry_buffer->clear<GL::Depth>();

			// We failed the stencil test where stencil = depth, and incremented its value, hence
			// we want to only write to areas that are stencil = depth + 1.
			glStencilFunc(GL_EQUAL, depth + 1, 0xFF);

			geometry_buffer->draw(
				width, height, entities, portals,
				portal_view, portal_proj,
				geometry_buffer, light
			);

			light_buffer->bind(target_buffer);
			light_buffer->enable_mask<GL::Color>(true);
			light_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
			light_buffer->enable_test<GL::Depth>(false);
			light_buffer->enable_test<GL::Stencil>(true);
			light_buffer->enable_mask<GL::Depth, GL::Stencil>(false);
			glStencilFunc(GL_EQUAL, depth + 1, 0xFF);

			light_buffer->draw(
				width, height, { quad }, portals, 
				portal_view, portal_proj, 
				light_buffer, light
			);
			light_buffer->enable_test<GL::Depth>(true);
		}
		// Otherwise, continue drawing inner portals to the stencil buffer
		else {
			draw_portal(
				width, height, depth + 1, 
				entities, portals, target_buffer, 
				light, portal_view, portal_proj
			);
		}

		// We're now going to decrement the stencil value we incremented so that the next portal isn't 
		// affected. This has the effect of unstenciling the inner portal but keeping the outer portal.
		bind(geometry_buffer, geometry_buffer);
		enable_mask<GL::Color, GL::Depth>(false);
		enable_mask<GL::Stencil>(true);
		enable_test<GL::Stencil>(true);

		// Fail the stencil test when we're inside the inner portal that we just drew to.
		glStencilFunc(GL_NOTEQUAL, depth + 1, 0xFF);
		glStencilOp(GL_DECR, GL_KEEP, GL_KEEP);

		// Draw the portal.
		draw_meshes(simple_shader, { portal }, view, proj);

		bind(light_buffer, light_buffer);
		enable_mask<GL::Color, GL::Depth>(false);
		enable_mask<GL::Stencil>(true);
		enable_test<GL::Stencil>(true);

		// Fail the stencil test when we're inside the inner portal that we just drew to.
		glStencilFunc(GL_NOTEQUAL, depth + 1, 0xFF);
		glStencilOp(GL_DECR, GL_KEEP, GL_KEEP);

		// Draw the portal.
		draw_meshes(simple_shader, { portal }, view, proj);
	}

	bind(geometry_buffer);
	enable_mask<GL::Color>(false);
	enable_mask<GL::Depth, GL::Stencil>(true);
	enable_test<GL::Depth>(true);
	enable_test<GL::Stencil>(false);
	enable_mask<GL::Stencil>(false);
	glDepthFunc(GL_ALWAYS);
	clear<GL::Depth>();

	draw_meshes(simple_shader, portals, view, proj);

	bind(light_buffer);
	enable_mask<GL::Color>(false);
	enable_mask<GL::Depth, GL::Stencil>(true);
	enable_test<GL::Depth>(true);
	enable_test<GL::Stencil>(false);
	enable_mask<GL::Stencil>(false);
	glDepthFunc(GL_ALWAYS);
	clear<GL::Depth>();

	draw_meshes(simple_shader, portals, view, proj);

	// Re-enable all testing but disable writing to the stencil buffer
	// Draw the scene from the portal's view
	geometry_buffer->bind();
	geometry_buffer->enable_mask<GL::Color>(true);
	geometry_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	geometry_buffer->enable_test<GL::Depth, GL::Stencil>(true);
	glDepthFunc(GL_LESS);
	glStencilFunc(GL_LEQUAL, depth, 0xFF);
	geometry_buffer->enable_mask<GL::Stencil>(false);
	geometry_buffer->draw(width, height, entities, portals, view, proj, geometry_buffer, light);

	light_buffer->bind(target_buffer);
	light_buffer->enable_mask<GL::Color>(true);
	light_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	light_buffer->enable_test<GL::Depth>(false);
	light_buffer->enable_test<GL::Stencil>(true);
	glStencilFunc(GL_LEQUAL, depth, 0xFF);
	light_buffer->enable_mask<GL::Depth, GL::Stencil>(false);
	light_buffer->draw(width, height, { quad }, portals, view, proj, light_buffer, light);
}

void PortalBuffer::draw_meshes(Shader& shader, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj) {
	shader.bind();
	shader.bind_uniform<glm::mat4>("view", view);
	shader.bind_uniform<glm::mat4>("proj", proj);

	for (const auto& portal : portals) {
		shader.bind(portal.second);
		shader.draw(portal.second);
	}
}