#include "relativity/core/gl_error.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/graphics/buffer/geometry_buffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

GeometryBuffer::GeometryBuffer(const string& vertex_shader_source, const string& fragment_shader_source,
	const GLuint width, const GLuint height, const bool is_multisampled, const GLsizei max_samples):
		Framebuffer(is_multisampled) {

	const GLsizei samples = (max_samples < GL_MAX_COLOR_TEXTURE_SAMPLES) ? max_samples : GL_MAX_COLOR_TEXTURE_SAMPLES;

	auto create_texture = [is_multisampled, samples, width, height]
			(GLenum internal_format, GLenum format, GLenum type, GLenum index) {
		GLuint texture_buffer;
		const GLenum texture_type = (is_multisampled) ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
		// Create a texture to render the framebuffer's vertex positions to
		glGenTextures(1, &texture_buffer);
		glBindTexture(GL_TEXTURE_2D, texture_buffer);

		(is_multisampled)
			? glTexImage2DMultisample(texture_type, samples, internal_format, width, height, GL_TRUE)
			: glTexImage2D(texture_type, 0, internal_format, width, height, 0, format, type, nullptr);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Attach the texture to the framebuffer
		glFramebufferTexture2D(GL_FRAMEBUFFER, index, texture_type, texture_buffer, 0);
		return texture_buffer;
	};
	
	GLuint position_map = create_texture(GL_RGB16F, GL_RGB, GL_FLOAT, POSITION_MAP);
	GLuint normal_map = create_texture(GL_RGB16F, GL_RGB, GL_FLOAT, NORMAL_MAP);
	GLuint albedo_map = create_texture(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, ALBEDO_MAP);
	GLuint direction_map = create_texture(GL_RGB16F, GL_RGB, GL_FLOAT, DIRECTION_MAP);
	GLuint depth_map = create_texture(GL_RED, GL_RED, GL_FLOAT, DEPTH_MAP);
	get_attachments().insert(make_pair(POSITION_MAP, position_map));
	get_attachments().insert(make_pair(NORMAL_MAP, normal_map));
	get_attachments().insert(make_pair(ALBEDO_MAP, albedo_map));
	get_attachments().insert(make_pair(DIRECTION_MAP, direction_map));
	get_attachments().insert(make_pair(DEPTH_MAP, depth_map));

	int attached = 0;
	GLuint* draw_attachments= new GLuint[get_attachments().size()];

	for (const auto pair : get_attachments()) {
		draw_attachments[attached++] = pair.first;
	}

	glDrawBuffers(get_attachments().size(), draw_attachments);
	delete(draw_attachments);

	// Create a render buffer for storing the depth and stencil values
	glGenRenderbuffers(1, &depth_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
	
	(is_multisampled)
		? glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, width, height)
		: glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	// Attach the render buffer to the framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);

	GL_CHECK;
	check();

	// Configure shaders
	shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	shader.add_attribute(0, 3, GL_FLOAT, GL_FALSE, 17, 0);
	shader.add_attribute(1, 3, GL_FLOAT, GL_TRUE, 17, 6);
	shader.add_attribute(2, 3, GL_FLOAT, GL_FALSE, 17, 3);
	shader.add_attribute(3, 2, GL_FLOAT, GL_FALSE, 17, 9);
	shader.add_attribute(4, 3, GL_FLOAT, GL_FALSE, 17, 11);
	shader.add_attribute(5, 3, GL_FLOAT, GL_FALSE, 17, 14);
	shader.link();

	// Reactivate the default render buffer and framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

GeometryBuffer::~GeometryBuffer() {
	glDeleteRenderbuffers(1, &depth_buffer);

	for (const auto pair : get_attachments()) {
		glDeleteTextures(1, &pair.first);
	}
}

void GeometryBuffer::draw(
	const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer, const shared_ptr<Light>& light
) {
	glCullFace(GL_BACK);
	glViewport(0, 0, width, height);
	GL_CHECK; 

	shader.bind();
	shader.bind(*light);
	GL_CHECK;

	// Update the matrices and camera
	shader.bind_uniform<glm::mat4>("view", view);
	shader.bind_uniform<glm::mat4>("proj", proj);
	shader.bind_uniform<glm::mat4>("light_space", light->get_light_space());
	GL_CHECK;

	for (const auto& entity : entities) {
		for (const auto mesh : entity->get_graphics_component()->get_meshes()) {
			for (const auto submesh : mesh->get_submeshes()) {
				shader.bind(submesh);

				for (const auto texture : submesh->get_textures()) {
					texture->bind();
				}
				shader.draw(submesh);
			}
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_CHECK;
}