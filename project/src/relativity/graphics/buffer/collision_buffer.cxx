#include "relativity/core/gl_error.hxx"
#include "relativity/core/gl_types.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/graphics/buffer/collision_buffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <string>

using namespace relativity::core;
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

CollisionBuffer::CollisionBuffer(const string& vertex_shader_source, const string& fragment_shader_source,
	const string& simple_vertex_shader_source, const string& simple_fragment_shader_source,
	const GLuint width, const GLuint height, const GLuint depth_map, const shared_ptr<Entity> quad,
	const bool is_multisampled, const GLsizei max_samples):	
		Framebuffer(is_multisampled), depth_map(depth_map), quad(quad) {
	// Create a texture to render the framebuffer's contents to
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create a render buffer for storing the depth and stencil values
	glGenRenderbuffers(1, &render_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, render_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	// Attach the render buffer to the framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, render_buffer);

	// Attach the texture to the framebuffer
	GL_CHECK;
	glBindTexture(GL_TEXTURE_2D, 0);
	GL_CHECK;
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
	GL_CHECK;

	check();

	// Configure shaders
	box_shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	box_shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	box_shader.add_attribute(0, 3, GL_FLOAT, GL_FALSE, 3, 0);
	box_shader.link();

	// Configure simple shader
	simple_shader.add_shader("vertex", simple_vertex_shader_source, GL_VERTEX_SHADER);
	simple_shader.add_shader("fragment", simple_fragment_shader_source, GL_FRAGMENT_SHADER);
	simple_shader.add_attribute(0, 3, GL_FLOAT, GL_FALSE, 17, 0);
	simple_shader.link();

	// Reactivate the default render buffer and framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

CollisionBuffer::~CollisionBuffer() {
	glDeleteRenderbuffers(1, &render_buffer);
	glDeleteTextures(1, &texture);
}

void CollisionBuffer::draw(const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer, 
	const shared_ptr<Light>& light
) {
	// We need to blit the buffer output to the target buffer when multisampling
	if (is_multisampled()) {
		glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}
	GL_CHECK;

	glViewport(0, 0, width, height);
	GL_CHECK;

	// Bind depth map
	box_shader.bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_map);
	box_shader.bind_uniform<GLint>("depth_map", 0);
	GL_CHECK;

	draw_portal(0, entities, portals, view, proj);
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_CHECK;
}

void CollisionBuffer::draw_portal(const int depth, const EntityList& entities, const PortalMap& portals, 
		const glm::mat4& view, const glm::mat4& proj) {
	// For each portal render the scene from inside of it. To render recursively we render the scene
	// as viewed by each portal as viewed by each portal. This means the scene is rendered 
	// num_portals * num_portals * depth! A depth value past two is going to become slow extremely fast~
	// We can reduce the number of draws by using space-partitioning. We do not have enough PhDs for 
	// space-partitioning.
	for (const auto& portal : portals) {
		// 1. Draw the portal into the stencil buffer
		// Disable writing to the colour and depth buffers
		enable_mask<GL::Depth>(true);
		enable_test<GL::Depth>(false);
		enable_mask<GL::Depth, GL::Color>(false);
		// Enable stencil testing and fail the test when we're inside the portal region.
		// At depth 0 this increments stencil value where the portal is normally. 
		// Past depth 0 it increments the stencil value only when it's inside the outer portal 
		// and inner portal.
		// If the fragment is inside our portal's region but the stencil value is equal to the current
		// depth then that fragment is inside the outer portal (and hence makes the inner portal).
		enable_mask<GL::Stencil>(true);
		enable_test<GL::Stencil>(true);
		glStencilFunc(GL_NOTEQUAL, depth, 0xFF);
		glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

		// Draw the portal to the stencil buffer from the outer camera. 
		// This is just what we've said above.
		draw_meshes(simple_shader, { portal }, view, proj);

		// We now need to draw what's inside of the inner portal, a.k.a. it will either become the new
		// outer portal, or we've reached the maximum depth and will draw the scene from the portal's
		// viewpoint.
		// To calculate this we just multiply our current view matrix by the portal's modelview matrix.
		const auto orientation = portal.second->get_quaternion();
		const auto entrance_model = portal.second->get_model();
		const auto exit_model = portals.at(portal.second->get_exit())->get_model();
		const auto portal_view = view
			* entrance_model
			* glm::rotate(
				glm::mat4(),
				glm::radians(180.f),
				glm::vec3(0.f, 1.f, 0.f) * orientation
			)
			* glm::inverse(exit_model);

		const auto portal_proj = portal.second->project(portal_view, proj);

		// If we've reached the maximum depth, draw the innermost portal from the inner portal's viewpoint
		if (depth == get_max_depth()) {
			// Enable drawing to the colour and depth buffers.
			// We're now going to draw the scene from the portal's viewpoint within the stencil area we 
			// just incremented, the inner portal as clipped by the outer portal.
			enable_mask<GL::Color, GL::Depth>(true);
			// Clear the depth buffer. We'll need to perform another depth test in the outer portal
			// to make sure the inner portal is occluded properly by external objects, but for now 
			// none of them should have any effect on what's seen by the inner portal itself.
			clear<GL::Depth>();
			enable_test<GL::Depth>(true);

			// Disable writing to the stencil buffer, but enable stencil testing as we now
			// only want to draw the scene as viewed by the inner portal as clipped by the outer portal.
			enable_mask<GL::Stencil>(true);
			enable_test<GL::Stencil>(true);
			// We failed the stencil test where stencil = depth, and incremented its value, hence
			// we want to only write to areas that are stencil = depth + 1.
			glStencilFunc(GL_EQUAL, depth + 1, 0xFF);
			enable_mask<GL::Stencil>(false);

			// Draw the scene from the portal's view with a new projection matrix that has been adjusted 
			// to not clip anything directly in front of the portal. The math behind this is rather complex,
			// so I won't bother explaining it: http://www.terathon.com/lengyel/Lengyel-Oblique.pdf
			draw_boxes(
				box_shader, entities, portals,
				portal_view, portal_proj
			);
		}
		// If we haven't reached the maximum depth, render the scene for each portal again but use this
		// new inner portal as the outer portal in the next depth level. 
		else {
			draw_portal(
				depth + 1, entities, portals, 
				portal_view, portal_proj
			);
		}

		// We're now going to decrement the stencil value we incremented so that the next portal isn't 
		// affected. This has the effect of unstenciling the inner portal but keeping the outer portal.
		enable_mask<GL::Color, GL::Depth>(false);
		enable_mask<GL::Stencil>(true);
		enable_test<GL::Stencil>(true);

		// Fail the stencil test when we're inside the inner portal that we just drew to.
		glStencilFunc(GL_NOTEQUAL, depth + 1, 0xFF);
		glStencilOp(GL_DECR, GL_KEEP, GL_KEEP);

		// Draw the portal.
		draw_meshes(simple_shader, { portal }, view, proj);
	}

	// Finish off by drawing the outer portal
	// Draw each portal to the depth buffer first
	enable_mask<GL::Depth, GL::Stencil>(true);
	glStencilFunc(GL_EQUAL, depth, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	enable_test<GL::Depth, GL::Stencil>(true);
	enable_mask<GL::Color>(false);
	glDepthFunc(GL_ALWAYS);
	clear<GL::Depth>();
	
	draw_meshes(simple_shader, portals, view, proj);

	glDepthFunc(GL_LESS);
	// Re-enable all testing but disable writing to the stencil buffer
	// Draw the scene from the portal's view
	enable_mask<GL::Color>(true);
	enable_mask<GL::Depth, GL::Stencil>(true);
	enable_test<GL::Depth, GL::Stencil>(true);
	glStencilFunc(GL_LEQUAL, depth, 0xFF);
	enable_mask<GL::Stencil>(false);

	if (depth == 0 && get_max_depth() > 0) {
		draw_boxes(box_shader, {}, portals, view, proj);
	}
	else {
		draw_boxes(box_shader, entities, portals, view, proj);
	}
}

void CollisionBuffer::draw_boxes(Shader& shader, const EntityList& entities, const PortalMap& portals,
		const glm::mat4& view, const glm::mat4& proj) {
	shader.bind();
	shader.bind_uniform<glm::mat4>("view", view);
	shader.bind_uniform<glm::mat4>("proj", proj);

	for (const auto& entity : entities) {
		for (const auto& mesh : entity->get_graphics_component()->get_meshes()) {
			for (const auto& submesh : mesh->get_submeshes()) {
				if (submesh->has_bounding_box()) {
					shader.bind(submesh);
					shader.draw(submesh, true);
				}
			}
		}
	}
	for (const auto& portal : portals) {
		for (const auto& mesh : portal.second->get_mesh()->get_meshes()) {
			shader.bind_uniform<glm::mat4>("model", portal.second->get_model());
			
			for (const auto& submesh : mesh->get_submeshes()) {
				if (submesh->has_bounding_box()) {
					shader.draw(submesh, true);
				}
			}
		}
	}
}

void CollisionBuffer::draw_meshes(Shader& shader, const PortalMap& portals,
		const glm::mat4& view, const glm::mat4& proj) {
	shader.bind();
	shader.bind_uniform<glm::mat4>("view", view);
	shader.bind_uniform<glm::mat4>("proj", proj);
	
	for (const auto& portal : portals) {
		shader.bind(portal.second);
		shader.draw(portal.second);
	}
}