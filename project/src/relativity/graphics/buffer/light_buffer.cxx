#include "relativity/core/gl_error.hxx"
#include "relativity/core/gl_types.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/graphics/buffer/geometry_buffer.hxx"
#include "relativity/graphics/buffer/light_buffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp> 

#include <memory>
#include <string>

using namespace relativity::core;
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

LightBuffer::LightBuffer(const string& vertex_shader_source, const string& fragment_shader_source,
	const GLuint width, const GLuint height, const shared_ptr<Framebuffer> geometry_buffer,
	const bool is_multisampled, const GLsizei max_samples):
		Framebuffer(is_multisampled), geometry_buffer(geometry_buffer) {
	const GLenum texture_type = (is_multisampled) ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
	const GLsizei samples = (max_samples < GL_MAX_COLOR_TEXTURE_SAMPLES) ? max_samples : GL_MAX_COLOR_TEXTURE_SAMPLES;

	// Create a texture to render the framebuffer's contents to
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	(is_multisampled)
		? glTexImage2DMultisample(texture_type, samples, GL_RGB, width, height, GL_TRUE)
		: glTexImage2D(texture_type, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create a render buffer for storing the depth and stencil values
	glGenRenderbuffers(1, &render_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, render_buffer);
	
	(is_multisampled)
		? glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH24_STENCIL8, width, height)
		: glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	// Attach the render buffer to the framebuffer
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, render_buffer);

	// Attach the texture to the framebuffer
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture_type, texture, 0);
	GL_CHECK;

	check();

	// Configure shaders
	shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	shader.add_attribute(0, 2, GL_FLOAT, GL_FALSE, 4, 0);
	shader.add_attribute(1, 2, GL_FLOAT, GL_FALSE, 4, 2);
	shader.link();

	// Reactivate the default render buffer and framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

LightBuffer::~LightBuffer() {
	glDeleteRenderbuffers(1, &render_buffer);
	glDeleteTextures(1, &texture);
}

void LightBuffer::draw(const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer,
	const shared_ptr<Light>& light
) {
	// We need to blit the buffer output to the target buffer when multisampling
	if (is_multisampled()) {
		glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	}
	GL_CHECK;
	
	glCullFace(GL_BACK);
	glViewport(0, 0, width, height);
	GL_CHECK; 

	shader.bind();
	// Update the light
	shader.bind(*light);
	GL_CHECK;

	// Bind position map
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, geometry_buffer->get_attachments()[POSITION_MAP]);
	// Bind normal map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, geometry_buffer->get_attachments()[NORMAL_MAP]);
	// Bind albedo map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, geometry_buffer->get_attachments()[ALBEDO_MAP]);
	// Bind camera direction map
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, geometry_buffer->get_attachments()[DIRECTION_MAP]);
	GL_CHECK;

	shader.bind_uniform<GLint>("position_map", 0);
	shader.bind_uniform<GLint>("normal_map", 1);
	shader.bind_uniform<GLint>("albedo_map", 2);
	shader.bind_uniform<GLint>("direction_map", 3);

	for (const auto& entity : entities) {
		for (const auto mesh : entity->get_graphics_component()->get_meshes()) {
			for (const auto submesh : mesh->get_submeshes()) {
				shader.bind(submesh);
				shader.draw(submesh);
			}
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_CHECK;
}