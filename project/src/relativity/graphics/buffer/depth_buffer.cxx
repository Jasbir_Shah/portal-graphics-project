#include "relativity/core/gl_error.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/buffer/depth_buffer.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"

#include <gl/glew.h> 
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <string>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

DepthBuffer::DepthBuffer(const string& vertex_shader_source, const string& fragment_shader_source, 
	const GLuint width, const GLuint height, const bool is_multisampled, const GLsizei max_samples):
		Framebuffer(is_multisampled) {
	const GLenum texture_type = (is_multisampled) ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
	const GLsizei samples = (max_samples < GL_MAX_COLOR_TEXTURE_SAMPLES) ? 4 : GL_MAX_COLOR_TEXTURE_SAMPLES;
	
	// Create a texture to render the framebuffer's contents to
	glGenTextures(1, &texture);
	glBindTexture(texture_type, texture);

	(is_multisampled)
		? glTexImage2DMultisample(texture_type, samples, GL_DEPTH_COMPONENT, width, height, GL_TRUE)
		: glTexImage2D(texture_type, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if (!is_multisampled) {
		// Make the texture wraparound
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Anything outside the depth map shouldn't default as white
		GLfloat border_colour[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border_colour);
	}

	// Attach the texture to the framebuffer
	glBindTexture(texture_type, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture_type, texture, 0);
	
	// Disable unused framebuffer operations
	glReadBuffer(GL_NONE);
	glDrawBuffer(GL_NONE);

	GL_CHECK;
	check();

	// Configure shaders
	shader.add_shader("vertex", vertex_shader_source, GL_VERTEX_SHADER);
	shader.add_shader("fragment", fragment_shader_source, GL_FRAGMENT_SHADER);
	shader.add_attribute(0, 3, GL_FLOAT, GL_FALSE, 17, 0);
	shader.link();

	// Reactivate the default framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

DepthBuffer::~DepthBuffer() {
	glDeleteTextures(1, &texture);
}

void DepthBuffer::draw(const GLuint width, const GLuint height,
	const EntityList& entities, const PortalMap& portals,
	const glm::mat4& view, const glm::mat4& proj,
	const shared_ptr<Framebuffer>& target_buffer, const shared_ptr<Light>& light) {
	glBindFramebuffer(GL_READ_FRAMEBUFFER, get());

	(target_buffer)
		? glBindFramebuffer(GL_DRAW_FRAMEBUFFER, target_buffer->get())
		: glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	glCullFace(GL_FRONT);
	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_DEPTH_BUFFER_BIT);

	// Set matrices
	shader.bind();
	glUniformMatrix4fv(shader.get_uniform("light_space"), 1, GL_FALSE, glm::value_ptr(light->get_light_space()));

	for (const auto entity : entities) {
		for (const auto mesh : entity->get_graphics_component()->get_meshes()) {
			for (const auto submesh : mesh->get_submeshes()) {
				shader.bind(submesh);
				shader.draw(submesh);
			}
		}
	}
	GL_CHECK;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}