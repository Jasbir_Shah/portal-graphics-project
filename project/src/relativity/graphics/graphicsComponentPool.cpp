#include "relativity/graphics/graphicsComponentPool.h"
#include "relativity/entity/graphicscomponent.h"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/component_type.hxx"
#include "relativity/graphics/mesh_type.hxx"

#include <initializer_list>
#include <memory>
#include <vector>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace std;

GraphicsComponentPool::GraphicsComponentPool(const std::string& source_dir): source_dir(source_dir) {
	// We're going to create a quad for postprocessng
	std::vector<GLfloat> quad_data = {
		-1.0f,  1.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,

		-1.0f,  1.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 1.0f, 0.0f,
		1.0f,  1.0f, 1.0f, 1.0f
	};

	// Most of this can be refactored into two small methods
	auto mesh_quad = make_shared<Mesh>(quad_data, 6);
	auto mesh_quad_sub = make_shared<Mesh>(quad_data, 6);
	//auto mesh_handgun = make_shared<Mesh>(source_dir + "models/Handgun_obj.obj");
	auto mesh_stairs = make_shared<Mesh>(source_dir + "models/STAIRSMODULE3BASIC-82X.obj");
	auto mesh_player = make_shared<Mesh>(source_dir + "models/CHARACTERMODULE2FULL.obj");
	auto mesh_portal = make_shared<Mesh>(source_dir + "models/portal.obj");
	auto mesh_portal_large = make_shared<Mesh>(source_dir + "models/portal-large-arch.obj");
	auto mesh_portal_small = make_shared<Mesh>(source_dir + "models/portal-small-arch.obj");
	auto mesh_portal_door = make_shared<Mesh>(source_dir + "models/portal-door.obj");
	auto mesh_exit = make_shared<Mesh>(source_dir + "models/exit.obj");
	
	mesh_quad->get_submeshes().push_back(mesh_quad_sub);
	meshPool.insert(make_pair(MeshType::QUAD, mesh_quad));
	//meshPool.insert(make_pair(MeshType::HANDGUN, mesh_handgun));
	meshPool.insert(make_pair(MeshType::STAIRS, mesh_stairs));
	// Default player mesh
	meshPool.insert(make_pair(MeshType::PLAYER, mesh_player));
	meshPool.insert(make_pair(MeshType::PORTAL, mesh_portal));
	meshPool.insert(make_pair(MeshType::PORTAL_LARGE, mesh_portal_large));
	meshPool.insert(make_pair(MeshType::PORTAL_SMALL, mesh_portal_small));
	meshPool.insert(make_pair(MeshType::PORTAL_DOOR, mesh_portal_door));
	meshPool.insert(make_pair(MeshType::EXIT, mesh_exit));

	auto component_quad = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::QUAD] });
	//auto component_handgun = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::HANDGUN] });
	auto component_stairs = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::STAIRS] });
	auto component_player = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::PLAYER] });
	auto component_portal = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::PORTAL] });
	auto component_portal_large = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::PORTAL_LARGE] });
	auto component_portal_small = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::PORTAL_SMALL] });
	auto component_portal_door = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::PORTAL_DOOR] });
	auto component_exit = make_shared<GraphicsComponent>(initializer_list<shared_ptr<Mesh>>{ meshPool[MeshType::EXIT] });
	
	compPool.insert(make_pair(ComponentType::QUAD, component_quad));
	//compPool.insert(make_pair(ComponentType::HANDGUN, component_handgun));
	compPool.insert(make_pair(ComponentType::STAIRS, component_stairs));
	compPool.insert(make_pair(ComponentType::PLAYER, component_player));
	compPool.insert(make_pair(ComponentType::PORTAL, component_portal));
	compPool.insert(make_pair(ComponentType::PORTAL_LARGE, component_portal_large));
	compPool.insert(make_pair(ComponentType::PORTAL_SMALL, component_portal_small));
	compPool.insert(make_pair(ComponentType::PORTAL_DOOR, component_portal_door));
	compPool.insert(make_pair(ComponentType::EXIT, component_exit));
}

GraphicsComponentPool::~GraphicsComponentPool()	{
	// Do nothing
}
