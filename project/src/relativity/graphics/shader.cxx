#include "relativity/core/gl_error.hxx"
#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/vertex_attribute.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"

#include <gl/glew.h> 
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <spdlog/spdlog.h>

#include <memory>
#include <fstream>
#include <functional>
#include <string>

using namespace relativity::entity;
using namespace relativity::graphics;
using namespace std;

Shader::Shader(): logger(spdlog::get("shader")) {
	program = glCreateProgram();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	attributes = vector<VertexAttribute>();
	GL_CHECK;
}

Shader::~Shader() {
	glUseProgram(program);

	for (auto& shader : shaders) {
		glDeleteShader(shader.second);
		glDetachShader(program, shader.second);
	}

	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(program);
	GL_CHECK;
}

void Shader::add_attribute(const GLuint index, const GLint size, 
		 GLenum type, const GLboolean normalised, 
		const GLsizei stride, const GLuint offset) {
	glBindVertexArray(vao);
	attributes.push_back(
		VertexAttribute(index, size, type, normalised, stride, offset)
	);
	GL_CHECK;
}

void Shader::add_uniform(const string& key) {
	const auto uniform = glGetUniformLocation(program, key.c_str());
	uniforms[key] = uniform;
	GL_CHECK;
}

void Shader::add_shader(const string& key, const string& source_path,
	const GLenum type) {
	using BufferedCharIter = istreambuf_iterator<char>;
	get_logger()->info("Loading shader from {}", source_path);

	// Load the shader source from a file
	ifstream shader_stream(source_path);

	// Convert the ifstream into a string
	const string shader_contents((BufferedCharIter(shader_stream)), BufferedCharIter());
	const auto* shader_source = shader_contents.c_str();

	// Compile the shader source
	auto shader = glCreateShader(type);
	glShaderSource(shader, 1, &shader_source, nullptr);
	glCompileShader(shader);

	if (check(shader)) {
		shaders[key] = shader;
		glAttachShader(program, shader);
		GL_CHECK;
	}
}

void Shader::bind(Light& light) {
	GL_CHECK;
	bind_uniform<GLfloat>("light.ambient_intensity", light.ambient_intensity);
	bind_uniform<GLfloat>("light.falloff_radius", light.falloff_radius);
	bind_uniform<GLfloat>("light.fog_density", light.fog_density);
	bind_uniform<GLfloat>("light.specular_strength", light.specular_strength);
	bind_uniform<GLboolean>("light.blinn_phong_flag", light.using_blinn_phong);
	bind_uniform<GLboolean>("light.brdf_flag", light.using_brdf);
	bind_uniform<GLboolean>("light.fog_flag", light.using_fog);
	bind_uniform<GLboolean>("light.fog_flag", light.using_fog);
	bind_uniform<GLboolean>("light.gamma_flag", light.using_gamma);
	bind_uniform<GLboolean>("light.perspective_flag", light.using_perspective);
	bind_uniform<GLboolean>("light.shadow_flag", light.using_shadow);
	bind_uniform<GLboolean>("light.ambient_flag", light.using_ambient);
	bind_uniform<GLboolean>("light.diffuse_flag", light.using_diffuse);
	bind_uniform<GLboolean>("light.specular_flag", light.using_specular);
	bind_uniform<GLboolean>("light.falloff_flag", light.using_falloff);
	bind_uniform<GLboolean>("light.material_diffuse_flag", light.using_material_diffuse);
	bind_uniform<GLboolean>("light.material_normal_flag", light.using_material_normal);
	bind_uniform<GLboolean>("light.material_specular_flag", light.using_material_specular);
	bind_uniform<glm::vec4>("light.light_position", glm::vec4(light.get_position(), 1.0f));
	bind_uniform<glm::vec4>("light.light_colour", light.get_diffuse());
	bind_uniform<glm::vec4>("light.fog_colour", light.get_fog());
	GL_CHECK;
}

void Shader::bind(Mesh& mesh) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", mesh.shininess);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", mesh.using_diffuse_map);
	bind_uniform<GLboolean>("mesh.using_normal_map", mesh.using_normal_map);
	bind_uniform<GLboolean>("mesh.using_specular_map", mesh.using_specular_map);
	bind_uniform<glm::mat4>("model", mesh.get_model());

	if (mesh.using_diffuse_map) bind_uniform<GLint>("mesh.diffuse_map", 1);
	if (mesh.using_normal_map) 	bind_uniform<GLint>("mesh.normal_map", 2);
	if (mesh.using_specular_map) bind_uniform<GLint>("mesh.specular_map", 3);

	GL_CHECK;
}

void Shader::bind(const Mesh& mesh) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", mesh.shininess);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", mesh.using_diffuse_map);
	bind_uniform<GLboolean>("mesh.using_normal_map", mesh.using_normal_map);
	bind_uniform<GLboolean>("mesh.using_specular_map", mesh.using_specular_map);
	bind_uniform<glm::mat4>("model", mesh.get_model());

	if (mesh.using_diffuse_map) bind_uniform<GLint>("mesh.diffuse_map", 1);
	if (mesh.using_normal_map) 	bind_uniform<GLint>("mesh.normal_map", 2);
	if (mesh.using_specular_map) bind_uniform<GLint>("mesh.specular_map", 3);

	GL_CHECK;
}

void Shader::bind(const shared_ptr<Mesh>& mesh) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", mesh->shininess);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", mesh->using_diffuse_map);
	bind_uniform<GLboolean>("mesh.using_normal_map", mesh->using_normal_map);
	bind_uniform<GLboolean>("mesh.using_specular_map", mesh->using_specular_map);
	bind_uniform<glm::mat4>("model", mesh->get_model());

	if (mesh->using_diffuse_map) bind_uniform<GLint>("mesh.diffuse_map", 1);
	if (mesh->using_normal_map) bind_uniform<GLint>("mesh.normal_map", 2);
	if (mesh->using_specular_map) bind_uniform<GLint>("mesh.specular_map", 3);

	GL_CHECK;
}

void Shader::bind(Portal& portal) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", 10.f);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", false);
	bind_uniform<GLboolean>("mesh.using_normal_map", false);
	bind_uniform<GLboolean>("mesh.using_specular_map", false);
	GL_CHECK;
}

void Shader::bind(const Portal& portal) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", 10.f);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", false);
	bind_uniform<GLboolean>("mesh.using_normal_map", false);
	bind_uniform<GLboolean>("mesh.using_specular_map", false);
	GL_CHECK;
}

void Shader::bind(const shared_ptr<Portal>& portal) {
	GL_CHECK;
	bind_uniform<GLfloat>("mesh.shininess", 10.f);
	bind_uniform<GLboolean>("mesh.using_diffuse_map", false);
	bind_uniform<GLboolean>("mesh.using_normal_map", false);
	bind_uniform<GLboolean>("mesh.using_specular_map", false);
	GL_CHECK;
}

bool Shader::check(const GLuint shader) {
	// Get status
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_TRUE) {
		return true;
	}
	else {
		// Get log
		char buffer[512];
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		get_logger()->error(buffer);
		return false;
	}
	GL_CHECK;
}

void Shader::draw(const Mesh& mesh, const bool box) {
	GL_CHECK;
	if (!box) {
		mesh.bind();
	}

	glUseProgram(program);
	glBindVertexArray(vao);

	for (const auto& attribute : attributes) {
		attribute.enable();
	}

	if (box) {
		glDrawArrays(GL_LINES, 0, 24);
	}
	else {
		mesh.draw();
	}
	GL_CHECK;
}

void Shader::draw(const shared_ptr<Mesh>& mesh, const bool box) {
	GL_CHECK;
	
	if (!box) {
		mesh->bind();
	}
	else {
		mesh->bind_box();
	}

	glUseProgram(program);
	glBindVertexArray(vao);

	for (const auto& attribute : attributes) {
		attribute.enable();
	}
	
	if (box) {
		glDrawArrays(GL_LINES, 0, 24);
	}
	else {
		mesh->draw();
	}
	GL_CHECK;
}

void Shader::draw(const shared_ptr<Portal>& portal) {
	GL_CHECK;
	bind_uniform<glm::mat4>("model", portal->get_model());

	// Draw the portal's entrance
	for (const auto mesh : portal->get_mesh()->get_meshes()) {
		for (const auto submesh : mesh->get_submeshes()) {
			submesh->bind();

			glUseProgram(program);
			glBindVertexArray(vao);

			for (const auto& attribute : attributes) {
				attribute.enable();
			}

			submesh->draw();
		}
	}

	GL_CHECK;
}

void Shader::draw_batched(const GLint first, const GLsizei count, const bool box) {
	GL_CHECK;

	glUseProgram(program);
	glBindVertexArray(vao);

	for (const auto& attribute : attributes) {
		attribute.enable();
	}

	if (box) {
		glDrawArrays(GL_LINES, first, count);
	}
	else {
		glDrawArrays(GL_TRIANGLES, first, count);
	}
	GL_CHECK;
}

void Shader::link() {
	glLinkProgram(program);
	add_uniform("position_map");
	add_uniform("normal_map");
	add_uniform("albedo_map");
	add_uniform("direction_map");
	add_uniform("depth_map");
	add_uniform("camera_position");
	add_uniform("model");
	add_uniform("proj");
	add_uniform("view");
	add_uniform("light_space");
	add_uniform("depth_map");
	add_uniform("light.blinn_phong_flag");
	add_uniform("light.brdf_flag");
	add_uniform("light.gamma_flag");
	add_uniform("light.perspective_flag");
	add_uniform("light.shadow_flag");
	add_uniform("light.ambient_flag");
	add_uniform("light.diffuse_flag");
	add_uniform("light.specular_flag");
	add_uniform("light.falloff_flag");
	add_uniform("light.material_diffuse_flag");
	add_uniform("light.material_normal_flag");
	add_uniform("light.material_specular_flag");
	add_uniform("light.fog_flag");
	add_uniform("light.fog_colour");
	add_uniform("light.light_colour");
	add_uniform("light.light_direction");
	add_uniform("light.light_position");
	add_uniform("light.ambient_intensity");
	add_uniform("light.falloff_radius");
	add_uniform("light.fog_density");
	add_uniform("light.specular_strength");
	add_uniform("mesh.diffuse_map");
	add_uniform("mesh.normal_map");
	add_uniform("mesh.specular_map");
	add_uniform("mesh.shininess");
	add_uniform("mesh.using_diffuse_map");
	add_uniform("mesh.using_normal_map");
	add_uniform("mesh.using_specular_map");
	GL_CHECK;
}