#include "relativity/core/gl_error.hxx"
#include "relativity/graphics/texture.hxx"

#include <soil/SOIL.h>
#include <gl/glew.h>  
#include <spdlog/spdlog.h>
#include <memory>
#include <fstream>
#include <string>

using namespace relativity::graphics;
using namespace std;

Texture::TextureAtlas Texture::atlas;

Texture::Texture(const GLenum active_texture, const GLint format, const int texture_width, const int texture_height):
		width(texture_width), height(texture_height), slot(active_texture), buffer(0), logger(spdlog::get("texture")) {
	glGenTextures(1, &buffer);

	glActiveTexture(active_texture);
	glBindTexture(GL_TEXTURE_2D, buffer);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, nullptr);

	// Make the texture scale based on the nearest pixel
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	GL_CHECK;
}

Texture::Texture(const GLenum active_texture, const string& source_path): 
		width(0), height(0), slot(active_texture), buffer(0), logger(spdlog::get("texture")) {
	glGenTextures(1, &buffer);

	if (source_path.empty()) {
		get_logger()->warn("No source path given for texture");
	}
	else {
		get_logger()->info("Loading new texture from {}", source_path);
		load(source_path.c_str(), active_texture);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	GL_CHECK;
}

Texture::~Texture() {
	glDeleteTextures(GL_TEXTURE_2D, &buffer);
}

void Texture::load(const char* source_path, GLenum active_texture) {
	get_logger()->trace("Loading texture from {}", source_path);
	auto image = SOIL_load_image(source_path,&width, &height, 0, SOIL_LOAD_RGB);

	glActiveTexture(active_texture);
	glBindTexture(GL_TEXTURE_2D, buffer);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

	// Create a mipmap from the texture - we'll use this for scaling
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	SOIL_free_image_data(image);
}

shared_ptr<Texture> Texture::construct(const string& source_path, const GLenum active_texture) {
	if (atlas.count(string(source_path)) != 0) {
		return atlas.at(string(source_path));
	}
	else {
		atlas.insert(make_pair(
			source_path,
			make_shared<Texture>(active_texture, source_path)
		));
		return atlas.at(string(source_path));
	}
}