#include "relativity/graphics/mesh.hxx"

#include <boost/algorithm/string/find.hpp>
#include <gl/glew.h> 
#include <glm/glm.hpp>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <spdlog/spdlog.h>

#include <memory>
#include <fstream>
#include <iostream>
#include <string>

using namespace relativity::graphics;
using namespace std;

Mesh::Mesh(const vector<GLfloat> vertex_data, const int mesh_size, const vector<glm::vec3> bounding_box) :
		vertex_data(vertex_data), mesh_size(mesh_size), bounding_box(bounding_box), logger(spdlog::get("mesh")) {
	using_bounding_box = false;
	using_diffuse_map = false;
	using_normal_map = false;
	using_specular_map = false;
	is_floor = false;
	is_stair = false;
	is_wall = false;
	shininess = 1.0f;

	// Load the mesh data 
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	if (mesh_size > 0) {
		glBufferData(
			GL_ARRAY_BUFFER,
			sizeof(GLfloat) * vertex_data.size(),
			&vertex_data[0],
			GL_STATIC_DRAW
		);
	}
	else {
		get_logger()->warn("Attempting to load empty mesh");
	}

	model = glm::mat4();

	// Set aside a vertex buffer for the bounding box if it's a valid box
	if (bounding_box.size() == 8) {
		using_bounding_box = true;
		vector<GLfloat>& box = box_data;
		const auto a = bounding_box[0];
		const auto b = bounding_box[1];
		const auto c = bounding_box[2];
		const auto d = bounding_box[3];
		const auto e = bounding_box[4];
		const auto f = bounding_box[5];
		const auto g = bounding_box[6];
		const auto h = bounding_box[7];

		const auto add_point = [&box](const glm::vec3 point) {
			box.insert(end(box), { point.x, point.y, point.z });
		};

		/* Top Face */
		//  <0, 1>, <0, 3>, <2, 1>, <2, 3>
		add_point(a); add_point(b); 
		add_point(a); add_point(d);
		add_point(c); add_point(b);
		add_point(c); add_point(d);
		/* Sides */
		//  <0, 4>, <3, 7>, <1, 5>, <2, 6>
		add_point(a); add_point(e); 
		add_point(d); add_point(h); 
		add_point(b); add_point(f);
		add_point(c); add_point(g);
		/* Bottom Face */
		//  <4, 5>, <4, 7>, <6, 5>, <6, 7>
		add_point(e); add_point(f);
		add_point(e); add_point(h);
		add_point(g); add_point(f);
		add_point(g); add_point(h);

		glGenBuffers(1, &bbo);
		glBindBuffer(GL_ARRAY_BUFFER, bbo);

		glBufferData(
			GL_ARRAY_BUFFER,
			sizeof(GLfloat) * box_data.size(),
			&box_data[0],
			GL_STATIC_DRAW
		);
	}
}

Mesh::Mesh(const string& source_path): logger(spdlog::get("mesh")) {
	using_diffuse_map = false;
	using_normal_map = false;
	using_specular_map = false;
	using_bounding_box = false;
	is_floor = false;
	is_stair = false;
	is_wall = false;
	shininess = 1.0f;

	// Load the mesh data 
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	if (!source_path.empty()) {
		load(source_path);

		if (submeshes.size() > 0) {
			get_logger()->trace("Model has {} submeshes", submeshes.size());
			// Do nothing
		}
		else {
			get_logger()->warn("Model at {} is empty", source_path);
		}
	}

	model = glm::mat4();
}

Mesh::~Mesh() {
	if (vbo) {
		glDeleteBuffers(1, &vbo);
	}
}

void Mesh::draw() {
	// Draw each submesh in the mesh
	glDrawArrays(GL_TRIANGLES, 0, get_mesh_size());
}

void Mesh::draw() const {
	// Draw each submesh in the mesh
	glDrawArrays(GL_TRIANGLES, 0, get_mesh_size());
}

void Mesh::load(const string& source_path) {
	get_logger()->info("Loading mesh from {}", source_path);
	Assimp::Importer importer;
	vector<GLfloat> vert_data;
	int num_vertices = 0;

	// Create some closures for bounding box checks
	auto max = [](const GLfloat val, const GLfloat other) { return (val > other) ? val : other; };
	auto min = [](const GLfloat val, const GLfloat other) { return (val < other) ? val : other; };

	importer.ReadFile(source_path, aiProcessPreset_TargetRealtime_Quality 
	 | aiProcess_FixInfacingNormals
	 | aiProcess_OptimizeGraph
	 | 0);
	const auto scene = importer.GetScene();
	if (scene) {
		// If the scene is not empty then
		if (scene->HasMeshes()) {
			// For every mesh in the scene create a submesh
			for (unsigned int i = 0; i < scene->mNumMeshes; ++i) {

				get_logger()->info("ASSIMP: loading mesh {}/{} in file {}", 
					i, scene->mNumMeshes, source_path);

				const auto mesh = scene->mMeshes[i];
				const auto material = scene->mMaterials[mesh->mMaterialIndex];
				GLfloat max_x = 0;
				GLfloat min_x = 0;
				GLfloat max_y = 0;
				GLfloat min_y = 0;
				GLfloat max_z = 0;
				GLfloat min_z = 0;

				// For every face in the mesh
				for (unsigned int j = 0; j < mesh->mNumFaces; ++j) {
					const auto face = &mesh->mFaces[j];


					// OpenGL renders with triangle primitives, we want our faces to be triangulated
					if (face->mNumIndices != 3) {
						get_logger()->trace("ASSIMP: mesh in file {} is not triangulated", source_path);
						continue;
					}
					num_vertices += face->mNumIndices;

					// For every vertex in the face
					for (unsigned int k = 0; k < face->mNumIndices; ++k) {
						auto index = face->mIndices[k];
						auto vertex = mesh->mVertices[index];

						if (j == 0 && k == 0) {
							max_x = vertex.x;
							min_x = vertex.x;
							max_y = vertex.y;
							min_y = vertex.y;
							max_z = vertex.z;
							min_z = vertex.z;
						}
						else {
							max_x = max(max_x, vertex.x);
							min_x = min(min_x, vertex.x);
							max_y = max(max_y, vertex.y);
							min_y = min(min_y, vertex.y);
							max_z = max(max_z, vertex.z);
							min_z = min(min_z, vertex.z);
						}

						// Store the vertex positions
						vert_data.insert(end(vert_data), { vertex.x, vertex.y, vertex.z });

						get_logger()->trace("ASSIMP: adding vertex to mesh in file {}: <{}, {}, {}>",
							source_path, vertex.x, vertex.y, vertex.z);

						// Store the vertex normal if it exists
						if (mesh->mNormals) {
							auto normal = mesh->mNormals[index];

							vert_data.insert(end(vert_data), { normal.x, normal.y, normal.z });

							get_logger()->trace("ASSIMP: adding normal to mesh in file {}: <{}, {}, {}>", 
								source_path, normal.x, normal.y, normal.z);
						}
						else {
							vert_data.insert(end(vert_data), { 0, 0, 0 });

							get_logger()->trace("ASSIMP: mesh in file {} has no normals, lighting may be incorrect", 
								source_path);
						}

						// Store the vertex colours if they exist --
						// we're going to use materials instead
						aiColor3D colour(0.0f, 0.0f, 0.0f);

						if (AI_SUCCESS == material->Get(AI_MATKEY_COLOR_DIFFUSE, colour)) {
							vert_data.insert(end(vert_data), { colour.r, colour.g, colour.b });

							get_logger()->trace("ASSIMP: adding colour to mesh in file {}: <{}, {}, {}>",
								source_path, colour.r, colour.g, colour.b);
						}
						else {
							// Default colour is white
							vert_data.insert(end(vert_data), { 1, 1, 1 });
						}

						// Store the vertex texture coordinates if it exists
						if (mesh->mTextureCoords[0]) {
							auto texture = mesh->mTextureCoords[0][index];

							vert_data.insert(end(vert_data), { texture.x, 1 - texture.y });

							get_logger()->trace("ASSIMP: adding texture to mesh in file {}: <{}, {}>", 
								source_path, texture.x, 1 - texture.y);
						}
						else {
							vert_data.insert(end(vert_data), { 0, 0 });
						}

						// Store the tangent coordinates if they exist
						if (mesh->HasTangentsAndBitangents()) {
							auto tangent = mesh->mTangents[index];
							auto bitangent = mesh->mBitangents[index];

							vert_data.insert(end(vert_data), { tangent.x, tangent.y, tangent.z});
							vert_data.insert(end(vert_data), { bitangent.x, bitangent.y, bitangent.z });
						}
						else {
							vert_data.insert(end(vert_data), { 0, 0, 0 });
							vert_data.insert(end(vert_data), { 0, 0, 0 });
						}
					}
				}
				vector<glm::vec3> bounding_box = generate_bounding_box(min_x, max_x, min_y, max_y, min_z, max_z);
				auto submesh = make_shared<Mesh>(vert_data, num_vertices, bounding_box);

				aiString tex_path;

				// Check for textures 
				// Diffuse
				if (AI_SUCCESS == material->Get(
						AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), tex_path)) {
					get_logger()->trace("Found diffuse map at {}", tex_path.C_Str());
					submesh->using_diffuse_map = true;
					submesh->get_textures().push_back(
						Texture::construct(tex_path.C_Str(), GL_TEXTURE1)
					);
				}

				// Normal
				if (AI_SUCCESS == material->Get(
						AI_MATKEY_TEXTURE(aiTextureType_HEIGHT,	0), tex_path)) {
					get_logger()->trace("Found normal map at {}", tex_path.C_Str());
					submesh->using_normal_map = true;
					submesh->get_textures().push_back(
						Texture::construct(tex_path.C_Str(), GL_TEXTURE2)
					);
				}

				// Specular
				if (AI_SUCCESS == material->Get(
						AI_MATKEY_TEXTURE(aiTextureType_SPECULAR, 0), tex_path)) {
					get_logger()->trace("Found specular map at {}", tex_path.C_Str());
					submesh->using_specular_map = true;
					submesh->get_textures().push_back(
						Texture::construct(tex_path.C_Str(), GL_TEXTURE3)
					);
				}

				// Check if the mesh has its shininess set and override the default value
				if (AI_SUCCESS == material->Get(AI_MATKEY_SHININESS, submesh->shininess)) {
					get_logger()->trace("Shininess for submesh is {}", submesh->shininess);
				}
				else {
					submesh->shininess = 1.0f;
				}

				aiString name;
				if (AI_SUCCESS == material->Get(AI_MATKEY_NAME, name)) {
					string name_str = name.C_Str();
					get_logger()->trace("ASSIMP: submesh material is {}", name_str);

					if (boost::ifind_first(name_str, "floor")) {
						submesh->is_floor = true;
					}
					else if (boost::ifind_first(name_str, "stairs")) {
						submesh->is_stair = true;
					}
					else if (boost::ifind_first(name_str, "wall")) {
						submesh->is_wall = true;
					}
				}

				get_submeshes().push_back(submesh);
			}
		}
	}
	else {
		get_logger()->error("ASSIMP: no asset found at path {}", source_path);
	}
}

vector<glm::vec3> Mesh::generate_bounding_box(const GLfloat min_x, const GLfloat max_x, 
		const GLfloat min_y, const GLfloat max_y,
		const GLfloat min_z, const GLfloat max_z) {
	return vector<glm::vec3>({
		glm::vec3(min_x, max_y, min_z), glm::vec3(min_x, max_y, max_z), glm::vec3(max_x, max_y, max_z), glm::vec3(max_x, max_y, min_z),
		glm::vec3(min_x, min_y, min_z), glm::vec3(min_x, min_y, max_z), glm::vec3(max_x, min_y, max_z), glm::vec3(max_x, min_y, min_z)
	});
}