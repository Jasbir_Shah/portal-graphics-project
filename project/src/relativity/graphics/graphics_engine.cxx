#define GLM_FORCE_RADIANS

#define _COLLISION
//#define _COLOUR
#define _NORMAL
//#define _POSITION
//#define _LIGHT

#include <gl/glew.h>
#include <glm/glm.hpp>

#include "relativity/core/gl_types.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/graphics_engine.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/texture.hxx"
#include "relativity/graphics/buffer/blend_buffer.hxx"
#include "relativity/graphics/buffer/collision_buffer.hxx"
#include "relativity/graphics/buffer/forward_buffer.hxx"
#include "relativity/graphics/buffer/geometry_buffer.hxx"
#include "relativity/graphics/buffer/light_buffer.hxx"
#include "relativity/graphics/buffer/portal_buffer.hxx"
#include "relativity/graphics/buffer/screen_buffer.hxx"

using namespace relativity::core;
using namespace relativity::entity;
using namespace relativity::event;
using namespace relativity::graphics;
using namespace relativity::graphics::buffer;
using namespace std;

GraphicsEngine::GraphicsEngine(const shared_ptr<Entity> quad,
	const shared_ptr<EventQueue> event_queue,
	const GLuint window_width, const GLuint window_height,
	const GLuint shadow_width, const GLuint shadow_height,
	const std::shared_ptr<Entity> player) :
	quad(quad), event_queue(event_queue), logger(spdlog::stderr_logger_mt("graphics-engine", true)) {

	// Heads up display textures.
	auto hudTexture = Texture::construct("resources/textures/HUD/Ingame HUD.png", GL_TEXTURE0);	

	// Create buffers
	geometry_buffer = make_shared<GeometryBuffer>(
		"resources/shaders/geometry_shader.vert",
		"resources/shaders/geometry_shader.frag",
		window_width, window_height
	);

	light_buffer = make_shared<LightBuffer>(
		"resources/shaders/light_shader.vert",
		"resources/shaders/light_shader.frag",
		window_width, window_height, geometry_buffer
	);

	// Used for rendering portals in a deferred context, it has no dependencies and 
	// is only partially implemented.
	portal_buffer = make_shared<PortalBuffer>(
		"resources/shaders/texture_shader.vert", "resources/shaders/texture_shader.frag",
		"resources/shaders/simple_shader.vert", "resources/shaders/simple_shader.frag",
		window_width, window_height, 
		geometry_buffer, light_buffer, // Required but don't draw to them
		quad
	);

	// Used for rendering collision boxes, it has no dependencies (although you can adjust the 
	// shader to take advantage of the depth map - by which it requires the g-buffer).
	collision_buffer = make_shared<CollisionBuffer>(
		"resources/shaders/collision_shader.vert", "resources/shaders/collision_shader.frag",
		"resources/shaders/simple_shader.vert", "resources/shaders/simple_shader.frag",
		window_width, window_height,
		geometry_buffer->get_attachments()[DEPTH_MAP], // Optional
		quad
	);

	// Used for rendering the scene with portals in a forward context, it has no dependencies.
	forward_buffer = make_shared<ForwardBuffer>(
		"resources/shaders/forward_shader.vert", "resources/shaders/forward_shader.frag",
		"resources/shaders/simple_shader.vert", "resources/shaders/simple_shader.frag",
		window_width, window_height,
		geometry_buffer->get_attachments()[DEPTH_MAP], // Optional
		quad
	);
	
	blend_buffer = make_shared<BlendBuffer>(
		"resources/shaders/blend_shader.vert",
		"resources/shaders/blend_shader.frag",
		window_width, window_height,
		forward_buffer->get_output(),  // Required
		collision_buffer->get_output(),
		geometry_buffer->get_attachments()[DEPTH_MAP] // Optional
	);

	// Shows the g-buffer's albedo map.
	demo_colour_buffer = make_shared<BlendBuffer>(
		"resources/shaders/blend_shader.vert",
		"resources/shaders/blend_shader.frag",
		window_width, window_height,
		geometry_buffer->get_attachments()[NORMAL_MAP],  // Required
		collision_buffer->get_output(),
		geometry_buffer->get_attachments()[DEPTH_MAP] // Optional
	);
    
	// Used for combining texture buffers together with a special HUD-oriented blending function, 
	// it depends on the specified buffers.
	hud_buffer = make_shared<BlendBuffer>(
		"resources/shaders/hud_shader.vert",
		"resources/shaders/hud_shader.frag",
		window_width, window_height, 
		blend_buffer->get_output(), hudTexture->get_buffer(), // Required
		geometry_buffer->get_attachments()[DEPTH_MAP] // Optional
	);

	// Outputs a texture buffer to the screen, it depends on the specified buffer.
	screen_buffer = make_shared<ScreenBuffer>(
		"resources/shaders/texture_shader.vert",
		"resources/shaders/texture_shader.frag",
		window_width, window_height, 
		hud_buffer->get_output() // Required
	);

	// SET UP CAMERA
	const auto proj = glm::perspective(
		45.0f,														// Vertical field of view
		GLfloat(window_width) / GLfloat(window_height),				// Aspect ratio
		26.0f,														// Near plane distance (min z)
		1250.0f														// Far plane distance (max z)
	);

	camera = make_shared<entity::Camera>(proj, player);

	// SET UP LIGHTING
	// Create a point light source
	const glm::vec3 light_position(0.0f, 80.0, -100.0f);
	const glm::vec2 light_orientation(90.0f, -89.9f);
	const glm::vec4 light_diffuse(1.0f);
	light = make_shared<entity::Light>(proj, light_position, light_orientation,
		light_diffuse);

	// Set some attributes for the light source
	light->falloff_radius = 700.0f;
	light->fog_density *= 0.05f;
	light->set_fog(glm::vec4(glm::vec3(1.0f), 1.0f));

	// Make the camera and light event listeners
	event_queue->subscribe(camera);
	event_queue->subscribe(light);

	// Set a background color, typically this will be overwritten by the framebuffers
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	Framebuffer::set_max_depth(0);
}

GraphicsEngine::~GraphicsEngine() {
	// Do nothing
}

void GraphicsEngine::update(const double time_delta,
	const vector<shared_ptr<Entity>>& entities,
	const PortalMap& portals, const shared_ptr<Entity> player,
	const GLuint width, const GLuint height, const shared_ptr<Window> window) {
	static bool render_forward_buffer = true;
	static bool render_albedo_map = false;
	static bool render_normal_map = false;
	static bool render_position_map = false;
	static bool render_collision_boxes = false;
	const auto view = camera->get_view();
	const auto proj = camera->get_projection();

	// Deferred Renderer //
	// Render some portals
	//portal_buffer->bind();
	//portal_buffer->enable_mask<GL::Color>(true);
	//portal_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	//portal_buffer->clear();
	//portal_buffer->draw(width, height, entities, portals, view, proj, portal_buffer, light);
	//
	// Do not enable the following if you've enabled the above portal buffer //

	// Map important geometric attributes
	if (geometry_buffer && (render_albedo_map || render_normal_map || render_position_map)) {
		geometry_buffer->bind();
		geometry_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
		geometry_buffer->enable_test<GL::Depth>(true);
		geometry_buffer->clear();
		geometry_buffer->enable_mask<GL::Stencil>(false);
		geometry_buffer->draw(width, height, entities, portals, view, proj, geometry_buffer, light);
	}

	// Draw the lighting and colours
	//light_buffer->bind();
	//light_buffer->enable_mask<GL::Color, GL::Depth>(true);
	//light_buffer->enable_test<GL::Depth>(false);
	//light_buffer->clear<GL::Color, GL::Depth>();
	//light_buffer->enable_mask<GL::Depth>(false);
	//light_buffer->draw(width, height, { quad }, portals, view, proj, light_buffer, light);

	// Forward Renderer //
	// Draw the collision boxes
	if (collision_buffer && render_collision_boxes) {
		collision_buffer->bind();
		collision_buffer->enable_mask<GL::Color>(true);
		collision_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
		collision_buffer->clear();
		collision_buffer->draw(width, height, entities, portals, view, proj, collision_buffer, light);
	}

	// Draw the portals and lighting
	if (forward_buffer && render_forward_buffer) {
		forward_buffer->bind();
		forward_buffer->enable_mask<GL::Color>(true);
		forward_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
		forward_buffer->clear();
		forward_buffer->draw(width, height, entities, portals, view, proj, forward_buffer, light);
	}

	// Blend the forward and collision buffers
	blend_buffer->bind();
	blend_buffer->enable_mask<GL::Color>(true);
	blend_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	blend_buffer->enable_test<GL::Depth, GL::Stencil>(false);
	blend_buffer->draw(width, height, { quad }, portals, view, proj, blend_buffer, light);
	
	// Blend the HUD in as well
	hud_buffer->bind();
	hud_buffer->enable_mask<GL::Color>(true);
	hud_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	hud_buffer->enable_test<GL::Depth, GL::Stencil>(false);
	hud_buffer->enable_mask<GL::Depth, GL::Stencil>(false);
	hud_buffer->draw(width, height, { quad }, portals, view, proj, hud_buffer, light);

	// Lastly, render the complete scene as a texture onto a fullscreen quad
	screen_buffer->bind(nullptr);
	screen_buffer->enable_mask<GL::Color>(true);
	screen_buffer->enable_mask<GL::Depth, GL::Stencil>(true);
	screen_buffer->enable_test<GL::Depth, GL::Stencil>(false);
	screen_buffer->enable_mask<GL::Depth, GL::Stencil>(false);
	screen_buffer->draw(width, height, { quad }, portals, glm::mat4(), glm::mat4(), nullptr, light);

	glfwSwapBuffers(window->get());

	std::shared_ptr<Texture> hudTexture;

	// Check for events
	bool processing = true;
	while (processing) {
		processing = false;
		const auto event = poll();

		switch (event.get_type()) {
		case event::EventType::NO_EVENT: break;
		case event::EventType::INCREASE_PORTAL_DEPTH:
			Framebuffer::set_max_depth(Framebuffer::get_max_depth() + 1);
			get_logger()->info("Increased portal depth to {}", Framebuffer::get_max_depth());
			break;
		case event::EventType::DECREASE_PORTAL_DEPTH:
			Framebuffer::set_max_depth(Framebuffer::get_max_depth() - 1);
			get_logger()->info("Decreased portal depth to {}", Framebuffer::get_max_depth());
			break;
		case event::EventType::SWITCH_COLLISION:
			get_logger()->info("Toggling collision boxes.");
			render_collision_boxes = !render_collision_boxes;
			collision_buffer->bind();
			collision_buffer->clear();
			break;
		case event::EventType::SWITCH_COLOUR:
			render_forward_buffer = false;
			render_normal_map = false;
			render_position_map = false;
			render_albedo_map = true;
			blend_buffer->set_input(geometry_buffer->get_attachments()[ALBEDO_MAP]);
			get_logger()->info("Switching to G-buffer: albedo map.");
			break;
		case event::EventType::SWITCH_NORMAL:
			render_forward_buffer = false;
			render_albedo_map = false;
			render_position_map = false;
			render_normal_map = true;
			blend_buffer->set_input(geometry_buffer->get_attachments()[NORMAL_MAP]);
			get_logger()->info("Switching to G-buffer: normal map.");
			break;
		case event::EventType::SWITCH_POSITION:
			render_forward_buffer = false;
			render_albedo_map = false;
			render_normal_map = false;
			render_position_map = true;
			blend_buffer->set_input(geometry_buffer->get_attachments()[POSITION_MAP]);
			get_logger()->info("Switching to G-buffer: position map.");
			break;
		case event::EventType::SWITCH_LIGHT:
			render_albedo_map = false;
			render_normal_map = false;
			render_position_map = false;
			render_forward_buffer = true;
			blend_buffer->set_input(forward_buffer->get_output());
			get_logger()->info("Switching to forward renderer.");
			break;
		default:
			camera->notify(event);
			light->notify(event);
			processing = true;
			break;
		}
	}

	camera->update(time_delta);
	light->update(time_delta);
}