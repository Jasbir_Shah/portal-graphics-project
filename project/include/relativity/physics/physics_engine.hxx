#pragma once

#include <glm/glm.hpp>
#include <spdlog/spdlog.h>

#include <iostream>
#include <memory>

#include "relativity/event/actor.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/mesh.hxx"

namespace relativity
{
	namespace physics
	{
		class PhysicsEngine : public relativity::event::Actor
		{
		private:
			using EntityList = std::vector<std::shared_ptr<entity::Entity>>;
			using PortalMap = std::unordered_map<std::string, std::shared_ptr<entity::Portal>>;
			const std::shared_ptr<spdlog::logger> logger;
			const int MIN = 4;
			const int MAX = 2;
			const int FEET = 18;

			const float GRAVITY = 20.0f;

			glm::vec3 player_position;

			/* Getters */
			inline auto get_logger() { return logger; }
		public:
			void name() override { std::cout << std::endl << "Physics Engine"; }
			/* Constructors */
			PhysicsEngine();

			/* Destructors */
			~PhysicsEngine();

			// After adding entities, it will look like this instead
			// void update(const double time_delta, const std::vector<Entity>& entities)
			void update(
				const double time_delta,
				const EntityList& entities, const PortalMap& portals,
				const std::shared_ptr<entity::Entity>& player,
				const std::shared_ptr<entity::Entity>& stairs
			);
			void movePlayer(const double time_delta, const std::shared_ptr<entity::Entity>& player);
			bool applyGravity(const std::vector<glm::vec3>& bounding_box, const std::shared_ptr<entity::Entity>& player);

			void checkCollisions(
				const double time_delta,
				const std::shared_ptr<entity::Entity>& player,
				const std::shared_ptr<entity::Entity>& stairs
			);

			void checkPortals(
				const PortalMap& portals,
				const std::shared_ptr<entity::Entity>& player
			);

			bool check(
				const std::shared_ptr<entity::Entity>& player,
				std::vector<glm::vec3>& bounding_box,
				const double time_delta
			);

			bool intersects(
				const std::shared_ptr<entity::Entity>& player,
				const std::shared_ptr<graphics::Mesh>& mesh
			);
		};
	}
}