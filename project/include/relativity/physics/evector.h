#pragma once

namespace relativity
{
	namespace physics
	{
		class EVector
		{
			private:
				double x;
				double y;
				double z;

			public:
				EVector();
				EVector(double _x, double _y, double _z);
				~EVector();

				void setX(double _x);
				void setY(double _y);
				void setZ(double _z);

				double getX();
				double getY();
				double getZ();
		};
	}
}
