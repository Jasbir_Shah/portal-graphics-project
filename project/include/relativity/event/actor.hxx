#pragma once

#include <memory>
#include <queue>

#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"

namespace relativity { namespace event {
	/**
	 * An actor is an object that can interact with events. They utilise an 
	 * asynchronous mailbox system where received events are stored in a local queue.
	 */
	class Actor {
	private:
		std::queue<Event> events;

		inline auto& get_events() { return events; }

	protected:

	public:
		Actor() {
			// Do nothing
		}

		~Actor() {
			// Do nothing
		}

		/**
		 * This will be moved to protected later on
		 */
		auto poll() {
			if (!get_events().empty()) {
				auto event = get_events().front();
				get_events().pop();
				return event;
			}
			else {
				return Event(0, EventType::NO_EVENT);
			}
		}

		/**
		 * For debuggery only.
		 */
		virtual void name() = 0;

		virtual void update(const float time_delta) {
			// Process events
		}

		void notify(const Event event) {
			get_events().push(event);
		}
	};
} }
