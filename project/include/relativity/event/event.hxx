#pragma once

#include "relativity/event/actor.hxx"
#include "relativity/event/event_type.hxx"

namespace relativity { namespace event {
	// NOTE: we actually haven't used the context anywhere so we just fill it with dummy values.
	/**
	 * An event is a reified action. It contains a context (either its caller or
	 * destination) and a type that represents its action.
	 */
	class Event {
	private:
		const int context;
		const EventType type;

	public:
		Event(const int context, const EventType type = EventType::NO_EVENT): 
			context(context), type(type) {
			// Do nothing 
		}

		~Event() {
			// Do nothing 
		}

		inline auto get_context() const { return context; }
		inline auto get_type() const { return type; }
	};
} }
