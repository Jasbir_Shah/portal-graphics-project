#pragma once

#include <memory>
#include <list>

#include "relativity/event/actor.hxx"
#include "relativity/event/event.hxx"

namespace relativity { namespace event {
	/**
	 * An event queue acts a switch for events that pass through it. Actors can subscribe to the 
	 * event queue so that they get notified by any events that pass through it.
	 */
	class EventQueue {
	private:
		std::list<std::shared_ptr<Actor>> subscribers;

		inline auto& get_subscribers() { return subscribers; }

	public:
		EventQueue() {
			// Do nothing 
		}

		~EventQueue() {
			// Do nothing 
		}

		void notify(const Event event) {
			if (event.get_type() != EventType::NO_EVENT) {
				for (const auto& subscriber : get_subscribers()) {
					subscriber->notify(event);
				}
			}
		}

		void subscribe(const std::shared_ptr<Actor> subscriber) {
			if (subscriber) {
				get_subscribers().push_back(subscriber);
			}
		}

		void unsubscribe(const std::shared_ptr<Actor> subscriber) {
			if (subscriber) {
				get_subscribers().remove(subscriber);
			}
		}
	};
} }
