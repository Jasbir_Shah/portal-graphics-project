#pragma once

#include <glfw/glfw3.h>  
#include <spdlog/spdlog.h>
#include <string>

namespace relativity { namespace core {
	/**
	 * A wrapper around a GLFW window instance. The error callback is shared between all wrappers, but other callbacks are redirected to the calling wrapper instance.
	 * This wrapper only redirects the callbacks, it doesn't define how they're handled, instead a subclass must handle the callback definitions. 
	 */
	class WindowWrapper {
	private:
		/**
		 * A logger for GLFW errors, shared by all WindowWrappers.
 		 */
		static std::shared_ptr<spdlog::logger> LOGGER;
		GLFWwindow* window;

		/**
		 * The callback for GLFW errors, shared by all WindowWrappers.
		 */
		static void s_error_callback(int error, const char* description);

		/**
		 * The redirecting callback for GLFW key callbacks.
		 */
		static void s_key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

		/**
		 * The redirecting callback for GLFW scrolling callbacks.
		 */
		static void s_scroll_callback(GLFWwindow* window, double x, double y);

		/**
		 * The instance callback for GLFW key callbacks.
		 */
		virtual void key_callback(const int key, const int scancode, const int action, const int mods) = 0;
		
	    /**
		 * The instance callback for GLFW key callbacks.
		 */
		virtual void scroll_callback(const double x, const double y) = 0;

	public:
		/**
		* Constructs a WindowWrapper with a GLFWwindow instance that has the specified width, height and title.
		*
		* @param width   The width of the window.
		* @param height  The height of the window.
		* @param title   The title of the window.
		*/
		WindowWrapper(const int width, const int height, const std::string& title);


		/**
		* Destroys a WindowWrapper and deallocates its GLFWwindow instance.
		*/
		~WindowWrapper();

		/**
		 * Exposes the raw GLFWwindow pointer.
		 *
		 * @return  The pointer to the GLFWwindow instance.
		 */
		inline const auto get() { return window; }

		/**
		* Exposes the raw GLFWwindow pointer.
		*
		* @return  The pointer to the GLFWwindow instance.
		*/
		inline const auto get() const { return window; }
	};
} }
