#pragma once

#ifndef GL_CHECK
#define GL_CHECK relativity::core::gl_error_check(__FILE__, __LINE__)
#define GL_CHECK_SUPPRESS relativity::core::gl_error_check(__FILE__, __LINE__, true)
#endif

#include <gl/glew.h>
#include <spdlog/spdlog.h>
#include <string>

namespace relativity {
	namespace core {
		/**
		 * Checks if any errors have been flagged by OpenGL. If there are any errors they get logged
		 * to the console with their line number and error description.
		 */
		static void gl_error_check(const std::string& file, const int line, bool suppress = false) {
		#ifdef _DEBUG // Only check for errors in debug builds
		const auto error = glGetError();

		if (suppress) return;

		switch (error) {
		case GL_NO_ERROR: break;
		case GL_INVALID_ENUM: spdlog::get("gl")->error("Error in file {}, at line {}: Invalid Enum", file, line); break;
		case GL_INVALID_VALUE: spdlog::get("gl")->error("Error in file {}, at line {}: Invalid Value", file, line); break;
		case GL_INVALID_OPERATION: spdlog::get("gl")->error("Error in file {}, at line {}: Invalid Operation", file, line); break;
		case GL_STACK_OVERFLOW: spdlog::get("gl")->error("Error in file {}, at line {}: Stack Overflow", file, line); break;
		case GL_STACK_UNDERFLOW: spdlog::get("gl")->error("Error in file {}, at line {}: Stack Underflow", file, line); break;
		case GL_OUT_OF_MEMORY: spdlog::get("gl")->error("Error in file {}, at line {}: Out of Memory", file, line); break;
		case GL_TABLE_TOO_LARGE: spdlog::get("gl")->error("Error in file {}, at line {}: Table too Large", file, line); break;
		default: spdlog::get("gl")->error("Error in file {}, at line {}: Unknown", file, line); break;
		}
		#endif // NDEBUG

	}
} };