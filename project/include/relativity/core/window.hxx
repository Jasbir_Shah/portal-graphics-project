#pragma once

#include <glfw/glfw3.h>  
#include <spdlog/spdlog.h>
#include <iostream>
#include <string>

#include "relativity/core/window_wrapper.hxx"
#include "relativity/event/actor.hxx"

namespace relativity { namespace core {
	/**
	 * A WindowWrapper with event handling. Events that are routed through the Window can trigger
	 * new events which the Window stores for future access.
	 */
	class Window : public relativity::core::WindowWrapper, public relativity::event::Actor {
	private:
		const std::shared_ptr<spdlog::logger> logger;

		void key_callback(const int key, const int scancode, const int action, const int mods) override;
		void scroll_callback(const double x, const double y) override;

		inline auto get_logger() { return logger; }

	public:
		void name() override { std::cout << std::endl << "Window";  }

		Window(const int width, const int height, const std::string& title);
		~Window();
	};
} }
