#pragma once

#include <gl/glew.h>  
#include <glfw/glfw3.h>  
#include <spdlog/spdlog.h>
#include <soil/SOIL.h>

#include <chrono>
#include <memory>
#include <string>
#include <unordered_map>

#include "relativity/config/default_settings.hxx"
#include "relativity/config/settings.hxx"
#include "relativity/core/window.hxx"
#include "relativity/event/event_queue.hxx"
#include "relativity/graphics/graphicsComponentPool.h"
#include "relativity/graphics/graphics_engine.hxx"
#include "relativity/physics/physics_engine.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/portal.hxx"
#include "relativityUtils/createPortalsFromFile.hxx"

namespace relativity { namespace core {
	/**
	 * An Engine initialises a GLFW and OpenGL context. It loads in a scene, sets the player and passes 
	 * them on to its child engines for processing.
	 */
	class Engine {
	private:
		using PortalMap = std::unordered_map<std::string, std::shared_ptr<entity::Portal>>;

		const std::shared_ptr<config::Settings> settings;
		const std::shared_ptr<spdlog::logger> logger;
		const std::chrono::high_resolution_clock::time_point start_time;
		bool started;
		std::shared_ptr<graphics::GraphicsComponentPool> graphics_pool;
		std::shared_ptr<event::EventQueue> event_queue;
		std::shared_ptr<graphics::GraphicsEngine> graphics_engine;
		std::shared_ptr<physics::PhysicsEngine> physics_engine;
		std::shared_ptr<core::Window> window;
		std::vector<std::shared_ptr<entity::Entity>> entities;
		PortalMap portals;
		std::shared_ptr<entity::Entity> player;
		std::shared_ptr<entity::Entity> stairs;

		/** Initialised GLFW, returns false if it fails. */
		bool initialise_glfw();
		/** Initialised OpenGL, returns false if it fails. */
		bool initialise_open_gl();
		
		/* Getters */
		inline auto get_logger() { return logger;  }
		inline auto get_start_time() { return start_time; }
		inline auto& get_graphics() { return graphics_engine; }
		inline auto& get_physics() { return physics_engine; }
		inline auto& get_event_queue() { return event_queue; }

		/* Setters */
		inline void set_started(bool started) { this->started = started; }
	public:
		/* Constructors */
		Engine(const std::string& source_dir, 
			const std::shared_ptr<config::Settings> settings =
				std::make_shared<config::DefaultSettings>(720, 1280, 4, 3));

		/* Destructors */
		~Engine();
		
		/**
		 * Updates the graphics and physics engines based on the time that's elapsed since the previous
		 * update.
		 */
		void update(const double time_delta);
		void process_events();
		void run();

		/* Getters */
		inline auto has_started() { return started; }
		inline const auto get_window() { return window; }
	};
} }