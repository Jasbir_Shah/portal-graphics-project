#pragma once

#include <glfw/glfw3.h>  
#include <spdlog/spdlog.h>
#include <iostream>
#include <string>

#include "relativity/core/window_wrapper.hxx"
#include "relativity/event/actor.hxx"

namespace relativity { namespace core {
	/**
	 * A trait-type class for GL buffer types. These are used for templating.
	 */
	class GL final {
	private:
		GL() {};
	public:
		class Color final { private: Color() {} };
		class Depth final { private: Depth() {} };
		class Stencil final { private: Stencil() {} };
	};
} };
