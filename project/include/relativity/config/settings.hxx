#pragma once

namespace relativity { namespace config {
	/**
	 * A Settings object encapsulates configuration info. This class cannot be
	 * instantiated, you must subclass your own implementation to use it.
	 * <p>
	 * An example use case would be to create an AutomaticSettings object that
	 * retrieves its settings by checking a user's available drivers and hardware.
	 */
	class Settings {
	public:
		/**
		 * Returns the Open GL major version.
		 *
		 * @return  The OpenGL major version.
		 */
		virtual int get_open_gl_major() = 0;

		/**
		 * Returns the Open GL minor version.
		 *
		 * @return  The OpenGL minor version.
		 */
		virtual int get_open_gl_minor() = 0;

		/**
		 * Returns the height of the primary window in pixels.
		 *
		 * @return  The window height in pixels.
		 */
		virtual int get_window_height() = 0;

		/**
		 * Returns the width of the primary window in pixels.
		 *
		 * @return  The window width in pixels.
		 */
		virtual int get_window_width() = 0;
	protected:
		Settings() { }
	};
} }