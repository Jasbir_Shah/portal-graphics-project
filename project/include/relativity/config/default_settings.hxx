#pragma once

#include "relativity/config/settings.hxx"

namespace relativity { namespace config {
	class DefaultSettings : public relativity::config::Settings {
	private:
		const int gl_major;
		const int gl_minor;
		const int window_height;
		const int window_width;
	public:
		DefaultSettings(const int height, const int width, const int major, const int minor) :
			window_height(height), window_width(width), gl_major(major), gl_minor(minor) { }

		inline int get_open_gl_major() override { return gl_major; }
		inline int get_open_gl_minor() override { return gl_minor; }
		inline int get_window_height() override { return window_height; }
		inline int get_window_width() override { return window_width; }
	};
} }