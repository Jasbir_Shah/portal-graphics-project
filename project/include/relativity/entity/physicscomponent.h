#pragma once

#include <vector>
#include <numeric>

namespace relativity
{
	namespace entity
	{
		class PhysicsComponent
		{
			private:
				const size_t SIZEOFVECTOR = 3;
				const size_t X = 0;
				const size_t Y = 1;
				const size_t Z = 2;

				std::vector<double> location;
				std::vector<double> velocity;
				std::vector<double> acceleration;

			public:
				PhysicsComponent();
				~PhysicsComponent();

				void setLocation(std::vector<double> _location);					//teleporting
				void setLocation(double _x, double _y, double _z);							//teleporting
				void setVelocity(std::vector<double> _velocity);					//moving
				void setVelocity(double _x, double _y, double _z);							//moving
				void setAcceleration(std::vector<double> _acceleration);			//unsure yet - may not be required
				void setAcceleration(double _x, double _y, double _z);						//unsure yet - may not be required
				void move();
		};
	}
}
