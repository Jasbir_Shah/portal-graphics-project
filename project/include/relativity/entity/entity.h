#pragma once

#include "relativity/entity/physicscomponent.h"
#include "relativity/entity/graphicscomponent.h"


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>
#include <iostream>
#include <string>

namespace relativity
{
	namespace entity
	{
		class Entity : public relativity::event::Actor
		{
			private:
				std::shared_ptr<PhysicsComponent> physics_component;
				std::shared_ptr<GraphicsComponent> graphics_component;

				const float GRAVITY = 20;
				const float MOVEMENT_SPEED = 20;
				const float ROTATION_SPEED = 0.5;
					
				glm::vec3 movement = glm::vec3(
					-1.0f,
					0.0f,
					0.0f);
				
				float yaw;
				float pitch;
				float roll;
				float old_yaw;
				float old_pitch;
				float old_roll;

				glm::vec3 gravity;
				glm::vec3 position;
				glm::vec3 world_position;
				glm::vec3 old_world_position;
				glm::vec3 orientation;
				glm::vec3 target;
				glm::vec3 old_position;
				glm::vec3 up;
				glm::mat4 old_model;

				bool moving_forward;
				bool moving_backward;
				bool rotate_left;
				bool rotate_right;
				bool changed;

			public:

				Entity(const std::shared_ptr<GraphicsComponent> graphics_component = nullptr,
					const std::shared_ptr<PhysicsComponent> physics_component = nullptr);
				~Entity();

				void set_gravity(glm::vec3 _gravity);
				void set_yaw(float _yaw);
				void set_pitch(float _pitch);
				void set_roll(float _roll);
				void set_old_yaw(float _old_yaw);
				void set_old_pitch(float _old_pitch);
				void set_old_roll(float _old_roll);
				void set_position(glm::vec3 _position);
				void set_world_position(glm::vec3 _world_position);
				void set_orientation(glm::vec3 _orientation);
				void set_target(glm::vec3 _target);
				void set_old_position(glm::vec3 _old_position);
				void set_old_model(glm::mat4 _old_model);
				void set_up(glm::vec3 _up);
				void set_movement(glm::vec3 _movement);


				std::shared_ptr<PhysicsComponent> get_physics_component();
				std::shared_ptr<GraphicsComponent> get_graphics_component();
				glm::vec3 get_gravity();
				float get_yaw();
				float get_pitch();
				float get_roll();
				float get_old_yaw();
				float get_old_pitch();
				float get_old_roll();
				glm::vec3 get_position();
				glm::vec3 get_world_position();
				glm::vec3 get_orientation();
				glm::vec3 get_target();
				glm::vec3 get_old_position();
				glm::mat4 get_old_model();
				glm::vec3 get_up();
				glm::vec3 get_movement();

				void name();
				void stop();
				void update(const double time_delta);
				void processEvents();
		};
	}
}