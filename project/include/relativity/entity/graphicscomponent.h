#pragma once

#include "relativity/event/actor.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/texture.hxx"

#include <initializer_list>
#include <memory>

namespace relativity {	namespace entity {
	class GraphicsComponent {
	private:
		std::vector<std::shared_ptr<graphics::Mesh>> meshes;

	public:
		GraphicsComponent();
		GraphicsComponent(std::initializer_list<std::shared_ptr<graphics::Mesh>> meshes);

		~GraphicsComponent();

		/* Getters */
		auto& get_meshes() const { return meshes; }
		auto& get_meshes() { return meshes; }

		/* Setters */
		void set_meshes(std::vector<std::shared_ptr<graphics::Mesh>> meshes) { this->meshes = meshes; }
		/* Copy Constructors */
		GraphicsComponent(const GraphicsComponent& component) {
			this->meshes = component.meshes;
		}

		GraphicsComponent(const std::shared_ptr<GraphicsComponent> component) {
			this->meshes = component->meshes;
		}

		GraphicsComponent& operator=(const GraphicsComponent& component) {
			this->meshes = component.meshes;
			return *this;
		}
	};
} };