#pragma once

#include "relativity/event/actor.hxx"
#include "relativity/event/event_type.hxx"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <spdlog/spdlog.h>

namespace relativity { namespace entity {
	/**
	 * A Light is a reorientable and translatable point light source. While a light can be 
	 * reoriented it will still emit light in all surrounding directions and hence its orientation
	 * has no bearing on the actual scene.
	 */
	class Light: public relativity::event::Actor {
	private:
		// Orthonormal vectors
		static const glm::vec3 roll;
		static const glm::vec3 yaw;
		static const glm::vec3 pitch;
		
		const std::shared_ptr<spdlog::logger> logger;

		// Coordinates
		glm::mat4 light_space;
		glm::mat4 projection;
		glm::mat4 view;
		/** Light's non-homogenous Cartesian co-ordinates */
		glm::vec3 position;
		/** Light's non-homogenous front directional vector */
		glm::vec3 target;

		// Rotational movement
		float yaw_angle;
		float pitch_angle;
		float rotation_speed;

		// Colours
		glm::vec4 diffuse;
		glm::vec4 specular;
		glm::vec4 fog;
		
		inline void process_events();

	public:
		// Flags
		bool using_blinn_phong;
		bool using_brdf;
		bool using_fog;
		bool using_gamma;
		bool using_perspective;
		bool using_shadow;
		bool using_ambient;
		bool using_diffuse;
		bool using_specular;
		bool using_falloff;
		bool using_material_diffuse;
		bool using_material_normal;
		bool using_material_specular;
		bool is_rotating;

		// Parameters
		float ambient_intensity;
		float falloff_radius;
		float fog_density;
		float specular_strength;

		/* Getters */
		// Coordinates
		inline auto& get_light_space() { return light_space; }
		inline auto& get_projection() { return projection; }
		inline auto& get_view() { return view; }
		inline auto& get_position() { return position; }
		inline auto& get_target() { return target; }
		inline auto& get_diffuse() { return diffuse; }
		inline auto& get_fog() { return fog; }

		// Rotational movement
		inline auto get_yaw() const { return yaw_angle; }
		inline auto get_yaw() { return yaw_angle; }
		inline auto get_pitch() const { return pitch_angle; }
		inline auto get_pitch() { return pitch_angle; }
		inline auto get_rotation_speed() const { return rotation_speed; }
		inline auto get_rotation_speed() { return rotation_speed; }
		inline auto get_logger() const { return logger; }
		inline auto get_logger() { return logger;  }

		/* Setters */
		inline void set_projection(const glm::mat4 projection) {
			this->projection = projection;
			this->projection = glm::perspective(45.0f, 1280.0f / 720.0f, 1.0f, 1000.0f);
			light_space = get_projection() * get_view();
		}

		inline void set_view(const glm::mat4 view) {
			this->view = view;
			light_space = get_projection() * get_view();
		}

		inline void set_position(const glm::vec3 position) {
			this->position = position;
			set_view(glm::lookAt(position, position + target, pitch));
		}

		inline void set_target(const glm::vec3 target) { this->target = target; }

		/**
		* Sets this light's yaw orientation to the specified degree.
		*
		* @param angle  The new orientation angle in degrees.
		*/
		void set_yaw(const float angle) {
			yaw_angle = angle;
		}

		/**
		* Sets this light's pitch orientation to the specified degree.
		* <p>
		* The angle must be within [-89.9, 89.9] degrees to prevent the light from flipping.
		*
		* @param angle  The new orientation angle in degrees.
		*/
		void set_pitch(const float angle) {
			if (angle > 89.9f) pitch_angle = 89.9f;
			else if (angle < -89.9f) pitch_angle = -89.9f;
			else {
				pitch_angle = angle;
			}
		}
		
		inline void set_diffuse(const glm::vec4 diffuse) { this->diffuse = diffuse; }
		inline void set_specular(const glm::vec4 specular) { this->specular = specular; }
		inline void set_fog(const glm::vec4 fog) { this->fog = fog; }
		inline void set_rotation_speed(const float speed) { rotation_speed = speed; }

		inline void name() override { std::cout << std::endl << "Light"; }

		void update(const float time_delta) override;

		/**
		 * Constructs a point light source with the specified position, orientation, diffuse colour, ambient strength and
		 * fog colour.
		 *
		 * @param projection   The transformation matrix for producing the light's
		 *					   projection.
		 * @param position     A vector containing the x, y, z, Cartesian coordinates
		 *					   of the light.
		 * @param orientation  A vector containing the yaw and pitch orientation of
		 *					   the light.
		 * @param diffuse      The light's diffuse colour.
		 * @param ambient      The ambient lighting strength.
		 * @param fog          The fog colour.
		 */
		Light(const glm::mat4 projection,
			const glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
			const glm::vec2 orientation = glm::vec2(65.0f, 0.0f),
			const glm::vec4 diffuse = glm::vec4(1.0f),
			const float ambient = 0.1f, const glm::vec4 fog = glm::vec4(1.0f)):
				projection(projection), position(position), 
				ambient_intensity(ambient), diffuse(diffuse), fog(fog), 
				logger(spdlog::stderr_logger_mt("light", true)) {
			yaw_angle = orientation.x;
			pitch_angle = orientation.y;
			rotation_speed = 10.0f;
			is_rotating = false;

			// Flags
			using_blinn_phong = true;
			using_brdf = false;
			using_fog = true;
			using_gamma = true;
			using_perspective = true;
			using_shadow = false;
			using_ambient = true;
			using_diffuse = true;
			using_specular = true;
			using_falloff = true;
			using_material_diffuse = true;
			using_material_normal = true;
			using_material_specular = true;
			
			// Parameters
			falloff_radius = 100;
			fog_density = 0.008f;
			specular_strength = 1.0f;

			auto yaw_radians = glm::radians(yaw_angle);
			auto pitch_radians = glm::radians(pitch_angle);

			// Calculate our light's direction vector
			target = glm::normalize(glm::vec3(
				cos(yaw_radians) * cos(pitch_radians),
				sin(pitch_radians),
				sin(yaw_radians) * cos(pitch_radians)
			));

			view = glm::lookAt(position, position + target, pitch);
			light_space = projection * view;
		}

		~Light() {
			// Do nothing 
		}
	};
} }
