#pragma once

#include "relativity/entity/camera.hxx"
#include "relativity/entity/graphicscomponent.h"
#include "relativity/graphics/mesh.hxx"
#include "relativity/event/actor.hxx"
#include "relativity/event/event_type.hxx"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iostream>

namespace relativity { namespace entity {
	/**
	 * A Portal is a reorientable and translatable localised biview frustum.
	 */
	class Portal : public relativity::event::Actor {
	private:
		glm::mat4 model;
		glm::vec3 orientation;
		glm::vec3 position;
		glm::vec3 gravity;
		std::string exit;
		std::shared_ptr<GraphicsComponent> mesh;

		inline void process_events();
	
	public:
		/* Getters */
		inline auto& get_exit() { return exit; }
		inline auto& get_exit() const { return exit; }
		inline auto& get_mesh() { return mesh; }
		inline auto& get_mesh() const { return mesh; }
		inline auto& get_model() { return model; }
		inline auto& get_model() const { return model; }
		inline auto& get_orientation() { return orientation; }
		inline auto& get_orientation() const { return orientation; }
		inline auto& get_position() { return position; }
		inline auto& get_position() const { return position; }
		inline auto& get_gravity() { return gravity; }
		inline auto& get_gravity() const { return gravity; }

		/**
		 * Returns this portal's orientation as a quaternion.
		 */
		inline auto& get_quaternion() { 
			return glm::toQuat(
				glm::orientate3(glm::vec3(
					glm::radians(get_orientation().x),
					glm::radians(get_orientation().y),
					glm::radians(get_orientation().z)
				))
			); 
		}

		/**
		 * Returns this portal's orientation as a quaternion.
		 */
		inline auto& get_quaternion() const {
			return glm::toQuat(
				glm::orientate3(glm::vec3(
					glm::radians(get_orientation().x),
					glm::radians(get_orientation().y),
					glm::radians(get_orientation().z)
				))
			);
		}

		/* Setters */
		inline void set_exit(const std::string key) { exit = key; }

		inline void set_mesh(std::shared_ptr<entity::GraphicsComponent> component) { 
			this->mesh = component; 
		}

		/** 
		 * You need to update the model matrix after this, but we never use this so I can't be bothered.
		 */
		inline void set_position(const glm::vec3 position) { this->position = position; }

		/**
		* You need to update the model matrix after this, but we never use this so I can't be bothered.
		*/
		inline void set_orientation(const glm::vec3 orientation) { this->orientation = orientation; }

		inline void name() override { std::cout << std::endl << "Portal";  }

		/**
		 * Updates this Portal according to the specified time delta.
		 *
		 * @param time_delta  The amount of time to update for.
		 */
		void update(const float time_delta) override;

		/**
		 * Constructs a Portal with the specified position, orientation, exit and mesh.
		 *
		 * @param position      The Cartesian coordinates representing the portal's entrance location.
		 * @param orientation   The Euler angles for determining the portal's mesh orientation.
		 * @param exit			A key to the portal's corresponding exit portal.
		 * @param mesh			The mesh to display the portal as.
		 */
		Portal(
			const glm::vec3 position, const glm::vec3 orientation, const glm::vec3 gravity,
			const std::string exit, const std::shared_ptr<GraphicsComponent> mesh
		);

		~Portal() {
			// Do nothing 
		}

		void draw();
		void draw() const;

		/**
		 * Return an altered projection matrix to account for clipping at the portal's surface.
		 */
		glm::mat4 project(const glm::mat4& view, const glm::mat4& proj);
	};
} }
