#pragma once

#include "relativity/event/actor.hxx"
#include "relativity/event/event_type.hxx"
#include "relativity/entity/entity.h"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

namespace relativity { namespace entity {
	/**
	 * A Camera is a reorientable and translatable view frustum.
	 */
	class Camera : public relativity::event::Actor {
	private:

		glm::vec3 up;
		glm::mat4 projection;
		glm::mat4 view;
		glm::vec3 position;
		glm::vec3 target;
		glm::vec3 distance;

		std::shared_ptr<entity::Entity> player;

		const float ROTATION_CHANGE = 2;
		const float ZOOM_CHANGE = 5;

		float set_distance;
		float yaw_rotation;
		float pitch_rotation;

		inline void process_events();
	
	public:
		// Spatial movement
		bool moving_up;
		bool moving_down;
		bool moving_forward;
		bool moving_forward_once;
		bool moving_backward;
		bool moving_backward_once;
		bool moving_left;
		bool moving_right;

		// Rotational movement
		bool rotating_up;
		bool rotating_down;
		bool rotating_left;
		bool rotating_right;

		/* Getters */
		// Rotational movement
		inline auto& get_yaw() const { return yaw_rotation; }
		inline auto& get_yaw() { return yaw_rotation; }
		inline auto& get_pitch() const { return pitch_rotation; }
		inline auto& get_pitch() { return pitch_rotation; }

		// Coordinates
		inline auto& get_projection() { return projection; }
		inline auto& get_view() { return view; }
		inline auto& get_position() { return position; }
		inline auto& get_target() { return target; }
		inline void set_view(const glm::mat4 view) { this->view = view; }
		inline void set_position(const glm::vec3 position) { this->position = position;}
		inline void set_target(const glm::vec3 target) { this->target = target; }
		inline void name() override { std::cout << std::endl << "Camera";  }

		/**
		 * Updates this camera according to the specified time delta.
		 *
		 * @param time_delta  The amount of time to update for.
		 */
		void update(const float time_delta) override;
		void calculate_position();

		/**
		 * Constructs a Camera with the specified projection matrix,
		 * Cartesian position, and Euler orientation. 
		 *
		 * @param projection   The transformation matrix for producing the camera's
		 *					   projection.
		 * @param position     A vector containing the x, y, z, Cartesian coordinates
		 *					   of the camera.
		 * @param orientation  A vector containing the yaw and pitch orientation of
		 *					   the camera.
		 */
		Camera(const glm::mat4 projection,
			   const glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), 
			   const glm::vec2 orientation = glm::vec2(65.0f, 0.0f)): 
				projection(projection), position(position) {
			// Spatial movement
			moving_up = false;
			moving_down = false;
			moving_forward = false;
			moving_forward_once = false;
			moving_backward = false;
			moving_backward_once = false;
			moving_left = false;
			moving_right = false;
			// Rotational movement
			yaw_rotation= orientation.x;
			pitch_rotation = orientation.y;
			rotating_up = false;
			rotating_down = false;
			rotating_left = false;
			rotating_right = false;

			auto yaw_radians = glm::radians(yaw_rotation);
			auto pitch_radians = glm::radians(pitch_rotation);

			target = glm::normalize(glm::vec3(
				cos(yaw_radians) * cos(pitch_radians),
				sin(pitch_radians),
				sin(yaw_radians) * cos(pitch_radians)
			));

			up = glm::vec3(0, 1, 0);

			view = glm::lookAt(position, position + target, up);
		}

		/**
		 * Constructs a third-person camera attached to an entity.
		 */
		Camera(const glm::mat4 projection, const std::shared_ptr<entity::Entity> _player) : projection(projection)
		{
			player = _player;
			position = player->get_world_position();

			moving_forward_once = false;
			moving_backward_once = false;
			rotating_up = false;
			rotating_down = false;
			rotating_left = false;
			rotating_right = false;

			set_distance = 30;
			yaw_rotation = 0;
			pitch_rotation = 0;

			const auto beginning_orientation = glm::normalize(glm::vec3(
				0, 
				1, 
				1
			));

			distance = glm::vec3(
				-30,
				10,
				0
			);

			up = player->get_up();

			view = glm::lookAt(position + distance, player->get_world_position(), up);
		}

		~Camera() {
			// Do nothing 
		}
	};
} }
