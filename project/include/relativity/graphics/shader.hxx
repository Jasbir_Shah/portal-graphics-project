#pragma once

#include <gl/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <spdlog/spdlog.h>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "relativity/entity/camera.hxx"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/vertex_attribute.hxx"

namespace relativity {	namespace graphics {
	/**
	 * A Shader is a wrapper around an OpenGL shader. It provides automatic
	 * attribute, uniform and shader processing management to alleviate the need for
	 * manual intervention.
	 * <p>
	 * Although a Shader object abstracts out a lot of the underlying details some compromises have to be made.
	 * The Shader must be manually compiled after adding shader sources, and this has to be done before adding 
	 * any uniforms.
	 * <p>
	 * Uniforms can only be accessed by string keys, this means that uniform access still isn't type-safe.
	 * In this case, this is desirable as it allows a generic form of uniform binding across shaders.
	 */
	class Shader {
	private:
		const std::shared_ptr<spdlog::logger> logger;
		std::unordered_map<std::string, GLint> uniforms;
		std::unordered_map<std::string, GLuint> shaders;
		std::vector<VertexAttribute> attributes;
		GLuint program;
		GLuint vao;

		/* Getters */
		inline auto get_logger() { return logger; }

	public:
		/**
		 * Constructs an empty and uncompiled Shader.
		 */
		Shader();
		~Shader();

		/* Getters */
		/**
		 * Returns the uniform location with the specified key. If no such uniform
		 * exists then -1 is returned and OpenGL will safely ignore the call.
		 *
		 * @param key  The key of the uniform location to return
		 * @return     The location of the uniform with the specified key
		 */
		inline GLint get_uniform(const std::string& key) {
			return uniforms[key];
		}

		/* Setters */
		/**
		 * Add an attribute with the specified index, size, type, stride and offset,
		 * to the shader. Additionally the values can be normalised.
		 * <p>  
		 * Currently this assumes you are always adding floating point values.
		 *
		 * @param index		  The attribute index.
		 * @param size		  The number of values in the attribute.
		 * @param type		  The value type of the attribute.	
		 * @param normalised  True if the attribute values should be normalised.
		 * @param stride	  The stride of the attribute values.
		 * @param offset	  The offset of the attribute.
		 */
		void add_attribute(const GLuint index, const GLint size, const GLenum type,
			const GLboolean normalised, const GLsizei stride, const GLuint offset);

		void add_uniform(const std::string& key);

		/**
		 * Adds the shader source at the specified path to the Shader as the 
		 * specified shader type, using the given key to store it.
		 *
		 * @param key		   The key of the shader source to add.
		 * @param source_path  The path to the shader source file.
		 * @param type		   The type to compile the shader as.
		 */
		void add_shader(const std::string& key,	const std::string& source_path, 
			const GLenum type);

		/**
		 * Removes the specified uniform from this Shader.
		 *
		 * @param key  The key of the uniform to remove.
		 */
		inline void remove_uniform(const std::string& key) { uniforms.erase(key); }

		/**
		 * Removes the specified shader source from this Shader. The Shader must be relinked to complete the removal.
		 *
		 * @param key  The key of the shader source to remove.
		 */
		inline void remove_shader(const std::string& key) { shaders.erase(key); }

		/**
		 * Makes this Shader the active OpenGL shader program. This method must be called before
		 * adding uniforms as well as before updating uniforms.
		 */
		inline void bind() { glUseProgram(program); }

		/**
		 * Updates this Shader's uniforms according to the specified Light.
		 *
		 * @param light  The Light to retrieve values from.
		 */
		void bind(entity::Light& light);

		/**
		 * Updates this Shader's uniforms according to the specified Mesh.
		 *
		 * @param mesh  The Mesh to retrieve values from.
		 */
		void bind(Mesh& mesh);

		/**
		* Updates this Shader's uniforms according to the specified Mesh.
		*
		* @param mesh  The Mesh to retrieve values from.
		*/
		void bind(const Mesh& mesh);

		/**
		* Updates this Shader's uniforms according to the specified Mesh.
		*
		* @param mesh  The Mesh to retrieve values from.
		*/
		void bind(const std::shared_ptr<Mesh>& mesh);

		/**
		* Updates this Shader's uniforms according to the specified Portal.
		*
		* @param portal  The Portal to retrieve values from.
		*/
		void bind(entity::Portal& portal);

		/**
		* Updates this Shader's uniforms according to the specified Portal.
		*
		* @param portal  The Portal to retrieve values from.
		*/
		void bind(const entity::Portal& portal);

		/**
		* Updates this Shader's uniforms according to the specified Portal.
		*
		* @param portal  The Portal to retrieve values from.
		*/
		void bind(const std::shared_ptr<entity::Portal>& portal);

		/** 
		 * Returns trues if the given shader source successfully compiled, otherwise an error is logged.
		 *	
		 * @param shader  The shader source to check.
		 * @return		  True if the shader source compiled.
		 */
		bool check(const GLuint shader);

		/**
		 * Draws the specified Mesh to the current active buffer using this Shader.
		 *
		 * @param mesh  The Mesh to draw.
		 */
		void draw(const Mesh& mesh, const bool box = false);

		/**
		* Draws the specified Mesh to the current active buffer using this Shader.
		*
		* @param mesh  The Mesh to draw.
		*/
		void draw(const std::shared_ptr<Mesh>& mesh, const bool box = false);

		/**
		* Draws the specified Portal to the current active buffer using this Shader.
		*
		* @param portal  The Portal to draw.
		*/
		void draw(const std::shared_ptr<entity::Portal>& portal);

		void draw_batched(const GLint first, const GLsizei count, const bool box = false);

		template <class T>
		void bind_uniform(const std::string& key, const T& value) {
			// Log an error for unsupported types
			get_logger()->warn("Attempted to bind an unsupported uniform type: {}", key);
		}

		template <> void bind_uniform<glm::mat4>(const std::string& key, const glm::mat4& value) {
			glUniformMatrix4fv(get_uniform(key), 1, GL_FALSE, glm::value_ptr(value));
		}

		template <> void bind_uniform<glm::vec4>(const std::string& key, const glm::vec4& value) {
			glUniform4fv(get_uniform(key), 1, glm::value_ptr(value));
		}

		template <> void bind_uniform<glm::vec3>(const std::string& key, const glm::vec3& value) {
			glUniform3fv(get_uniform(key), 1, glm::value_ptr(value));
		}

		template <> void bind_uniform<GLfloat>(const std::string& key, const GLfloat& value) {
			glUniform1f(get_uniform(key), value);
		}

		template <> void bind_uniform<GLint>(const std::string& key, const GLint& value) {
			glUniform1i(get_uniform(key), value);
		}

		template <> void bind_uniform<GLboolean>(const std::string& key, const GLboolean& value) {
			glUniform1i(get_uniform(key), value);
		}

		/**
		 * Compiles and links this Shader's sources into a shader program.
		 */
		void link();
	};
} };