#pragma once

#include <gl/glew.h>
#include <spdlog/spdlog.h>

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "relativity/core/gl_types.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"

namespace relativity {	namespace graphics { namespace buffer {
	/**
	 * A Framebuffer is an object-oriented equivalent of an OpenGL Framebuffer object. It comes with additional
	 * utilities for driving the render system within a typesafe environment.
	 */
	class Framebuffer {
	protected:
		/** Some convenient type aliases as we pass these around a lot */
		using EntityList = std::vector<std::shared_ptr<entity::Entity>>;
		using PortalMap = std::unordered_map<std::string, std::shared_ptr<entity::Portal>>;

		/* Getters */
		/**
		 * Returns the logger associated with this class. 
		 */
		inline auto get_logger() { return logger; }
		
	public:
		using AttachmentMap = std::unordered_map<GLenum, GLuint>;
		/**
		 * Constructs an incomplete Framebuffer with no attachments.
		 */
		Framebuffer(const bool is_multisampled);
		~Framebuffer();

		/* Getters */
		inline auto is_multisampled() { return multisampled; }
		/** Returns the texture target of this framebuffer if it has one. */
		inline virtual GLuint get_output() = 0;
		/** Returns the memory handle of this framebuffer. */
		inline auto get() { return framebuffer; }
		/** Returns this framebuffer's attachments. */
		inline auto& get_attachments() { return  attachments; }
		/** Returns the maximum depth allowed for framebuffers that utilise recursive rendering. */
		static inline auto& get_max_depth() { return MAX_DEPTH; }

		/* Setters */
		/* Sets the maximum depth for recursive rendering to the specified value. */
		static inline void set_max_depth(const int depth) { 
			MAX_DEPTH = (depth >= 0) ? depth : 0;
		}

		virtual inline void set_input(const GLuint input) {
			// Do nothing
		}

		/**
		 * Clears the bit values set in the specified buffer. The associated mask must be set to true
		 * (write-enabled) before this method is called or it will be ignored.
		 */
		template <class T> void clear() {
			// Do nothing and log warning
			get_logger()->error("Tried to clear unknown buffer");
		}

		/**
		 * Clears the bit values set in the specified buffers. The associated masks must be set to true
		 * (write-enabled) before this method is called or it will be ignored.
		 */
		template <class T, class U> void clear() {
			// Do nothing and log warning
			get_logger()->error("Tried to clear unknown buffers");
		}
		
		/**
		 * Clears the bit values set in every buffer. The associated masks must be set to true
		 * (write-enabled) before this method is called or it will be ignored.
		 */
		void clear() { glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); }

		/** The following method calls are just template metafunctions */
		template <> void clear<core::GL::Color>() { glClear(GL_COLOR_BUFFER_BIT); }
		template <> void clear<core::GL::Depth>() { glClear(GL_DEPTH_BUFFER_BIT); }
		template <> void clear<core::GL::Stencil>() { glClear(GL_STENCIL_BUFFER_BIT); }

		template <> void clear<core::GL::Color, core::GL::Depth>() { 
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		template <> void clear<core::GL::Color, core::GL::Stencil>() { 
			glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}

		template <> void clear<core::GL::Depth, core::GL::Stencil>() { 
			glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		}

		template <> 
		void clear<core::GL::Depth, core::GL::Color>() { clear<core::GL::Color, core::GL::Depth>(); }

		template <> 
		void clear<core::GL::Stencil, core::GL::Color>() { clear<core::GL::Color, core::GL::Stencil>(); }

		template <> 
		void clear<core::GL::Stencil, core::GL::Depth>() { clear<core::GL::Depth, core::GL::Stencil>(); }

		/**
		* Enables testing for the specified buffers. The associated masks must be set to true
		* (write-enabled) before this method is called or the affected buffers will be ignored.
		*/
		template <class T, class U> 
		void enable_test(const bool enable) {
			// Do nothing and log warning
			get_logger()->error("Tried to enable/disable an unsupported test");
		}
		
		/** The following method calls are just template metafunctions */
		template <> void enable_test<core::GL::Depth, core::GL::Stencil>(const bool enable) {
			enable_test<core::GL::Depth>(enable); enable_test<core::GL::Stencil>(enable);
		}

		template <> void enable_test<core::GL::Stencil, core::GL::Depth>(const bool enable) {
			enable_test<core::GL::Depth, core::GL::Stencil>(enable);
		}

		/**
		 * Enables testing for the specified buffer. The associated mask must be set to true
		 * (write-enabled) before this method is called or it will be ignored.
		 */
		template <class T> void enable_test(const bool enable) {
			// Do nothing and log warning
			get_logger()->error("Tried to enable/disable an unsupported test");
		}

		/** The following method calls are just template metafunctions */
		template <> void enable_test<core::GL::Depth>(const bool enable) {
			if (enable)	glEnable(GL_DEPTH_TEST);
			else glDisable(GL_DEPTH_TEST);
		}

		template <> void enable_test<core::GL::Stencil>(const bool enable) {
			if (enable)	glEnable(GL_STENCIL_TEST);
			else glDisable(GL_STENCIL_TEST);
		}

		template <class T>
		void enable_mask(const bool r, const bool g, const bool b, const bool a) {
			// Do nothing and log warning
			get_logger()->error("Tried to enable/disable an unsupported mask");
		}

		template <class T, class U>
		void enable_mask(const bool enable) {
			// Do nothing and log warning
			get_logger()->error("Tried to enable/disable unsupported masks");
		}


		template <class T> void enable_mask(const bool enable) {
			// Do nothing and log warning
			get_logger()->error("Tried to enable/disable an unsupported mask");
		}

		template <>
		void enable_mask<core::GL::Color>(const bool r,	const bool g, const bool b,	const bool a) {
			const GLboolean x = (r) ? GL_TRUE : GL_FALSE;
			const GLboolean y = (g) ? GL_TRUE : GL_FALSE;
			const GLboolean z = (b) ? GL_TRUE : GL_FALSE;
			const GLboolean w = (a) ? GL_TRUE : GL_FALSE;
			glColorMask(x, y, z, w);
		}

		template <>	void enable_mask<core::GL::Color>(const bool enable) {
			if (enable) glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			else glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		}

		template <>	void enable_mask<core::GL::Depth>(const bool enable) {
			const GLboolean mask = (enable) ? GL_TRUE : GL_FALSE;
			glDepthMask(mask);
		}

		template <>	void enable_mask<core::GL::Stencil>(const bool enable) {
			const GLuint bit_mask = (enable) ? 0xFF : 0x00;
			glStencilMask(bit_mask);
		}

		template <>	void enable_mask<core::GL::Color, core::GL::Depth>(const bool enable) {
			enable_mask<core::GL::Color>(enable); enable_mask<core::GL::Depth>(enable);
		}

		template <>	void enable_mask<core::GL::Color, core::GL::Stencil>(const bool enable) {
			enable_mask<core::GL::Color>(enable); enable_mask<core::GL::Stencil>(enable);
		}

		template <>	void enable_mask<core::GL::Depth, core::GL::Stencil>(const bool enable) {
			enable_mask<core::GL::Depth>(enable); enable_mask<core::GL::Stencil>(enable);
		}

		template <>	void enable_mask<core::GL::Depth, core::GL::Color>(const bool enable) {
			enable_mask<core::GL::Color, core::GL::Depth>(enable);
		}

		template <>	void enable_mask<core::GL::Stencil, core::GL::Color>(const bool enable) {
			enable_mask<core::GL::Color, core::GL::Stencil>(enable);
		}

		template <>	void enable_mask<core::GL::Stencil, core::GL::Depth>(const bool enable) {
			enable_mask<core::GL::Depth, core::GL::Stencil>(enable);
		}

		/** 
		 * Returns trues if the framebuffer is complete, otherwise an error is logged.
		 *
		 * @return  True if the framebuffer is complete.
		 */
		bool check();

		/**
		* Draws the specified list of Entities and Portals to a selected target framebuffer with the
		* given viewport. A view, projection and light source may also be provided, however it's up to
		* the framebuffer's implementation as to whether or not they're used. This also applies to the 
		* target buffer but the implementee can be overridden by binding the target buffer beforehand.
		*
		* @param width			The width of the viewport in pixels.
		* @param height			The height of the viewport in pixels.
		* @param entities		The entities to draw.
		* @param portals		The portals to draw.
		* @param view			The view to draw from.
		* @param proj			The projection to draw from.
		* @param target_buffer  The buffer to draw to.
		* @param light			The primary light source to draw with.
		*/
		virtual void draw(
			const GLuint width, const GLuint height,
			const EntityList& entities,	const PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) = 0;

		/** Sets this buffer as the active buffer. */
		void bind();
		/** Sets the specified buffer as the active buffer, including within this framebuffer's context. */
		void bind(const std::shared_ptr<Framebuffer>& target_buffer);
		/** 
		 * Sets the specified buffers as the active read/write buffer, including within this 
		 * framebuffer's context.
		 */
		void bind(
			const std::shared_ptr<Framebuffer>& source_buffer, 
			const std::shared_ptr<Framebuffer>& target_buffer
		);

	private:
		// NOTE: we never enable this.
		/** True if this framebuffer uses multisampling */
		const bool multisampled;
		const std::shared_ptr<spdlog::logger> logger;
		AttachmentMap attachments;
		GLuint framebuffer;
		static int MAX_DEPTH;
	};
} } };