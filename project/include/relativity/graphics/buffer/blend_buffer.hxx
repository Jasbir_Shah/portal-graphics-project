#pragma once

#include <gl/glew.h>

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativityUtils/createPortalsFromFile.hxx"

namespace relativity {	namespace graphics { namespace buffer {
	/**
	 * A blend buffer blends two texture buffers together.
	 */
	class BlendBuffer: public Framebuffer {
	private:
		GLuint view_map;
		const GLuint box_map;
		const GLuint depth_map;
		GLuint render_buffer;
		GLuint texture;
		Shader shader;

	public:
		/**
		 * Constructs a Screen Buffer with a texture attachment.
		 */
		BlendBuffer(const std::string& vertex_shader_source, const std::string& fragment_shader_source,
			const GLuint width, const GLuint height, const GLuint view_map, const GLuint box_map, 
			const GLuint depth_map,	const bool is_multisampled = false, const GLsizei max_samples = 4);
		~BlendBuffer();
		
		/* Getters */
		inline GLuint get_output() override { return texture; }

		inline void set_input(const GLuint input) override {
			view_map = input;
		}

		void draw(
			const GLuint width, const GLuint height,
			const std::vector<std::shared_ptr<entity::Entity>>& entities,
			const relativity::utils::PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) override;
	};
} } };