#pragma once

#include <gl/glew.h>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"

#ifndef _G_MAPS_
#define  POSITION_MAP GL_COLOR_ATTACHMENT0 + 0
#define  NORMAL_MAP GL_COLOR_ATTACHMENT0 + 1
#define  ALBEDO_MAP GL_COLOR_ATTACHMENT0 + 2
#define  DIRECTION_MAP GL_COLOR_ATTACHMENT0 + 3
#define  DEPTH_MAP GL_COLOR_ATTACHMENT0 + 4
#endif // !_G_MAPS_

namespace relativity {	namespace graphics { namespace buffer {
	/**
	 * A geometry buffer renders multiple texture maps pertaining to certain important geometrical 
	 * information of the given objects. These include things such as the view space positions, depth
	 * values and colours.
	 */
	class GeometryBuffer: public Framebuffer {
	private:
		GLuint depth_buffer;
		Shader shader;

	public:
		/**
		 * Constructs a Geometry Buffer with a texture attachment.
		 */
		GeometryBuffer(const std::string& vertex_shader_source, const std::string& fragment_shader_source,
			const GLuint width, const GLuint height, const bool is_multisampled = false, const GLsizei max_samples = 4);
		~GeometryBuffer();
		
		/* Getters */
		inline GLuint get_output() override { return  depth_buffer; }

		void draw(
			const GLuint width, const GLuint height,
			const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) override;
	};
} } };