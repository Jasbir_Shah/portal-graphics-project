#pragma once

#include <gl/glew.h>

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativityUtils/createPortalsFromFile.hxx"

namespace relativity {	namespace graphics { namespace buffer {
	/**
	 * Renders portals 
	 */
	class PortalBuffer: public Framebuffer {
	private:
		const std::shared_ptr<entity::Entity> quad;
		const std::shared_ptr<Framebuffer> geometry_buffer;
		const std::shared_ptr<Framebuffer> light_buffer;
		GLuint render_buffer;
		GLuint texture;
		Shader texture_shader;
		Shader simple_shader;

		/**
		* Draws all portals to the buffer, including their viewpoints. For the sake of simplicity, the
		* original non-portal view is considered a portal.
		*/
		void draw_portal(
			const int depth, const GLuint width, const GLuint height,
			const EntityList& entities, const PortalMap& portals,
			const std::shared_ptr<Framebuffer>& target_buffer,
			const std::shared_ptr<entity::Light>& light,
			const glm::mat4& view, const glm::mat4& proj
		);


		void draw_meshes(
			Shader& shader, const PortalMap& portals,
			const glm::mat4& view, const glm::mat4& proj
		);

	public:
		/**
		 * Constructs a Portal Buffer with a texture attachment.
		 */
		PortalBuffer(const std::string& vertex_shader_source, const std::string& fragment_shader_source,
			const std::string& simple_vertex_shader_source, const std::string& simple_fragment_shader_source,
			const GLuint width, const GLuint height, const std::shared_ptr<Framebuffer> geometry_buffer,
			const std::shared_ptr<Framebuffer> light_buffer, const std::shared_ptr<entity::Entity> quad,
			const bool is_multisampled = false, const GLsizei max_samples = 4);
		~PortalBuffer();
		
		/* Getters */
		inline GLuint get_output() override { return texture; }

		void draw(
			const GLuint width, const GLuint height, 
			const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) override;
	};
} } };