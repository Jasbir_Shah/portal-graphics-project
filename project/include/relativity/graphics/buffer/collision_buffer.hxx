#pragma once

#include <gl/glew.h>

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"

namespace relativity {	namespace graphics { namespace buffer {
	/**
	 * A collision draws the bounding boxes surrounding an object, it has the capability do this within
	 * portals, recursively, as well.
	 */
	class CollisionBuffer: public Framebuffer {
	private:
		const std::shared_ptr<entity::Entity> quad;
		GLuint render_buffer;
		GLuint texture;
		GLuint depth_map;
		Shader box_shader;
		Shader simple_shader;

		/**
		 * Draws all portals to the buffer, including their viewpoints. For the sake of simplicity, the 
		 * original non-portal view is considered a portal.
		 */
		void draw_portal(
			const int depth, const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view, const glm::mat4& proj
		);
		
		/**
		 * Draws the bounding boxes surrounding the given entities and portals, using the specified 
		 * shader and viewpoint. 
		 */
		void draw_boxes(
			Shader& shader, const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view, const glm::mat4& proj
		);

		/**
		* Draws the given portals, using the specified shader and viewpoint.
		*/
		void draw_meshes(
			Shader& shader,	const PortalMap& portals,
			const glm::mat4& view, const glm::mat4& proj
		);

	public:
		/**
		 * Constructs a Screen Buffer with a texture attachment.
		 */
		CollisionBuffer(const std::string& vertex_shader_source, const std::string& fragment_shader_source,
			const std::string& simple_vertex_shader_source,const  std::string& simple_fragment_shader_source,
			const GLuint width, const GLuint height, const GLuint depth_map,
			const std::shared_ptr<entity::Entity> quad,
			const bool is_multisampled = false, const GLsizei max_samples = 4);
		~CollisionBuffer();
		
		/* Getters */
		inline GLuint get_output() override { return texture; }

		void draw(
			const GLuint width, const GLuint height,
			const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) override;
	};
} } };