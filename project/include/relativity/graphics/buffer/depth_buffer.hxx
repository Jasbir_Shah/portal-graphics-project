#pragma once

#include <gl/glew.h>

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"

namespace relativity {	namespace graphics { namespace buffer {
	// NOTE: we no longer use this as its purpose is redundant.
	/** 
	 * This buffer is used for drawing depth maps. 
	 */
	class DepthBuffer: public Framebuffer {
	private:
		GLuint texture;
		Shader shader;

	public:
		/**
		 * Constructs a Depth Buffer with a texture attachment.
		 */
		DepthBuffer(const std::string& vertex_shader_source, const std::string& fragment_shader_source,
			const GLuint width, const GLuint height, 
			const bool is_multisampled = false, const GLsizei max_samples = 4);
		~DepthBuffer();
		
		/* Getters */
		inline GLuint get_output() override { return texture; }

		void draw(
			const GLuint width, const GLuint height,
			const EntityList& entities, const PortalMap& portals,
			const glm::mat4& view = glm::mat4(), const glm::mat4& proj = glm::mat4(),
			const std::shared_ptr<Framebuffer>& target_buffer = nullptr,
			const std::shared_ptr<entity::Light>& light = nullptr) override;
	};
} } };