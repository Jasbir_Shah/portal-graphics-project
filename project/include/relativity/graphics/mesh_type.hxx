#pragma once

namespace relativity { namespace graphics {
	/**
	 * An enumeration of Mesh types.
	 */
	enum class MeshType {
		HANDGUN, QUAD, STAIRS, PLAYER, PORTAL, PORTAL_SMALL, PORTAL_LARGE, PORTAL_DOOR, EXIT
	};
} }
