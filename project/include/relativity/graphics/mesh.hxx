#pragma once

#include <gl/glew.h>
#include <glm/glm.hpp>
#include <spdlog/spdlog.h>

#include <memory>
#include <string>
#include <vector>

#include "relativity/graphics/texture.hxx"

namespace relativity {	namespace graphics {
	/**
	 * A mesh is a hierarchical structure that can contain links to additional meshes. It 
	 * encapsulates a vertex buffer object for the associated vertex data and another one for 
	 * its bounding box if available. In addition it keeps a reference to any textures applied to it
	 * and a model matrix for applying transformations to it.
	 */
	class Mesh {
	private:
		const std::shared_ptr<spdlog::logger> logger;
		bool using_bounding_box;

		std::vector<std::shared_ptr<Mesh>> submeshes;
		std::vector<std::shared_ptr<Texture>> textures;
		std::vector<GLfloat> vertex_data;
		std::vector<GLfloat> box_data;
		std::vector<glm::vec3> bounding_box;
		int mesh_size;
		glm::mat4 model;
		GLuint vbo;
		GLuint bbo;

		inline auto get_logger() { return logger; }
		inline auto get_logger() const { return logger; }
		inline auto& get_vertex_buffer() { return vbo; }
		inline auto& get_vertex_buffer() const { return vbo; }

		void load(const std::string& source_path);

		/**
		 * Creates a minimal bounding box based on the given min-max values as the extremities.
		 */
		std::vector<glm::vec3> generate_bounding_box(const GLfloat min_x, const GLfloat max_x,
			const GLfloat min_y, const GLfloat max_y,
			const GLfloat min_z, const GLfloat max_z);

	public:
		bool using_diffuse_map;
		bool using_normal_map;
		bool using_specular_map;
		bool is_floor;
		bool is_stair;
		bool is_wall;
		float shininess;

		/**
		 * Constructs a mesh from a vertex data source.
		 */
		Mesh(const std::vector<GLfloat> vertex_data, const int mesh_size, const std::vector<glm::vec3> bounding_box = std::vector<glm::vec3>());
		
		/**
		 * Constructs a mesh from a file source.
		 */
		Mesh(const std::string& source_path = "");
		~Mesh();

		inline void bind() { glBindBuffer(GL_ARRAY_BUFFER, vbo); }
		inline void bind() const { glBindBuffer(GL_ARRAY_BUFFER, vbo); }
		inline void bind_box() { glBindBuffer(GL_ARRAY_BUFFER, bbo); }
		inline void bind_box() const { glBindBuffer(GL_ARRAY_BUFFER, bbo); }
		inline auto& get_bounding_box() { return bounding_box; }
		inline auto& get_bounding_box() const { return bounding_box; }
		inline auto& get_bounding_buffer() { return bbo; }
		inline auto& get_bounding_buffer() const { return bbo; }
		inline auto& get_model() { return model; }
		inline auto& get_model() const { return model; }
		inline auto& get_submeshes() { return submeshes; }
		inline auto& get_submeshes() const { return submeshes; }
		inline auto& get_textures() { return textures; }
		inline auto& get_textures() const { return textures; }
		inline auto& get_box_data() { return box_data; }
		inline auto& get_box_data() const { return box_data; }
		inline auto& get_vertex_data() { return vertex_data; }
		inline auto& get_vertex_data() const { return vertex_data; }
		inline auto& get_mesh_size() { return mesh_size; }
		inline auto& get_mesh_size() const { return mesh_size; }
		inline auto& has_bounding_box() { return using_bounding_box; }
		inline auto& has_bounding_box() const { return using_bounding_box; }
		inline void set_model(glm::mat4 model) { 
			this->model = model; 
			// Apply transformation to submeshes
			for (const auto submesh : get_submeshes()) {
				submesh->set_model(model);
			}
		}

		void draw();
		void draw() const;

		/* Copy Constructor */
		Mesh(const Mesh& mesh): logger(spdlog::get("mesh")) {
			this->get_logger()->info("Copying");
			this->bounding_box = mesh.get_bounding_box();
			this->submeshes = mesh.get_submeshes();
			this->textures = mesh.get_textures();
			this->vertex_data = mesh.get_vertex_data();
			this->mesh_size = mesh.get_mesh_size();
			this->model = mesh.get_model();
			this->vbo = mesh.get_vertex_buffer();
			this->using_diffuse_map = mesh.using_diffuse_map;
			this->using_normal_map = mesh.using_normal_map;
			this->using_specular_map = mesh.using_specular_map;
		}

		Mesh& operator=(const Mesh& mesh) {
			this->submeshes = mesh.get_submeshes();
			this->bounding_box = mesh.get_bounding_box();
			this->textures = mesh.get_textures();
			this->vertex_data = mesh.get_vertex_data();
			this->mesh_size = mesh.get_mesh_size();
			this->model = mesh.get_model();
			this->vbo = mesh.get_vertex_buffer();
			this->using_diffuse_map = mesh.using_diffuse_map;
			this->using_normal_map = mesh.using_normal_map;
			this->using_specular_map = mesh.using_specular_map;
			return *this;
		}
	};
} };