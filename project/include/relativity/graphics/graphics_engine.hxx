#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <soil/SOIL.h>
#include <gl/glew.h>

#include <spdlog/spdlog.h>

#include "relativity/event/actor.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/shader.hxx"
#include "relativity/entity/entity.h"
#include "relativity/core/window.hxx"
#include "relativity/entity/camera.hxx"
#include "relativity/entity/light.hxx"
#include "relativity/entity/portal.hxx"
#include "relativity/event/event.hxx"
#include "relativity/event/event_type.hxx"
#include "relativity/event/event_queue.hxx"
#include "relativity/graphics/mesh.hxx"
#include "relativity/graphics/shader.hxx"
#include "relativity/graphics/buffer/framebuffer.hxx"
#include "relativity/entity/entity.h"
#include "relativity/core/gl_error.hxx"

namespace relativity {
	namespace graphics {
		/**
		 * A GraphicsEngine drives all graphical operations with an engine, such as rendering, setting up
		 * the render stage, configuring the camera and lightsource and managing framebuffers.
		 */
		class GraphicsEngine : public event::Actor {
		private:
			using PortalMap = std::unordered_map<std::string, std::shared_ptr<entity::Portal>>;

			const std::shared_ptr<spdlog::logger> logger;
			const std::shared_ptr<entity::Entity> quad;
			const std::shared_ptr<event::EventQueue> event_queue;

			// Framebuffers
			std::shared_ptr<buffer::Framebuffer> blend_buffer;
			std::shared_ptr<buffer::Framebuffer> collision_buffer;
			std::shared_ptr<buffer::Framebuffer> forward_buffer;
			std::shared_ptr<buffer::Framebuffer> geometry_buffer;
			std::shared_ptr<buffer::Framebuffer> hud_buffer;
			std::shared_ptr<buffer::Framebuffer> light_buffer;
			std::shared_ptr<buffer::Framebuffer> portal_buffer;
			std::shared_ptr<buffer::Framebuffer> screen_buffer;
			std::shared_ptr<buffer::Framebuffer> demo_light_buffer;
			std::shared_ptr<buffer::Framebuffer> demo_colour_buffer;
			std::shared_ptr<buffer::Framebuffer> demo_normal_buffer;
			std::shared_ptr<buffer::Framebuffer> demo_position_buffer;

			// Camera & lighting
			std::shared_ptr<entity::Camera> camera;
			std::shared_ptr<entity::Light> light;


			/* Getters */
			inline auto get_logger() { return logger; }
		public:
			void name() override { std::cout << std::endl << "Graphics Engine"; }

			/* Constructors */
			GraphicsEngine(const std::shared_ptr<entity::Entity> quad,
				const std::shared_ptr<event::EventQueue> event_queue,
				const GLuint window_width, const GLuint window_height,
				const GLuint shadow_width, const GLuint shadow_height,
				const std::shared_ptr<entity::Entity> player);

			/* Destructors */
			~GraphicsEngine();

			/**
			 * Updates the graphics engine by rendering the scene and checking for camera and light events.
			 */
			void update(const double time_delta, const std::vector<std::shared_ptr<entity::Entity>>& entities,
				const PortalMap& portals, const std::shared_ptr<entity::Entity> player,
				const GLuint width, const GLuint height, const std::shared_ptr<core::Window> window);

			/**
			* Render's a scene to specified buffer with the given shader, light, camera, depth map and resolution.
			*
			* @param shader		   The shader to draw the quad, it's assumed that the given shader maps a texture onto a 2D surface.
			* @param buffer		   The buffer to read from.
			* @param inter_buffer  The buffer to draw to, it must be a non-multisampled buffer.
			* @param light		   The light source to base the scene on.
			* @param camera		   The camera to base the scene on.
			* @param texture	   The texture to use as the default depth map.
			* @param width		   The viewport's width. If this is smaller than the buffer's texture attachment then the output is effectively clipped.
			* @param height		   The viewport's height. If this is smaller than the buffer's texture attachment then the output is effectively clipped.
			* @deprecated		   This function exists as legacy code from when we changed our rendering API.
			*/
			void render_scene(const std::vector<std::shared_ptr<entity::Entity>>& entities,
				const GLuint width, const GLuint height);
		};
	}
}