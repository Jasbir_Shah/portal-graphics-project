#pragma once

#include <gl/glew.h>

namespace relativity {	namespace graphics {
	/**
	 * A VertexAttribute is a simple container-type for vertex attribute information.
	 */
	class VertexAttribute {
	public:
		const GLuint index;
		const GLint size;
		const GLenum type;
		const GLboolean normalised; const GLsizei stride;
		const GLuint offset;

		VertexAttribute(const GLuint index, const GLint size, const GLenum type,
			const GLboolean normalised, const GLsizei stride, const GLuint offset) :
				index(index), size(size), type(type), normalised(normalised), 
				stride(stride), offset(offset) {
			glVertexAttribPointer(index, size, type, normalised,
				stride * sizeof(float), (void*)(offset * sizeof(float)));
			glEnableVertexAttribArray(index);
		}

		~VertexAttribute() { 
			// Do nothing
		}

		/**
		 * Enables this VertexAttribute.
		 */
		void enable() const {
			glVertexAttribPointer(index, size, type, normalised,
				stride * sizeof(float), (void*)(offset * sizeof(float)));
			glEnableVertexAttribArray(index);
		}
	};
} };