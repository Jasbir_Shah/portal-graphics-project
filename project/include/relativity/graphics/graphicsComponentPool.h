#pragma once

#include "relativity/graphics/component_type.hxx"
#include "relativity/graphics/mesh_type.hxx"
#include "relativity/entity/graphicscomponent.h"

#include <functional>
#include <memory>
#include <unordered_map>
#include <string>

namespace relativity {
	namespace graphics {
		/**
		 * A graphics component pool provides a centralised store for meshes so that they only have
		 * to be loaded in once even when reused.
		 */
		class GraphicsComponentPool {
		private:

			// From http://stackoverflow.com/questions/18837857/cant-use-enum-class-as-unordered-map-key
			struct EnumClassHash
			{
				template <typename T>
				std::size_t operator()(T t) const
				{
					return static_cast<std::size_t>(t);
				}
			};

			const std::string source_dir;
			std::unordered_map<MeshType, std::shared_ptr<Mesh>, EnumClassHash> meshPool;
			std::unordered_map < ComponentType, std::shared_ptr<entity::GraphicsComponent>, EnumClassHash > compPool;

		public:
			GraphicsComponentPool(const std::string& source_dir);
			~GraphicsComponentPool();

			/**
			 * Returns a copy of the Component with the specified key.
			 *
			 * @param type  The key associated with the Component.
			 * @return		The Component associated with the key.
			 */
			inline auto construct(const ComponentType& type) {
				auto component = std::make_shared<entity::GraphicsComponent>(compPool[type]);
				return component;
			}
		};
	}
};