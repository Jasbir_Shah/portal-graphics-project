#pragma once

namespace relativity { namespace graphics {
	/**
	 * An enumeration of Component types. This is NOT the same as MeshType, 
	 * contrary to how it might look.
	 */
	enum class ComponentType {
		HANDGUN, QUAD, STAIRS, PLAYER, PORTAL, PORTAL_SMALL, PORTAL_LARGE, PORTAL_DOOR, EXIT
	};
} }
