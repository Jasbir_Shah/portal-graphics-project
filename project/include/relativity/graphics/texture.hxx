#pragma once

#include <gl/glew.h>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <spdlog/spdlog.h>

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "relativity/event/actor.hxx"

namespace relativity { namespace graphics {
	/**
	 * A Texture is a wrapper around an OpenGL texture buffer object. It automates
	 * most of the texture loading and buffer management.
	 */
	class Texture {
	private:
		using TextureAtlas = std::unordered_map<std::string, std::shared_ptr<Texture>>;
		static TextureAtlas atlas;
		const std::shared_ptr<spdlog::logger> logger;
		GLuint buffer;
		int width;
		int height;

		inline auto get_logger() const { return logger; }
		inline auto get_logger() { return logger; }

		void load(const char*, GLenum active_texture);

	public:
		const GLenum slot;
		inline auto get_buffer() const { return buffer; }
		inline auto get_buffer() { return buffer; }
		inline auto get_width() const { return width; }
		inline auto get_width() { return width; }
		inline auto get_height() const { return width; }
		inline auto get_height() { return width; }

		/**
		 * Constructs an empty Texture with the specified texture unit, format and resolution.
		 *
		 * @param active_texture  The texture unit for this Texture to bind itself to.
		 * @param format		  The colour format of the Texture.
		 * @param texture_width   The width of the texture in pixels.
		 * @param texture_height  The height of the texture in pixels.
		 */
		Texture(GLenum active_texture, const GLint format = GL_RGB, const int texture_width = 0, const int texture_height = 0);
		
		/**
		 * Constructs a Texture from the specified source file.
		 *
		 * @param active_texture  The texture unit for this Texture to bind itself to.
		 * @param source_path	  The path to the texture's source file.
		 */
		Texture(GLenum active_texture, const std::string& source_path);
		~Texture();

		/**
		 * Sets this Texture as the current active texture buffer for its texture unit.
		 */
		inline void bind() const {
			glActiveTexture(slot);
			glBindTexture(GL_TEXTURE_2D, buffer);
		}

		/**
		* Sets this Texture as the current active texture buffer for its texture unit.
		*/
		inline void bind() { 
			glActiveTexture(slot);
			glBindTexture(GL_TEXTURE_2D, buffer);
		}

		/**
		 * Loads a texture from the specified source path if it doesn't already contain a reference in the
		 * texture atlas.
		 */
		static std::shared_ptr<Texture> construct(const std::string& source_path, const GLenum active_texture);
	};
} };