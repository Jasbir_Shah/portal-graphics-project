#define GLM_FORCE_RADIANS
#include "relativityUtils/createPortalsFromFile.hxx"

#include <boost/algorithm/string.hpp>
#include <gl/GL.h>
#include <glm/glm.hpp>
#include <yaml-cpp/yaml.h>

#include <memory>
#include <unordered_map>

using namespace std;
using namespace relativity::config;
using namespace relativity::entity;
using namespace relativity::graphics;
using namespace relativity::utils;

/*
* This method is used to create a vector of portals with details loaded in from
* an external YAML file
* <p>
* The file format must be:
* <code>
*	<label>:
*	  position: [<float>, <float>, <float>]
*     orientation: [<float>, <float>, <float>]
*	  destination: <label>
* </code>
* You can also use short-hand notation as per YAML specifications.
*/
PortalMap relativity::utils::load_portals_from_file(
	const shared_ptr<Settings> settings, const string& source_dir, 
		const shared_ptr<GraphicsComponentPool> graphicsPool) {
	PortalMap portals;
	const string source_path = "config/portals.yaml";
	YAML::Node nodes = YAML::LoadFile(source_dir + source_path);

	for (const auto node: nodes) {
		const auto name = node.first.as<string>();
		const auto position_node = node.second["position"];
		const auto orientation_node = node.second["orientation"];
		const auto destination_node = node.second["destination"];
		const auto mesh_node = node.second["mesh"];
		const auto gravity_node = node.second["gravity"];

		if (position_node.IsNull() || orientation_node.IsNull() || destination_node.IsNull() || mesh_node.IsNull()) {
			std::cout << std::endl << "Malformed portal in " << source_dir + source_path << std::endl;
			continue;
		}

		const auto position = position_node.as<glm::vec3>();
		const auto orientation = orientation_node.as<glm::vec3>();
		const auto destination = destination_node.as<string>();
		const auto mesh_switcher = [&graphicsPool](std::string& key) {
			if (boost::iequals(key, "portal")) return graphicsPool->construct(ComponentType::PORTAL);
			else if (boost::iequals(key, "large")) return graphicsPool->construct(ComponentType::PORTAL_LARGE);
			else if (boost::iequals(key, "small")) return graphicsPool->construct(ComponentType::PORTAL_SMALL);
			else if (boost::iequals(key, "door")) return graphicsPool->construct(ComponentType::PORTAL_DOOR);
			else return graphicsPool->construct(ComponentType::PORTAL);
		};
		const auto mesh = mesh_switcher(mesh_node.as<string>());

		const auto gravity = gravity_node.as<glm::vec3>();

		const auto portal = make_shared<Portal>(
			position, orientation, gravity, destination, mesh
		);
		
		portals.insert(make_pair(name, portal));
	}

	return portals;
}

/**
 * Define some conversions between YAML and GLM data types.
 */
namespace YAML {
	/* glm::vec3 */
	template<>
	struct convert<glm::vec3> {
		static Node encode(const glm::vec3& rhs) {
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			node.push_back(rhs.z);
			return node;
		}

		static bool decode(const Node& node, glm::vec3 &rhs) {
			if (!node.IsSequence() || node.size() != 3) {
				return false;
			}
			else {
				rhs.x = node[0].as<float>();
				rhs.y = node[1].as<float>();
				rhs.z = node[2].as<float>();
				return true;
			}
		}
	};
	
	/* glm::vec2 */
	template<>
	struct convert<glm::vec2> {
		static Node encode(const glm::vec2& rhs) {
			Node node;
			node.push_back(rhs.x);
			node.push_back(rhs.y);
			return node;
		}

		static bool decode(const Node& node, glm::vec2 &rhs) {
			if (!node.IsSequence() || node.size() != 2) {
				return false;
			}
			else {
				rhs.x = node[0].as<float>();
				rhs.y = node[1].as<float>();
				return true;
			}
		}
	};
}