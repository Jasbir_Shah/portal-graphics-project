#pragma once
#include "relativity/core/engine.hxx"
#include "relativity/entity/entity.h"
#include "relativity/entity/portal.hxx"
#include "relativity/config/settings.hxx"

#include <glm/glm.hpp>

#include <string>
#include <unordered_map>

/*
* This utility namespace is used to read portal data from the appropriate
* text file and use this data to set the location of a portal entity.
*/
namespace relativity { namespace utils {
	using PortalMap = std::unordered_map<std::string, std::shared_ptr<entity::Portal>>;

	//populate all portals found in a text file
	PortalMap load_portals_from_file(
		const std::shared_ptr<relativity::config::Settings> settings,
		const std::string& source_dir,
		const std::shared_ptr<relativity::graphics::GraphicsComponentPool> graphicsPool
	);
} };