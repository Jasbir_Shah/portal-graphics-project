#version 420

layout (location = 0) out vec4 out_colour;

/**	
 * This fragment shader maps each fragment to a white texel. 
 */
void main() {	
	out_colour = vec4(1.0f);
}
