#version 420

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 colour;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 texCoords;
layout (location = 4) in vec3 tangent;
layout (location = 5) in vec3 bitangent;

uniform vec3 camera_position;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform mat4 light_space;
uniform vec3 light_direction;

out vec3 CameraPosition;
out vec3 Colour;
out vec3 Normal;
out vec3 Position;
out vec2 TexCoords;

void main()
{
	// Pass through the texture and colour
	TexCoords = texCoords;
	Colour = colour;

	// Normalise tangent, bitangent and normal 
	vec3 nTangent = normalize(model * vec4(tangent, 0.0)).xyz;
	vec3 nBitangent = normalize(model * vec4(bitangent, 0.0)).xyz;
	vec3 nNormal = normalize(model * vec4(normal, 0.0)).xyz;
	
	// Calculate tangent matrix for normals
	mat4 tanMatrix = transpose(mat4(
		nTangent.x,   nTangent.y,   nTangent.z,   0.0,
		nBitangent.x, nBitangent.y, nBitangent.z, 0.0,
		nNormal.x,	 nNormal.y,    nNormal.z,    0.0,
		0.0,	     0.0,		  0.0,	       1.0
	));

	// Also pass the camera position
	CameraPosition = (tanMatrix * vec4(camera_position, 1.0f)).xyz;

	// Send the model space normals for later
	Normal = nNormal;

	// send the view space positions for later
	vec4 enhanced_position = model * vec4(position, 1.0f); 
	Position = (tanMatrix * enhanced_position).xyz; 

	// send projected position so we know where to draw on screen
    gl_Position = proj * view * enhanced_position;
}

