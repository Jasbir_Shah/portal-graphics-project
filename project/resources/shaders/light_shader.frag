#version 420

in vec2 TexCoords;

layout (location = 0) out vec4 out_colour;

struct Mesh {
	float shininess;
};

struct Light {
	bool blinn_phong_flag;
	bool brdf_flag;
	bool fog_flag;
	bool gamma_flag;
	bool perspective_flag;
	bool shadow_flag;
	bool ambient_flag;
	bool diffuse_flag;
	bool specular_flag;
	bool falloff_flag;
	bool material_diffuse_flag;
	bool material_normal_flag;
	bool material_specular_flag;
	vec4 light_position;
	vec4 light_colour;
	vec4 fog_colour;
	float ambient_intensity;
	float falloff_radius;
	float fog_density;
	float specular_strength;
};

uniform sampler2D position_map;
uniform sampler2D normal_map;
uniform sampler2D albedo_map;
uniform sampler2D direction_map;

uniform Light light;
uniform Mesh mesh;

float calculate_falloff(float dist, float radius);
float calculate_shadow(vec4 fragment_position, vec4 light_direction, vec4 normal);
vec4 calculate_ambient(vec4 ambient_colour, float strength);
vec4 calculate_diffuse(vec4 diffuse_colour, vec4 direction,	vec4 normal);

vec4 calculate_specular(vec4 specular_colour, vec4 direction, vec4 normal,
	vec4 camera_direction, bool half_vector, float intensity, float strength);

vec4 calculate_fog(vec4 fragment_colour, vec4 fog_colour, float fog_density,
	vec4 camera_position, vec4 fragment_position);

/**
 * This fragment shader calculates the lighting using Phong shading.
 * <p>
 * The basic lighting equation is: light_b = ambience + diffuse + specular
 * <p>
 * We want to also account for falloff, that is, distance from the light source.
 * The falloff can be calculated as the inverse of the distance squared: falloff = 1/dist^2
 * <p>
 * Given the falloff, our equation becomes: light_d = ambience + falloff * (diffuse + specular)
 */
void main() {	
	// Sample the texture maps - note, there's no set format, refer to the geometry buffer's shader
	vec4 normal = vec4(texture(normal_map, TexCoords).rgb, 0.0f);
	vec4 position = vec4(texture(position_map, TexCoords).rgb, 1.0f);
	vec4 diffuse = vec4(texture(albedo_map, TexCoords).rgb, 1.0f);
	vec4 specular = vec4(texture(albedo_map, TexCoords).a, texture(albedo_map, TexCoords).a, texture(albedo_map, TexCoords).a, 1.0f);
	vec4 direction = vec4(texture(direction_map, TexCoords).rgb, 0.0f);

	// Find the direction vector of the light source
	vec4 light_direction = normalize(light.light_position - position);

	// Then calculate the distance of the vector
	float dist = length(light_direction);

	// Calculate the ambient component
	if (light.ambient_flag) {
		out_colour = calculate_ambient(light.light_colour * diffuse, light.ambient_intensity);
	}
	// If ambient lighting is turned off use a default black ambient light
	else {
		out_colour = diffuse;
	}

	// out_colour = ambient
	// Calculate the diffuse and specular components
	if (light.diffuse_flag || light.specular_flag) {
		vec4 mixin = vec4(0.0f, 0.0f, 0.0f, 0.0f);

		// Calculate the diffuse component
		if (light.diffuse_flag) {
			mixin += calculate_diffuse(diffuse, light_direction, normal);
		}

		// Calculate the specular component
		if (light.specular_flag) {
			mixin += calculate_specular(
				light.light_colour * specular, 
				light_direction, 
				normal, direction, 
				light.blinn_phong_flag, 
				mesh.shininess, 
				light.specular_strength
			);
		}
		// mixin = diffuse + specular

		// Calculate shadows (test if the fragment is in a shadow)
		/*if (light.shadow_flag) {
			mixin *= calculate_shadow(light_space_position, lightDirection, normal);
		}*/
		// mixin = (1 - shadow) * (diffuse + specular)

		out_colour += mixin;
	}
	// out_color = ambient + (diffuse + specular) * (1 - shadow)

	// Calculate the falloff: use camera_distance instead of dist to base it on the camera
	if (light.falloff_flag) {
		out_colour *= calculate_falloff(dist, light.falloff_radius);
	}
	// out_color = (ambient + (diffuse + specular) * (1 - shadow)) * falloff

	// Add a fog effect.
	// To change with light position only use:
	//	calculate_fog(out_colour, fog_colour, light.light_position, fragPosition)
	// To change with persective use the camera and view space positions use:
	//	calculate_fog(out_colour, fog_colour, CameraPosition, fragPositionView)
	if (light.fog_flag) {
		out_colour = calculate_fog(out_colour, light.fog_colour, light.fog_density,
			light.light_position, position);
	}

	// Apply gamma correction - note, textures must not already be corrected
	if (light.gamma_flag) {
		vec3 gamma = vec3(1.0f / 2.2f);
		out_colour = pow(out_colour, vec4(gamma, 1.0f));
	}
}

/**
 * The basic falloff equation: 1 / dist^2 doesn't work well with our model because it falls off rapidly.
 * <p>
 * The equation we use is:
 * ((1 - dist^2)/radius^2)^2
 */
float calculate_falloff(float dist, float radius) {
	return (pow(clamp((1.0f - pow(dist, 2) / pow(radius, 2)), 0.0f, 1.0f), 2));
}

/**
 * We calculate the ambient component by just returning the ambient colour multiplied
 * by the ambient strength.
 */
vec4 calculate_ambient(vec4 ambient_colour, float strength) {
	return ambient_colour * strength;
}

/** 
 * We calculate the diffuse component by using the Lambertian light equation.
 * <p>
 * The Lambert light equation is: 
 * <code>
 *	diffuse = dot(frag_normal, light_direction) * diffuse_colour
 * </code>
 * <p>
 */
vec4 calculate_diffuse(vec4 diffuse_colour, vec4 direction,	vec4 normal) {
	// We use max to handle fragments that aren't facing the light source
	float diffuse_amount = max(0, dot(normal, direction));
	return diffuse_amount * diffuse_colour;
}

/** 
 * We calculate the specular component using the either the Blinn-Phong (half vector) or
 * Phong lighting equation.
 * <p>
 * The Phong equation is: 
 * <code>
*   reflection = reflect(-light_direction, normal)
 *	specular = strength * dot(reflection, normal)^shininess * colour
 * </code>
 * <p>
 * The Blinn-Phong equation is: 
 * <code>
 *  reflection = normalize(light_direction + camera_direction)
 *	specular = strength * dot(reflection, normal)^shininess * colour
 * </code>
 */
vec4 calculate_specular(vec4 specular_colour, vec4 direction, vec4 normal,
		vec4 camera_direction, bool half_vector, float intensity, float strength) {

	// reflect assumes the opposite direction so we need to reverse the direction
	vec4 reflection = (half_vector) ? 
		normalize(direction + camera_direction): 
		reflect(-direction, normal);
	
	float specular_intensity = (half_vector) ? 
		pow(max(0.0f, dot(reflection, normal)), intensity): 
		pow(max(0.0f, dot(reflection, camera_direction)), intensity);

	return strength * specular_intensity * specular_colour;
}

/**
 * Applies fog to a fragment using the squared exponential fog equation.
 * <p>
 * We calculate the amount of fog based on a fragment's distance from the camera.
 */
vec4 calculate_fog(vec4 fragment_colour, vec4 fog_colour, float fog_density,
		vec4 camera_position, vec4 fragment_position) {
	// Find the direction of the camera relative to the fragment
	vec4 displacement = camera_position - fragment_position;
	// Then calculate the distance of the fragment from the camera
	float dist = length(displacement);
	float fog_factor = clamp(1 / exp2(pow((fog_density * dist), 2)), 0.0f, 1.0f);

	// return mix(fog_colour, fragment_colour, fog_factor); 
	return fog_factor * fragment_colour + (1.0f - fog_factor) * fog_colour;
}

/**
 * Tests whether a fragment is inside a shadow based on a depth map. This relies 
 * on all objects in a scene being in view of the light, it won't work indoors unless the 
 * camera can't see things outside of the light's view.
 */
float calculate_shadow(vec4 fragment_position, vec4 light_direction, vec4 normal) {
	// Convert the fragment position into [-1, 1]
	vec3 projection = fragment_position.xyz / fragment_position.w;
	projection = projection * 0.5f + 0.5f;

	// Find the maximum depth of the light, and the current depth of the fragment
	float light_depth = texture(position_map, projection.xy).r;
	float fragment_depth = projection.z;
	
	// If the fragment is behind the furthest reaching point of the light,
	// it's in a shadow. We add a bias based on the angle between the normal and 
	// light direction so that we can smooth out the shadow.
	float bias = max(0.05f * (1.0f - dot(normal, light_direction)), 0.005f);
	float shadow = (fragment_depth - bias > light_depth) ? 1.0f : 0.0f;

	// It's possible for parts of the scene to be outside the depth map
	// because of our projection transform, yet not be caught by our depth map border
	// this creates an unrealistic shadow for everything at the very edge of our view space,
	// to fix that we force the shadow off when the depth is outside our maximum value
	if (fragment_depth > 1.0f) shadow = 0.0f;
	
	return 1.0f - shadow ;
}
