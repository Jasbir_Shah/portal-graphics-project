#version 400

layout (location = 0) in vec3 position;

uniform mat4 light_space;
uniform mat4 model;

/**
 * This shader is intended for using when rendering depth maps. The output position of each vertex is 
 * it's position relative to the light source.
 */
void main()
{
	// send position in light space so we can place shadows independent of the camera
    gl_Position = light_space * model * vec4(position, 1.0f);
}

