#version 420

layout (location = 0) in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

/**
 * A vertex shader that simply passes through 3D vertex positions. This shader is intended for use with bounding
 * boxes or similar meshes.
 */
void main()
{
    gl_Position = proj * view * model * vec4(position, 1.0f);
}

