#version 400

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texture_coords;

out vec2 textureCoords;

/**
 * A vertex shader that simply passes through 2D vertex positions. This shader is intended for use with quads or 
 * similar meshes.
 */
void main()
{
	textureCoords = texture_coords;
    gl_Position = vec4(position.x, position.y, 0.0f, 1.0f);
}

