#version 420

in vec3 CameraPosition;
in vec3 Colour;
in vec2 TexCoords;

// Fragment position and normal in model space
in vec3 Normal;
in vec3 Position;

layout (location = 0) out vec3 position;
layout (location = 1) out vec3 normal;
layout (location = 2)  out vec4 albedo;
layout (location = 3)  out vec3 direction;
layout (location = 4) out vec3 depth;

struct Mesh {
	bool using_diffuse_map;
	bool using_normal_map;
	bool using_specular_map;
	float shininess;
	sampler2D diffuse_map;
	sampler2D normal_map;
	sampler2D specular_map;
};

struct Light {
	bool material_diffuse_flag;
	bool material_normal_flag;
	bool material_specular_flag;
};

uniform Light light;
uniform Mesh mesh;

/**
 * The geometry shader - more colloquially known as the g-buffer shader - produces texture maps of 
 * any geographical information we wish to obtain.
 */
void main() {	
	// Find the distance from the fragment's position to the camera's position
	float dist = distance(CameraPosition, Position);

	// Draw the fragment's position onto the position map (RGB = XYZ)
	position = Position;

	// Draw the fragment's normal onto the normal map (RGB = XYZ)
	normal = (mesh.using_normal_map && light.material_normal_flag) 
		? normalize(texture(mesh.normal_map, TexCoords).rgb * 2.0f - 1.0f) 
		: Normal;

	// Draw the fragment's colour before lighting onto the albedo map (RGB = RGB)
	albedo.rgb = (mesh.using_diffuse_map && light.material_diffuse_flag) 
		? texture(mesh.diffuse_map, TexCoords).rgb 
		: Colour;

	// Draw the fragment's specular colour onto the albedo map (A = R). The specular colour is always a 
	// shade of grey, hence we only need one value for it. We would more likely associate this value with the 
	// specular strength and set the specular colour in the mesh in practice.
	albedo.a = (mesh.using_specular_map  && light.material_specular_flag) 
		? texture(mesh.specular_map, TexCoords).r
		: 1.f;

	// FIMUL optimisation
	//float linearize = 1.0f / 255;

	// Draw the fragment's direction from the camera onto the direction map (RGB = XYZ)
	direction = normalize(CameraPosition - Position);

	// Draw the fragment's depth value onto the depth map (R = Z). Depth values are logarithmic, so
	// trying to display this map will reveal a nearly completely red scene as most values will be at
	// the extremely far end of the scale. To display the map, first linearize the depth value, just
	// keep in mind that you need to unlinearize to actually use the map.
	depth = vec3(
		//2 / (1001 - 
		gl_FragCoord.z 
		//* 999)
	);
}
