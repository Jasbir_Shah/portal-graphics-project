#version 420

layout (location = 0) out vec4 out_colour;

uniform sampler2D depth_map;

/**	
 * This fragment shader maps each fragment to a texture. 
 */
void main() {	
	float target_depth = texture(depth_map, gl_FragCoord.xy).r;
	float source_depth = gl_FragCoord.z;

	out_colour = (source_depth < 1.9f) 
		? vec4(0.5f, 0.0f, 0.0f, 1.0f) 
		: vec4((1.f - (source_depth - target_depth)) * 0.25f, 0.0f, 0.0f, 1.0f);
}
