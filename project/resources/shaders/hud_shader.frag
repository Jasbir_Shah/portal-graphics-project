#version 420

in vec2 TexCoords;

layout (location = 0) out vec4 out_colour;

uniform sampler2D view_map;
uniform sampler2D position_map;
uniform sampler2D depth_map;

/**
 *
 */
void main() {	
	// Sample the texture maps
	vec4 view = vec4(texture(view_map, TexCoords).rgb, 1.0f);
	vec4 box = vec4(texture(position_map, TexCoords).rgb, 1.0f);

	out_colour = (box.r > 0.2f && box.g < 0.1f && box.b < 0.1f)
		? view * 0.4f
		: vec4(mix(view.rgb, box.rgb, 0.5f), 1.0f);
}
