#version 400

in vec2 textureCoords;
layout (location = 0) out vec4 out_colour;

uniform sampler2D view_map;

/**	
 * This fragment shader maps each fragment to a texture. 
 */
void main() {	
	vec4 colour = texture(view_map, textureCoords);
	out_colour = colour;
}
